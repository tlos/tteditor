package pl.edu.agh.iet.dzioklos.tteditor;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import pl.edu.agh.iet.dzioklos.tteditor.config.Config;
import pl.edu.agh.iet.dzioklos.tteditor.gui.MainShellFactory;

public class Main {
	private static final Logger log = Logger.getLogger(Main.class);
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BasicConfigurator.configure();
		log.info("Hello, Starting TTEditor...");
		Display display = new Display();
		final MainShellFactory shellFacotory = new MainShellFactory(display);
		final Shell shell = shellFacotory.getComposedShell();
		shell.open();
		
//		String sampleStartText = Config.getSampleStartText();
//		shellFacotory.getcEditorComposite().setText(sampleStartText);
//		
		if(args.length > 0 ){
			shellFacotory.openFile(args[0]);
		}
		
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch())
				display.sleep();
		}
		
		display.dispose();

//		shellFacotory.exit();
	}

}
