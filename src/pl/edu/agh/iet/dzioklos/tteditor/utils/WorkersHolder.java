package pl.edu.agh.iet.dzioklos.tteditor.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import pl.edu.agh.iet.dzioklos.tteditor.config.Config;

public class WorkersHolder {
	public static final ExecutorService THREAD_POOL = Executors.newFixedThreadPool(Config.THREADS_IN_POOL()); 
}
