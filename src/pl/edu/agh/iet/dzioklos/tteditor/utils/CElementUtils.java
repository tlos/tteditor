package pl.edu.agh.iet.dzioklos.tteditor.utils;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import pl.edu.agh.iet.dzioklos.tteditor.beans.CElement;
import pl.edu.agh.iet.dzioklos.tteditor.beans.CElement.Type;

public class CElementUtils {
	public static List<CElement> getLineCommentsOnly(List<CElement> elements){
		List<CElement> result = new LinkedList<CElement>();
		
		for( CElement e: elements){
			if( e.getType() == Type.COMMENT )
				result.add(e);
		}
		
		return Collections.unmodifiableList(result);
	}
}
