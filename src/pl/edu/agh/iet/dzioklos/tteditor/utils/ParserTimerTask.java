package pl.edu.agh.iet.dzioklos.tteditor.utils;

import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;

public abstract class ParserTimerTask implements Runnable{
	private static final Logger log = Logger.getLogger(ParserTimerTask.class);
	private AtomicBoolean postpone = new AtomicBoolean(false);
	
	@Override
	public void run() {
		
		if( !postpone.get()  ){
			log.info("Running ParserTask");
			perform();
		}else{
			log.info("Postponing ParserTask");
			postpone.set(false);
		}
		
	} 
	/**
	 * pospones one invokation of task
	 */
	public void postpone(){
		postpone.set(true);
	}
	public void removePostpone(){
		postpone.set(false);
	}
	
	public abstract void perform();
	
}
