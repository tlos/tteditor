package pl.edu.agh.iet.dzioklos.tteditor.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

import pl.edu.agh.iet.dzioklos.tteditor.config.Config;

public class CCompilerUtils {
	private static final Logger log = Logger.getLogger(CCompilerUtils.class);
	public static String compile(String inFileName, String outFileName) throws Exception{
		log.info("Trying to compile inFile=" + inFileName + " to outFile="+outFileName);
		StringBuilder commandToCompile = new StringBuilder();
		commandToCompile.append(Config.GET_COMPILATOR_PATH());
		commandToCompile.append(" ");
		
		commandToCompile.append(Config.GET_COMPILATOR_CFLAGS());
		commandToCompile.append(" ");
		
		commandToCompile.append(Config.GET_COMPILATOR_LFLAGS());
		commandToCompile.append(" ");
		
		commandToCompile.append( "-o ");
		commandToCompile.append(outFileName);
		
		commandToCompile.append(" ");
		commandToCompile.append(inFileName);
		
		String command = commandToCompile.toString();
		log.info("Trying to invoke compiler with command:\n"+command);
		
		Process process = Runtime.getRuntime().exec(command);
		process.waitFor();
		
		BufferedReader bin = new BufferedReader( new InputStreamReader( process.getErrorStream()));
		BufferedReader binstd = new BufferedReader( new InputStreamReader( process.getInputStream()));
		StringBuffer outStringBuf = new StringBuffer();
		String line = "";
		while((line=bin.readLine())!= null){
			outStringBuf.append(line);
			outStringBuf.append("\n");
		}
		while((line=binstd.readLine())!= null){
			outStringBuf.append(line);
			outStringBuf.append("\n");
		}
		bin.close();
		binstd.close();
		String outString = outStringBuf.toString();
		if( outString.toLowerCase().contains("error")){
			throw new CompilerException(outString);
		}
		return outString;
	}
}	
