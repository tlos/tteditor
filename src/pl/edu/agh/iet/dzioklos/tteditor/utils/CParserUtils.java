package pl.edu.agh.iet.dzioklos.tteditor.utils;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;

import pl.edu.agh.iet.dzioklos.tteditor.antlr.grammar.CParserWrapper;
import pl.edu.agh.iet.dzioklos.tteditor.antlr.grammar.ExtendedRecognitionException;
import pl.edu.agh.iet.dzioklos.tteditor.beans.CElement;
import pl.edu.agh.iet.dzioklos.tteditor.beans.CElement.Type;
import pl.edu.agh.iet.dzioklos.tteditor.beans.CError;
import pl.edu.agh.iet.dzioklos.tteditor.config.Config;
import pl.edu.agh.iet.dzioklos.tteditor.gui.CEditorComposite;
import pl.edu.agh.iet.dzioklos.tteditor.gui.ConsoleComposite;
import pl.edu.agh.iet.dzioklos.tteditor.gui.TreeComposite;

/**
 * 
 * SINGLETON
 * 
 */
public class CParserUtils {
	private static final Logger log = Logger.getLogger(CParserUtils.class);
	private ConsoleComposite consoleComposite;
	private TreeComposite treeComposite;
	private CEditorComposite cEditorComposite;

	private ParserTimer parserTimer;
	private ParserTimerTask parserTimerTask;

	private AtomicBoolean isDirty = new AtomicBoolean(false);

	private static CParserUtils __instance = null;

	private CParserUtils(ConsoleComposite consoleComposite,
			TreeComposite treeComposite, CEditorComposite cEditorComposite) {
		super();
		this.consoleComposite = consoleComposite;
		this.treeComposite = treeComposite;
		this.cEditorComposite = cEditorComposite;
	}

	/**
	 * Initialize parser for gui purpose
	 * 
	 * @return instance of new created object or null if object already exists,
	 *         then use getInstance
	 */
	public synchronized static CParserUtils initialize(
			ConsoleComposite consoleComposite, TreeComposite treeComposite,
			CEditorComposite cEditorComposite) {
		if (__instance == null) {
			__instance = new CParserUtils(consoleComposite, treeComposite,
					cEditorComposite);
			return __instance;
		} else
			return null;
	}

	/**
	 * 
	 * @return instance of initialized object, remember to use initialize at
	 *         first
	 */
	public synchronized static CParserUtils getInstance() {
		return __instance;
	}

	public void parseAndRefreshGuiInBackground() {
		WorkersHolder.THREAD_POOL.execute(new Runnable() {
			@Override
			public void run() {
			//	 consoleComposite.appendText("PARSING...    ");
				consoleComposite.clearConsole();
				CParserWrapper parser;
				String input = cEditorComposite.getText();

				cEditorComposite.clearBadLinesAndRefresh();
				if (input.isEmpty()) {

					return;
				}
				parser = new CParserWrapper(input);
				try {
					log.info("PARSING....");
					parser.parse();
					List<CElement> elements = parser.getCElements();
					treeComposite.setCElementsAndRefreshTree(elements);
					cEditorComposite.setCElements(elements);
//					printCElements(elements);
				} catch (ExtendedRecognitionException excp) {
					consoleComposite.appendText("FOUND ERRORS: "
							+ excp.getErrorsList().size());
					for (CError error : excp.getErrorsList()) {
						StringBuilder sb = new StringBuilder();
						sb.append("# line: ");
						sb.append(error.getLineNo() + ":" + error.getColumnNo());
						sb.append("\t" + error.getParserMsg());
						consoleComposite.appendText(sb.toString());
						cEditorComposite.addBadLineNoAndRefresh(error
								.getLineNo() - 1);// goToAndSelectLine(e.getLineNo());
					}

				}
				return;
			}
		});
	}

	private void printCElements(List<CElement> elements) {
		consoleComposite.appendText("Printing CElements (total: "
				+ elements.size() + "):");
		for (CElement e : elements) {
			if (e.getType() != Type.COMMENT) {
				continue;
			}

			consoleComposite.appendTextWithoutNewLine("type: " + e.getType());
			consoleComposite.appendTextWithoutNewLine("\tname: " + e.getName());
			consoleComposite.appendTextWithoutNewLine("\tline: "
					+ e.getLineNo());
			switch (e.getType()) {
			case DEFINE_SATEMENT: {

			}
			case FUNCTION: {
				consoleComposite.appendTextWithoutNewLine("\treturn type: "
						+ e.getReturnType());
				break;
			}
			case GLOBAL_VARIABLE: {
				consoleComposite.appendTextWithoutNewLine("\t type: "
						+ e.getReturnType());

			}
			case STRUCT: {
				
			}
			case COMMENT:{
				consoleComposite.appendTextWithoutNewLine("\nL_start: " + e.getCommentStartOffsetInLine() + "G_start: " + e.getCommentStartGlobalOffset() + ", len: " + e.getCommentLength());
				break;
			}
			
			}
			consoleComposite.appendText("\n=============");
		}

	}

	public void addAutoParseLogic(StyledText styledText) {
		// add timer
		parserTimer = new ParserTimer(
				Config.GET_AUTOPARSE_TIMER_INTERVAL_MSECS());
		parserTimerTask = new ParserTimerTask() {

			@Override
			public void perform() {
				if (isDirty.getAndSet(false))
					parseAndRefreshGuiInBackground();
			}
		};
		parserTimer.addTimerTast(parserTimerTask);
		WorkersHolder.THREAD_POOL.execute(parserTimer);

		styledText.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent arg0) {

				isDirty.compareAndSet(false, true); // if was not dirty set to
													// dirty

				// if (arg0.keyCode != ';') {
				for (char c : Config.GET_AUTOPARSE_TRIGGERING_CHARS()) {
					if (c == arg0.character) {
						parserTimerTask.removePostpone();
						parserTimerTask.run();
						return;
					}
				}
				parserTimerTask.postpone();

			}
		});

	}

	public void stopTimer() {
		parserTimer.stopTimer();
	}

}
