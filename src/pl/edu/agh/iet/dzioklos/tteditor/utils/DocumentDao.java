package pl.edu.agh.iet.dzioklos.tteditor.utils;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.log4j.Logger;

import pl.edu.agh.iet.dzioklos.tteditor.config.Config;

public class DocumentDao {
	private static final Logger log = Logger.getLogger(DocumentDao.class);
	
	public static void saveDocument(String fileName,String doc) throws IOException{
		log.info("Writing document to file "+ fileName);
		FileWriter fw = new FileWriter(fileName);
		BufferedWriter bout = new BufferedWriter(fw);
		//bout.write(new String(doc.getBytes(),"UTF-8" ));
		bout.write(doc);
		bout.flush();
		bout.close();
		fw.close();
		log.info("Writing document to file "+ fileName + " completed successfully");
	}
	
	public static String loadDocument(String fileName) throws IOException{
		log.info("Loading document from file "+ fileName);
		FileReader fr = new FileReader(fileName);
		StringBuffer sb = new StringBuffer();
		char [] buf = new char[Config.DAO_BUFFER_SIZE()];
		int len = 0;
		while(len != -1){
			len = fr.read(buf);
			if( len > 0 ) 
				sb.append(buf,0,len);
			
		}
		
		fr.close();
		log.info("Loading document from file "+ fileName + " completed successfully");
		return sb.toString();//new String(sb.toString().getBytes(),"UTF-8");//sb.toString();
	}
}
