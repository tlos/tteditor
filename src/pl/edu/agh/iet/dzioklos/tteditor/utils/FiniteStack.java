package pl.edu.agh.iet.dzioklos.tteditor.utils;

import java.util.Stack;

/**
 * Stack with finite item count
 * if we try to oush more items than capacity,
 * items from bottom are removed
 *
 * @author Tomek
 *
 * @param <T> value type
 */
public class FiniteStack<T> extends Stack<T> {

	private static final long serialVersionUID = -4061617524562707010L;
	private Node head;
	private Node last;
	private int maxSize;
	private int size = 0;

	public FiniteStack(int capacity) {
		this.maxSize = capacity;
	}

	private class Node {
		public Node(T val, Node next, Node prev) {
			super();
			this.val = val;
			this.next = next;
			this.prev = prev;
		}
		T val;
		Node next;
		Node prev;
	}

	@Override
	public synchronized boolean add(T e) {
		return push(e) != null;
	}
	
	
	@Override
	public synchronized T pop() {
		if (size == 0)
			return null;
		T result = last.val;
		last = last.prev;
		if( last == null ){
			head = null;
		}
		else
			last.next = null;
		size--;
		return result;

	}

	@Override
	public synchronized T push(T item) {
		if (size > maxSize) {
			// remove first;
			head = head.next;
			head.prev = null;
			size--;
		}
		if (head == null) {
			head = new Node(item, null, null);
			last = head;
		} else {
			last.next = new Node(item, null, last);
			last = last.next;
		}
		size++;
		return item;
	}

	@Override
	public synchronized boolean isEmpty() {
		return size == 0;
	}

	@Override
	public synchronized int size() {
		return size;
	}

}
