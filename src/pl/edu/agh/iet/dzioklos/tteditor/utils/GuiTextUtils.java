package pl.edu.agh.iet.dzioklos.tteditor.utils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import pl.edu.agh.iet.dzioklos.tteditor.config.Config;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

@SuppressWarnings("unchecked")
public class GuiTextUtils {
	private static Map<String, String> text = new HashMap<String, String>();
	private static final Logger log = Logger.getLogger(GuiTextUtils.class);

	static {

		try {
			XStream xStream = new XStream(new DomDriver());
			xStream.alias("root", java.util.Map.class);

			// from XML, convert back to map
			text = (Map<String, String>) xStream.fromXML(new File(Config
					.GET_LOCALE_FILE()));
		} catch (Exception e) {
			log.error("Failed to get data from locale file = " + Config
					.GET_LOCALE_FILE());
		}

	}

	public static String guiText(String id) {
		String result = text.get(id);
		if (result == null) {
			log.warn("guiText->[NOT found] No value for key=" + id);
			return "";
		}
		log.info("guiText->[found] for key=" + id + " value=" + result);
		return result;
	}

	/*
	 * SAVE AND LOAD XML LOGIC
	 */

	// public static void main(String[] args) {
	//
	//
	//
	// // text.put("ConsoleComposice.cleanBtn", "Clean");
	// // text.put("TreeComposite.label", "Outline:     ");
	// // text.put("Shell.title", "TTEdytor " + Config.VERSION());
	// // text.put("MainShell.menuFile", "&File");
	// // text.put("MainShell.menuFile.exit", "Exit");
	// // text.put("MainShell.menuFile.open", "Open");
	// // text.put("MainShell.menuFile.save", "Save");
	// // text.put("MainShell.menuFile.saveAs", "Save As");
	// // text.put("MainShell.menuFile.closeFile", "Close file");
	// // text.put("MainShell.menuProject", "&Project");
	// // text.put("MainShell.menuProject.build", "Build Project");
	// // text.put("MainShell.menuDev", "&DEV");
	// //
	// // text.put("Console.compilationError", "Compilation Error");
	// // text.put("Console.compilationSuccess", "Compilation Success");
	// // text.put("Console.error", "Error");
	// // text.put("ConsoleComposite.label", "Console:");
	// //
	// // text.put("MainShellFactory.closeConfirmDialogMsg",
	// // "Do you want to save file before close?");
	//
	//
	//
	// XStream xStream = new XStream(new DomDriver());
	// xStream.alias("root", java.util.Map.class);
	//
	//
	// FileOutputStream fout = null;
	// try {
	// fout = new FileOutputStream(Config.GET_LOCALE_FILE());
	// xStream.toXML(text, fout);
	// } catch (FileNotFoundException e) {
	// e.printStackTrace();
	//
	// }
	//
	// }

}
