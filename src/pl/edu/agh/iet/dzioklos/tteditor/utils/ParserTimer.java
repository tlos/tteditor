package pl.edu.agh.iet.dzioklos.tteditor.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;

public class ParserTimer implements Runnable {
	private static final Logger log = Logger.getLogger(ParserTimer.class);
	private List<Runnable> tasks = new ArrayList<Runnable>(1);
	private long interval = 0;
	private AtomicBoolean stop = new AtomicBoolean(false);

	public ParserTimer(long milis) {
		this.interval = milis;;
	}
	
	@Override
	public void run() {
		while (true) {
			try {
				if( stop.get() ){
					log.info("Exiting run loop");
					return;
				}
				synchronized (ParserTimer.this) {
					log.info("Runnging tasks...");
					for( Runnable r : tasks ){
						r.run();
					}
					Thread.sleep(interval);
				}

			} catch (InterruptedException e) {
				log.warn("Interupted ! exiting run loop ");
				return;
			}
		}
	}

	public ParserTimer addTimerTast(Runnable task) {
		synchronized (this) {
			tasks.add(task);	
		}
		return this;
	}

	public ParserTimer setInterval(long milis) {
		synchronized (this) {
			this.interval =milis;
		}
		return this;
	}
	public void stopTimer(){
		stop.set(true);
	}
	
}
