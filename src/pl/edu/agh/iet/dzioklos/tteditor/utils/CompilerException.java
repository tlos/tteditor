package pl.edu.agh.iet.dzioklos.tteditor.utils;

public class CompilerException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7428361630174705053L;
	public CompilerException(String msg) {
		super(msg);
	}
	public CompilerException(String msg,Throwable cause) {
		super(msg,cause);
	}
	public CompilerException() {
	}
}
