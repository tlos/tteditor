package pl.edu.agh.iet.dzioklos.tteditor.beans;

import java.util.HashMap;
import java.util.Map;

public class CElement {
	public static enum Type {
		STRUCT, FUNCTION, GLOBAL_VARIABLE, DEFINE_SATEMENT, COMMENT;
	}

	private Type type;
	private String name;
	private int lineNo;
	private Map<String, String> paramsAndTypes = new HashMap<String, String>();
	private String returnType;

	private int commentStartLineOffset;
	private int commentStartGlobalOffset;
	private int commentLength;

	public int getCommentStartGlobalOffset() {
		return commentStartGlobalOffset;
	}

	public void setCommentStartGlobalOffset(int commentStartGlobalOffset) {
		this.commentStartGlobalOffset = commentStartGlobalOffset;
	}

	public int getCommentStartOffsetInLine() {
		return commentStartLineOffset;
	}

	public void setCommentStartOffsetInLine(int commentStartOffsetInLine) {
		this.commentStartLineOffset = commentStartOffsetInLine;
	}

	public int getCommentLength() {
		return commentLength;
	}

	public void setCommentLength(int commentLength) {
		this.commentLength = commentLength;
	}

	public CElement setType(Type type) {
		this.type = type;
		return this;
	}

	public CElement setName(String name) {
		this.name = name;
		return this;
	}

	public CElement setLineNo(int lineNo) {
		this.lineNo = lineNo;
		return this;
	}

	public CElement setReturnType(String returnType) {
		this.returnType = returnType;
		return this;
	}

	public CElement addParam(String paramName, String typeName) {
		this.paramsAndTypes.put(paramName, typeName);
		return this;
	}

	public Type getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public int getLineNo() {
		return lineNo;
	}

	public Map<String, String> getParamsAndTypes() {
		return paramsAndTypes;
	}

	public String getReturnType() {
		return returnType;
	}

}
