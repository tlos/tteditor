package pl.edu.agh.iet.dzioklos.tteditor.beans;

public class CError {

	private int lineNo;
	private int columnNo;
	private String parserMsg;

	public int getLineNo() {
		return lineNo;
	}

	public int getColumnNo() {
		return columnNo;
	}

	public String getParserMsg() {
		return parserMsg;
	}

	public CError(int lineNo, int columnNo, String parserMsg) {
		super();
		this.lineNo = lineNo;
		this.columnNo = columnNo;
		this.parserMsg = parserMsg;
	}

	@Override
	public String toString() {
		return String.format("## line: %d, column %d, %s", lineNo, columnNo,
				parserMsg);
	}

}
