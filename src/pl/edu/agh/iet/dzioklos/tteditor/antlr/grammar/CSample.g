grammar CSample;

options {
  language = Java;
    backtrack=true;
    memoize=true;
    k=2;
      output = AST;
  ASTLabelType=CommonTree; 
}
scope Symbols {
	Set types; // only track types in order to get parser working
} 

 


@header {
package pl.edu.agh.iet.dzioklos.tteditor.antlr.grammar;

import java.util.Set; 
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;

import pl.edu.agh.iet.dzioklos.tteditor.beans.CElement;
import pl.edu.agh.iet.dzioklos.tteditor.beans.CElement.Type;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import pl.edu.agh.iet.dzioklos.tteditor.beans.CError;

}

@lexer::header { 
package pl.edu.agh.iet.dzioklos.tteditor.antlr.grammar;

import java.util.List;
import java.util.ArrayList;

import pl.edu.agh.iet.dzioklos.tteditor.beans.CElement;
import pl.edu.agh.iet.dzioklos.tteditor.beans.CElement.Type;
}



@members {
	boolean isTypeName(String name) {
		for (int i = Symbols_stack.size()-1; i>=0; i--) {
			Symbols_scope scope = (Symbols_scope)Symbols_stack.get(i);
			if ( scope.types.contains(name) ) {
				return true;
			}
		}
		return false; 
	}
	
	private List<CError> errorsList = new ArrayList<CError>();

	public List<CError> getErrorsList(){
		return errorsList;
	}

	private void addCError(String msg) {
//		System.out.println("ERROR:" + msg);
		Pattern pattern = Pattern.compile("line (\\d+):(\\d+) (.*)");
		Matcher matcher = pattern.matcher(msg.trim());
		matcher.find();
		
		int line = Integer.parseInt(matcher.group(1));
		int column = Integer.parseInt(matcher.group(2));
		String message = matcher.group(3);
		
		
		
		CError error = new CError(line, column, message);
		errorsList.add(error);
//		System.out.println("EEEEEE: " + error);
//		System.out.println("msg: " + msg);
	}



//	@Overrride
	public void emitErrorMessage(String msg) {
		addCError(msg);
//        System.out.println("MSG_ERROR: " + msg);
    }
//	public void displayRecognitionError(String[] tokenNames,
//                                        RecognitionException e) {
//        String hdr = getErrorHeader(e);
//        String msg = getErrorMessage(e, tokenNames);
//    }
	
	private List<CElement> elements = new ArrayList<CElement>();
	
	public List<CElement> getCElements(){
		return elements;
	}
	
	void addComments(List<CElement> comments){
		elements.addAll(comments);
	}
	
	private void addCElement(CElement element){
		elements.add(element);
	}
	private boolean isDEclaredTypeWithName(Type type, String name){
		for (CElement elem: elements){
			if (elem.getType() == type && elem.getName().trim().equals(name.trim()) )
				return true;
		}
		return false;
	}
	
	private void addFunction(String nameWithArgs, String returnType, int lineNo) {
		if (isDEclaredTypeWithName(Type.FUNCTION, nameWithArgs)){
			CError error = new CError(lineNo, 0, "not unique name for function: " + nameWithArgs);
			errorsList.add(error);
			return;
		}

		CElement elem = new CElement();
		elem.setType(Type.FUNCTION);
		elem.setLineNo( lineNo);
		elem.setReturnType( returnType!=null ? returnType : "unkwn");
		if (nameWithArgs == null){
			elem.setName("unkwn");
		}else{
		elem.setName(nameWithArgs);
		}	
		addCElement(elem);
	}
	
	private void addStruct(List<StringPair> pairs, String name, int lineNo){
		CElement elem = new CElement();
		elem.setType(Type.STRUCT);
		elem.setName(name);
		elem.setLineNo(lineNo);
		
		for (StringPair sp : pairs){
			elem.addParam(sp.getString2(), sp.getString1());
		}		
		addCElement(elem);
	}

	void addGlobalVariable(String name, String returnType, int lineNo){
//		if (isDEclaredTypeWithName(Type.GLOBAL_VARIABLE, name)){
//			CError error = new CError(lineNo, 0, "not unique name for global variable: " + name);
//			errorsList.add(error);
//			return;
//		}
	
	
		CElement elem = new CElement();
		elem.setType(Type.GLOBAL_VARIABLE);
		elem.setLineNo(lineNo);
		elem.setReturnType(returnType);
		elem.setName(name);
		addCElement(elem);
	}
	
	private List<StringPair> pairsList;
	private boolean isInFunction = false;
}

@lexer::members {

	private List<CElement> comments = new ArrayList<CElement>();
	
	List<CElement> getComments(){
		return comments;
	}
	
	private void addComment(int startGlobalOffset, int startLineOffset, int lineNo, String text){
		CElement elem = new CElement();
		elem.setType(Type.COMMENT);
		elem.setCommentStartOffsetInLine(startLineOffset);
		elem.setCommentStartGlobalOffset(startGlobalOffset);
//		String normalizedText = text.replaceAll("\r\n", "\n");
		elem.setCommentLength(text.length());
		elem.setLineNo(lineNo);
		elem.setName(text);

		comments.add(elem);
	}
	
	
	
}


// now exceptions are thrown instead of catching and recovering
@rulecatch {
   catch (RecognitionException e) {
//    System.out.println("KACZ");
//    System.out.println(e.getMessage());
    throw e;
   }
}


program
	: translation_unit
	;

translation_unit
scope Symbols; // entire file is a scope
@init {
  $Symbols::types = new HashSet();
}
	: external_declaration+
	;


 
external_declaration
options {k=2;}
	: ( (declaration_specifiers?)! (declarator declaration*)! '{'! )=> function_definition! {isInFunction = false;
				System.out.println("IN2: " + isInFunction);
				}
	| declaration
	;



function_definition
scope Symbols; // put parameters and locals into same scope for now
@init {
  $Symbols::types = new HashSet();
}
	:	
	{
				isInFunction = true;
	}
	
	declaration_specifiers? declarator {
				System.out.println("IN1: " + isInFunction);
				addFunction($declarator.text, $declaration_specifiers.text, $declarator.start.getLine());
				
				}
		(	declaration+ compound_statement	// K&R style
		|	compound_statement				// ANSI style
		)
	;

declaration
scope {
  boolean isTypedef;
} 
@init {
  $declaration::isTypedef = false;
}
	: 'typedef' declaration_specifiers? {$declaration::isTypedef=true;}
	  init_declarator_list ';'! // special case, looking for typedef	
	| op2a=declaration_specifiers (op2b=init_declarator_list?) ';'! 
	{
		if (!$op2a.isStruct){
			if (!isInFunction){
				System.out.println("ADD: " + isInFunction);
				addGlobalVariable($op2b.text, $op2a.text, $op2a.start.getLine() );
			}
		}
	}
	;

declaration_specifiers returns [boolean isStruct]
	:   (   storage_class_specifier
		|  type_specifier {$isStruct = $type_specifier.isStruct;}
        |   type_qualifier
        )+
	;

init_declarator_list
	: init_declarator (',' init_declarator)*
	;

init_declarator
	: declarator ('=' initializer)?
	;

storage_class_specifier
	: 'extern'
	| 'static'
	| 'auto'
	| 'register'
	;

type_specifier returns [boolean isStruct]
	: 
	{$isStruct = false;}
	'void'
	| 'char'
	| 'short'
	| 'int'
	| 'long'
	| 'float'
	| 'double'
	| 'signed'
	| 'unsigned'
	| struct_or_union_specifier {$isStruct = true;}
	| enum_specifier {$isStruct = true;}
	| type_id {$isStruct = true;}
	;

type_id
    :   {isTypeName(input.LT(1).getText())}? IDENTIFIER
//    	{System.out.println($IDENTIFIER.text+" is a type");}
    ;

struct_or_union_specifier
options {k=3;}
scope Symbols; // structs are scopes
@init {
  $Symbols::types = new HashSet();
}
	: op1=struct_or_union op2=IDENTIFIER? '{' op3=struct_declaration_list '}' 
		{
//		System.out.println("STRUCT ID:");
//		System.out.println($op2.text);
		
//		System.out.println("STRUCT ELEMS: " + $op3.list.size());
		
//		System.out.println($op3.list.get(0).getString1());
//		System.out.println("STRUCT ELEMS: " + $op3.list.size());
		
//		System.out.println($op3.text);
//		System.out.println("---STRUCT");
		addStruct($op3.list, $op2.text, $op1.start.getLine());
		}
	| struct_or_union IDENTIFIER
	;

struct_or_union
	: 'struct'
	| 'union'
	;

struct_declaration_list returns [List<StringPair> list]
	:
	{
//	System.out.println("INITqqqqqqqqqqqqq");
	pairsList = new ArrayList<StringPair>();
	}
	op=struct_declaration+ {$list=pairsList; 
//	System.out.println("ASSIGN");
	}
	;

struct_declaration
	: 
	op1=specifier_qualifier_list op2=struct_declarator_list ';' 
		{
//		System.out.println("AAAA: " + $op1.text);
//		System.out.println("AAAA: " + $op2.text);
		StringPair pair = new StringPair($op1.text, $op2.text);
		pairsList.add(pair);
//		System.out.println("NEW " +pairsList.size());
		}
	;

specifier_qualifier_list
	: ( type_qualifier | type_specifier )+
	;

struct_declarator_list
	: struct_declarator (',' struct_declarator)*
	;

struct_declarator
	: declarator (':' constant_expression)?
	| ':' constant_expression
	;

enum_specifier
options {k=3;}
	: 'enum' '{' enumerator_list '}'
	| 'enum' IDENTIFIER '{' enumerator_list '}'
	| 'enum' IDENTIFIER
	;

enumerator_list
	: enumerator (',' enumerator)*
	;

enumerator
	: IDENTIFIER ('=' constant_expression)?
	;

type_qualifier
	: 'const'
	| 'volatile'
	;

declarator
	: pointer? direct_declarator
	| pointer
	;

direct_declarator
	:   (	IDENTIFIER
			{
			if ($declaration.size()>0&&$declaration::isTypedef) {
				$Symbols::types.add($IDENTIFIER.text);
				System.out.println("define type "+$IDENTIFIER.text);
			}
			}
		|	'(' declarator ')'
		)
        declarator_suffix*
	;

declarator_suffix
	:   '[' constant_expression ']'
    |   '[' ']'
    |   '(' parameter_type_list ')'
    |   '(' identifier_list ')'
    |   '(' ')'
	;

pointer
	: '*' type_qualifier+ pointer?
	| '*' pointer
	| '*'
	;

parameter_type_list
	: parameter_list (',' '...')?
	;

parameter_list
	: parameter_declaration (',' parameter_declaration)*
	;

parameter_declaration
	: declaration_specifiers (declarator|abstract_declarator)*
	;

identifier_list
	: IDENTIFIER (',' IDENTIFIER)*
	;

type_name
	: specifier_qualifier_list abstract_declarator?
	;

abstract_declarator
	: pointer direct_abstract_declarator?
	| direct_abstract_declarator
	;

direct_abstract_declarator
	:	( '(' abstract_declarator ')' | abstract_declarator_suffix ) abstract_declarator_suffix*
	;

abstract_declarator_suffix
	:	'[' ']'
	|	'[' constant_expression ']'
	|	'(' ')'
	|	'(' parameter_type_list ')'
	;
	
initializer
	: assignment_expression
	| '{' initializer_list ','? '}'
	;

initializer_list
	: initializer (',' initializer)*
	;

// E x p r e s s i o n s

argument_expression_list
	:   assignment_expression (',' assignment_expression)*
	;

additive_expression
	: (multiplicative_expression) ('+' multiplicative_expression | '-' multiplicative_expression)*
	;

multiplicative_expression
	: (cast_expression) ('*' cast_expression | '/' cast_expression | '%' cast_expression)*
	;

cast_expression
	: '(' type_name ')' cast_expression
	| unary_expression
	;

unary_expression
	: postfix_expression
	| '++' unary_expression
	| '--' unary_expression
	| unary_operator cast_expression
	| 'sizeof' unary_expression
	| 'sizeof' '(' type_name ')'
	;

postfix_expression
	:   primary_expression
        (   '[' expression ']'
        |   '(' ')'
        |   '(' argument_expression_list ')'
        |   '.' IDENTIFIER
        |   '->' IDENTIFIER
        |   '++'
        |   '--'
        )*
	;

unary_operator
	: '&'
	| '*'
	| '+'
	| '-'
	| '~'
	| '!'
	;

primary_expression
	: IDENTIFIER
	| constant
	| '(' expression ')'
	;

constant
    :   HEX_LITERAL
    |   OCTAL_LITERAL
    |   DECIMAL_LITERAL
    |	CHARACTER_LITERAL
	|	STRING_LITERAL
    |   FLOATING_POINT_LITERAL
    ;


///// 

expression
	: assignment_expression (',' assignment_expression)*
	;

constant_expression
	: conditional_expression
	;

assignment_expression
	: lvalue assignment_operator assignment_expression
	| conditional_expression
	;
	
lvalue
	:	unary_expression
	;

assignment_operator
	: '='
	| '*='
	| '/='
	| '%='
	| '+='
	| '-='
	| '<<='
	| '>>='
	| '&='
	| '^='
	| '|='
	;

conditional_expression
	: logical_or_expression ('?' expression ':' conditional_expression)?
	;

logical_or_expression
	: logical_and_expression ('||' logical_and_expression)*
	;

logical_and_expression
	: inclusive_or_expression ('&&' inclusive_or_expression)*
	;

inclusive_or_expression
	: exclusive_or_expression ('|' exclusive_or_expression)*
	;

exclusive_or_expression
	: and_expression ('^' and_expression)*
	;

and_expression
	: equality_expression ('&' equality_expression)*
	;
equality_expression
	: relational_expression (('=='|'!=') relational_expression)*
	;

relational_expression
	: shift_expression (('<'|'>'|'<='|'>=') shift_expression)*
	;

shift_expression
	: additive_expression (('<<'|'>>') additive_expression)*
	;


// S t a t e m e n t s

statement
	: labeled_statement
	| compound_statement
	| expression_statement
	| selection_statement
	| iteration_statement
	| jump_statement
	;

labeled_statement
	: IDENTIFIER ':' statement
	| 'case' constant_expression ':' statement
	| 'default' ':' statement
	;

compound_statement
scope Symbols; // blocks have a scope of symbols
@init {
  $Symbols::types = new HashSet();
}
	: '{' (declaration*)! statement_list? '}'
	;

statement_list
	: statement+
	;

expression_statement
	: ';'!
	| expression ';'!
	;

selection_statement
	: 'if' '(' expression ')' statement (options {k=1; backtrack=false;}:'else' statement)?
	| 'switch' '(' expression ')' statement
	;

iteration_statement
	: 'while' '(' expression ')' statement
	| 'do' statement 'while' '(' expression ')' ';'!
	| 'for' '(' expression_statement expression_statement expression? ')' statement
	;

jump_statement
	: 'goto' IDENTIFIER ';'!
	| 'continue' ';'!
	| 'break' ';'!
	| 'return' ';'!
	| 'return' expression ';'!
	;



// LEXER LEKSER

IDENTIFIER
	:	LETTER (LETTER|'0'..'9')*
	;
	
fragment
LETTER
	:	'$'
	|	'A'..'Z'
	|	'a'..'z'
	|	'_'
	;

CHARACTER_LITERAL
    :   '\'' ( EscapeSequence | ~('\''|'\\') ) '\''
    ;

STRING_LITERAL
    :  '"' ( EscapeSequence | ~('\\'|'"') )* '"'
    ;

HEX_LITERAL : '0' ('x'|'X') HexDigit+ IntegerTypeSuffix? ;

DECIMAL_LITERAL : ('0' | '1'..'9' '0'..'9'*) IntegerTypeSuffix? ;

OCTAL_LITERAL : '0' ('0'..'7')+ IntegerTypeSuffix? ;

fragment
HexDigit : ('0'..'9'|'a'..'f'|'A'..'F') ;

fragment
IntegerTypeSuffix
	:	('u'|'U')? ('l'|'L')
	|	('u'|'U')  ('l'|'L')?
	;

FLOATING_POINT_LITERAL
    :   ('0'..'9')+ '.' ('0'..'9')* Exponent? FloatTypeSuffix?
    |   '.' ('0'..'9')+ Exponent? FloatTypeSuffix?
    |   ('0'..'9')+ Exponent FloatTypeSuffix?
    |   ('0'..'9')+ Exponent? FloatTypeSuffix
	;

fragment
Exponent : ('e'|'E') ('+'|'-')? ('0'..'9')+ ;

fragment
FloatTypeSuffix : ('f'|'F'|'d'|'D') ;

fragment
EscapeSequence
    :   '\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\')
    |   OctalEscape
    ;

fragment
OctalEscape
    :   '\\' ('0'..'3') ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7')
    ;

fragment
UnicodeEscape
    :   '\\' 'u' HexDigit HexDigit HexDigit HexDigit
    ;

WS  :  (' '|'\r'|'\t'|'\u000C'|'\n') {$channel=HIDDEN;}
    ;

COMMENT
    :  
    {
    int lineNo = getLine();
    System.out.println("lineNo: " + lineNo);
    int startInLineOffset = getCharPositionInLine(); 
    int startGlobalOffset = getCharIndex();
    }
    
     '/*' ( options {greedy=false;} : . )* '*/' 
     {
     $channel=HIDDEN;
     addComment(startGlobalOffset, startInLineOffset, lineNo, getText());
     
     }
    ;
    
    //	private void addComment(int startOffset, int lineNo, String text){

LINE_COMMENT
    : {
    int lineNo = getLine();
    System.out.println("lineNo: " + lineNo);
    int startInLineOffset = getCharPositionInLine(); 
    int startGlobalOffset = getCharIndex();
    }
    lc=('//' ~('\n'|'\r')* '\r'? '\n') 
    	{	$channel=HIDDEN; 
    		//System.out.println("kk: " + getLine()+", " + getCharPositionInLine() + ", " + getCharIndex() + ", " + getText());
    		addComment(startGlobalOffset, startInLineOffset, lineNo, getText());
 
//private void addLineComment(int startGlobalOffset, int startLineOffset, int lineNo, String text){    		
    	}
    ;

// ignore #line info for now
LINE_COMMAND 
    : '#' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    ;
