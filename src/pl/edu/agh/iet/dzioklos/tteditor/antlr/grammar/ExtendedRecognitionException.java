package pl.edu.agh.iet.dzioklos.tteditor.antlr.grammar;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import pl.edu.agh.iet.dzioklos.tteditor.beans.CError;

public class ExtendedRecognitionException extends Exception {

	private static final long serialVersionUID = 1L;
	private List<CError> errorsList;

	public ExtendedRecognitionException(List<CError> errorsList) {
		this.errorsList = errorsList;
	}

	public ExtendedRecognitionException(CError error) {
		this.errorsList = new LinkedList<CError>();
		errorsList.add(error);
	}

	public List<CError> getErrorsList() {
		return Collections.unmodifiableList(errorsList);
	}

	@Deprecated
	public int getLineNo() {
		return errorsList.get(0).getLineNo();
	}

	@Deprecated
	public String getParserMsg() {
		return errorsList.get(0).getParserMsg();
	}

	@Deprecated
	public int getColumnNo() {
		return errorsList.get(0).getColumnNo();
	}

}
