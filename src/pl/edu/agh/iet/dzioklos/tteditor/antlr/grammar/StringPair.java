package pl.edu.agh.iet.dzioklos.tteditor.antlr.grammar;

public class StringPair {

	private String string1;
	private String string2;

	public String getString1() {
		return string1;
	}

	public String getString2() {
		return string2;
	}

	public StringPair(String string1, String string2) {
		super();
		this.string1 = string1;
		this.string2 = string2;
	}

}
