// $ANTLR 3.4 D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g 2013-06-21 10:47:52

package pl.edu.agh.iet.dzioklos.tteditor.antlr.grammar;

import java.util.Set; 
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;

import pl.edu.agh.iet.dzioklos.tteditor.beans.CElement;
import pl.edu.agh.iet.dzioklos.tteditor.beans.CElement.Type;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import pl.edu.agh.iet.dzioklos.tteditor.beans.CError;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.tree.*;


@SuppressWarnings({"all", "warnings", "unchecked"})
public class CSampleParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "CHARACTER_LITERAL", "COMMENT", "DECIMAL_LITERAL", "EscapeSequence", "Exponent", "FLOATING_POINT_LITERAL", "FloatTypeSuffix", "HEX_LITERAL", "HexDigit", "IDENTIFIER", "IntegerTypeSuffix", "LETTER", "LINE_COMMAND", "LINE_COMMENT", "OCTAL_LITERAL", "OctalEscape", "STRING_LITERAL", "UnicodeEscape", "WS", "'!'", "'!='", "'%'", "'%='", "'&&'", "'&'", "'&='", "'('", "')'", "'*'", "'*='", "'+'", "'++'", "'+='", "','", "'-'", "'--'", "'-='", "'->'", "'.'", "'...'", "'/'", "'/='", "':'", "';'", "'<'", "'<<'", "'<<='", "'<='", "'='", "'=='", "'>'", "'>='", "'>>'", "'>>='", "'?'", "'['", "']'", "'^'", "'^='", "'auto'", "'break'", "'case'", "'char'", "'const'", "'continue'", "'default'", "'do'", "'double'", "'else'", "'enum'", "'extern'", "'float'", "'for'", "'goto'", "'if'", "'int'", "'long'", "'register'", "'return'", "'short'", "'signed'", "'sizeof'", "'static'", "'struct'", "'switch'", "'typedef'", "'union'", "'unsigned'", "'void'", "'volatile'", "'while'", "'{'", "'|'", "'|='", "'||'", "'}'", "'~'"
    };

    public static final int EOF=-1;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__50=50;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__59=59;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__64=64;
    public static final int T__65=65;
    public static final int T__66=66;
    public static final int T__67=67;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int T__73=73;
    public static final int T__74=74;
    public static final int T__75=75;
    public static final int T__76=76;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int T__80=80;
    public static final int T__81=81;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int T__84=84;
    public static final int T__85=85;
    public static final int T__86=86;
    public static final int T__87=87;
    public static final int T__88=88;
    public static final int T__89=89;
    public static final int T__90=90;
    public static final int T__91=91;
    public static final int T__92=92;
    public static final int T__93=93;
    public static final int T__94=94;
    public static final int T__95=95;
    public static final int T__96=96;
    public static final int T__97=97;
    public static final int T__98=98;
    public static final int T__99=99;
    public static final int T__100=100;
    public static final int CHARACTER_LITERAL=4;
    public static final int COMMENT=5;
    public static final int DECIMAL_LITERAL=6;
    public static final int EscapeSequence=7;
    public static final int Exponent=8;
    public static final int FLOATING_POINT_LITERAL=9;
    public static final int FloatTypeSuffix=10;
    public static final int HEX_LITERAL=11;
    public static final int HexDigit=12;
    public static final int IDENTIFIER=13;
    public static final int IntegerTypeSuffix=14;
    public static final int LETTER=15;
    public static final int LINE_COMMAND=16;
    public static final int LINE_COMMENT=17;
    public static final int OCTAL_LITERAL=18;
    public static final int OctalEscape=19;
    public static final int STRING_LITERAL=20;
    public static final int UnicodeEscape=21;
    public static final int WS=22;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators

    protected static class Symbols_scope {
        Set types;
        // only track types in order to get parser working;
    }
    protected Stack Symbols_stack = new Stack();



    public CSampleParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public CSampleParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
        this.state.ruleMemo = new HashMap[214+1];
         

    }

protected TreeAdaptor adaptor = new CommonTreeAdaptor();

public void setTreeAdaptor(TreeAdaptor adaptor) {
    this.adaptor = adaptor;
}
public TreeAdaptor getTreeAdaptor() {
    return adaptor;
}
    public String[] getTokenNames() { return CSampleParser.tokenNames; }
    public String getGrammarFileName() { return "D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g"; }


    	boolean isTypeName(String name) {
    		for (int i = Symbols_stack.size()-1; i>=0; i--) {
    			Symbols_scope scope = (Symbols_scope)Symbols_stack.get(i);
    			if ( scope.types.contains(name) ) {
    				return true;
    			}
    		}
    		return false; 
    	}
    	
    	private List<CError> errorsList = new ArrayList<CError>();

    	public List<CError> getErrorsList(){
    		return errorsList;
    	}

    	private void addCError(String msg) {
    //		System.out.println("ERROR:" + msg);
    		Pattern pattern = Pattern.compile("line (\\d+):(\\d+) (.*)");
    		Matcher matcher = pattern.matcher(msg.trim());
    		matcher.find();
    		
    		int line = Integer.parseInt(matcher.group(1));
    		int column = Integer.parseInt(matcher.group(2));
    		String message = matcher.group(3);
    		
    		
    		
    		CError error = new CError(line, column, message);
    		errorsList.add(error);
    //		System.out.println("EEEEEE: " + error);
    //		System.out.println("msg: " + msg);
    	}



    //	@Overrride
    	public void emitErrorMessage(String msg) {
    		addCError(msg);
    //        System.out.println("MSG_ERROR: " + msg);
        }
    //	public void displayRecognitionError(String[] tokenNames,
    //                                        RecognitionException e) {
    //        String hdr = getErrorHeader(e);
    //        String msg = getErrorMessage(e, tokenNames);
    //    }
    	
    	private List<CElement> elements = new ArrayList<CElement>();
    	
    	public List<CElement> getCElements(){
    		return elements;
    	}
    	
    	void addComments(List<CElement> comments){
    		elements.addAll(comments);
    	}
    	
    	private void addCElement(CElement element){
    		elements.add(element);
    	}
    	private boolean isDEclaredTypeWithName(Type type, String name){
    		for (CElement elem: elements){
    			if (elem.getType() == type && elem.getName().trim().equals(name.trim()) )
    				return true;
    		}
    		return false;
    	}
    	
    	private void addFunction(String nameWithArgs, String returnType, int lineNo) {
    		if (isDEclaredTypeWithName(Type.FUNCTION, nameWithArgs)){
    			CError error = new CError(lineNo, 0, "not unique name for function: " + nameWithArgs);
    			errorsList.add(error);
    			return;
    		}

    		CElement elem = new CElement();
    		elem.setType(Type.FUNCTION);
    		elem.setLineNo( lineNo);
    		elem.setReturnType( returnType!=null ? returnType : "unkwn");
    		if (nameWithArgs == null){
    			elem.setName("unkwn");
    		}else{
    		elem.setName(nameWithArgs);
    		}	
    		addCElement(elem);
    	}
    	
    	private void addStruct(List<StringPair> pairs, String name, int lineNo){
    		CElement elem = new CElement();
    		elem.setType(Type.STRUCT);
    		elem.setName(name);
    		elem.setLineNo(lineNo);
    		
    		for (StringPair sp : pairs){
    			elem.addParam(sp.getString2(), sp.getString1());
    		}		
    		addCElement(elem);
    	}

    	void addGlobalVariable(String name, String returnType, int lineNo){
    //		if (isDEclaredTypeWithName(Type.GLOBAL_VARIABLE, name)){
    //			CError error = new CError(lineNo, 0, "not unique name for global variable: " + name);
    //			errorsList.add(error);
    //			return;
    //		}
    	
    	
    		CElement elem = new CElement();
    		elem.setType(Type.GLOBAL_VARIABLE);
    		elem.setLineNo(lineNo);
    		elem.setReturnType(returnType);
    		elem.setName(name);
    		addCElement(elem);
    	}
    	
    	private List<StringPair> pairsList;
    	private boolean isInFunction = false;


    public static class program_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "program"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:203:1: program : translation_unit ;
    public final CSampleParser.program_return program() throws RecognitionException {
        CSampleParser.program_return retval = new CSampleParser.program_return();
        retval.start = input.LT(1);

        int program_StartIndex = input.index();

        CommonTree root_0 = null;

        CSampleParser.translation_unit_return translation_unit1 =null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 1) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:204:2: ( translation_unit )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:204:4: translation_unit
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_translation_unit_in_program125);
            translation_unit1=translation_unit();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, translation_unit1.getTree());

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 1, program_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "program"


    public static class translation_unit_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "translation_unit"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:207:1: translation_unit : ( external_declaration )+ ;
    public final CSampleParser.translation_unit_return translation_unit() throws RecognitionException {
        Symbols_stack.push(new Symbols_scope());

        CSampleParser.translation_unit_return retval = new CSampleParser.translation_unit_return();
        retval.start = input.LT(1);

        int translation_unit_StartIndex = input.index();

        CommonTree root_0 = null;

        CSampleParser.external_declaration_return external_declaration2 =null;




          ((Symbols_scope)Symbols_stack.peek()).types = new HashSet();

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 2) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:212:2: ( ( external_declaration )+ )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:212:4: ( external_declaration )+
            {
            root_0 = (CommonTree)adaptor.nil();


            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:212:4: ( external_declaration )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==IDENTIFIER||LA1_0==30||LA1_0==32||LA1_0==63||(LA1_0 >= 66 && LA1_0 <= 67)||LA1_0==71||(LA1_0 >= 73 && LA1_0 <= 75)||(LA1_0 >= 79 && LA1_0 <= 81)||(LA1_0 >= 83 && LA1_0 <= 84)||(LA1_0 >= 86 && LA1_0 <= 87)||(LA1_0 >= 89 && LA1_0 <= 93)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:212:4: external_declaration
            	    {
            	    pushFollow(FOLLOW_external_declaration_in_translation_unit147);
            	    external_declaration2=external_declaration();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, external_declaration2.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
            	    if (state.backtracking>0) {state.failed=true; return retval;}
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 2, translation_unit_StartIndex); }

            Symbols_stack.pop();

        }
        return retval;
    }
    // $ANTLR end "translation_unit"


    public static class external_declaration_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "external_declaration"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:217:1: external_declaration options {k=2; } : ( ( ( ( declaration_specifiers )? ) ! ( declarator ( declaration )* ) ! '{' !)=> function_definition !| declaration );
    public final CSampleParser.external_declaration_return external_declaration() throws RecognitionException {
        CSampleParser.external_declaration_return retval = new CSampleParser.external_declaration_return();
        retval.start = input.LT(1);

        int external_declaration_StartIndex = input.index();

        CommonTree root_0 = null;

        CSampleParser.function_definition_return function_definition3 =null;

        CSampleParser.declaration_return declaration4 =null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 3) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:219:2: ( ( ( ( declaration_specifiers )? ) ! ( declarator ( declaration )* ) ! '{' !)=> function_definition !| declaration )
            int alt2=2;
            alt2 = dfa2.predict(input);
            switch (alt2) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:219:4: ( ( ( declaration_specifiers )? ) ! ( declarator ( declaration )* ) ! '{' !)=> function_definition !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_function_definition_in_external_declaration191);
                    function_definition3=function_definition();

                    state._fsp--;
                    if (state.failed) return retval;

                    if ( state.backtracking==0 ) {isInFunction = false;
                    				System.out.println("IN2: " + isInFunction);
                    				}

                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:222:4: declaration
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_declaration_in_external_declaration199);
                    declaration4=declaration();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, declaration4.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 3, external_declaration_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "external_declaration"


    public static class function_definition_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "function_definition"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:227:1: function_definition : ( declaration_specifiers )? declarator ( ( declaration )+ compound_statement | compound_statement ) ;
    public final CSampleParser.function_definition_return function_definition() throws RecognitionException {
        Symbols_stack.push(new Symbols_scope());

        CSampleParser.function_definition_return retval = new CSampleParser.function_definition_return();
        retval.start = input.LT(1);

        int function_definition_StartIndex = input.index();

        CommonTree root_0 = null;

        CSampleParser.declaration_specifiers_return declaration_specifiers5 =null;

        CSampleParser.declarator_return declarator6 =null;

        CSampleParser.declaration_return declaration7 =null;

        CSampleParser.compound_statement_return compound_statement8 =null;

        CSampleParser.compound_statement_return compound_statement9 =null;




          ((Symbols_scope)Symbols_stack.peek()).types = new HashSet();

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 4) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:232:2: ( ( declaration_specifiers )? declarator ( ( declaration )+ compound_statement | compound_statement ) )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:233:2: ( declaration_specifiers )? declarator ( ( declaration )+ compound_statement | compound_statement )
            {
            root_0 = (CommonTree)adaptor.nil();


            if ( state.backtracking==0 ) {
            				isInFunction = true;
            	}

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:237:2: ( declaration_specifiers )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==63||(LA3_0 >= 66 && LA3_0 <= 67)||LA3_0==71||(LA3_0 >= 73 && LA3_0 <= 75)||(LA3_0 >= 79 && LA3_0 <= 81)||(LA3_0 >= 83 && LA3_0 <= 84)||(LA3_0 >= 86 && LA3_0 <= 87)||(LA3_0 >= 90 && LA3_0 <= 93)) ) {
                alt3=1;
            }
            else if ( (LA3_0==IDENTIFIER) ) {
                switch ( input.LA(2) ) {
                    case 32:
                        {
                        alt3=1;
                        }
                        break;
                    case IDENTIFIER:
                        {
                        int LA3_18 = input.LA(3);

                        if ( (((synpred5_CSample()&&synpred5_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                            alt3=1;
                        }
                        }
                        break;
                    case 30:
                        {
                        int LA3_19 = input.LA(3);

                        if ( (((synpred5_CSample()&&synpred5_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                            alt3=1;
                        }
                        }
                        break;
                    case 63:
                    case 74:
                    case 81:
                    case 86:
                        {
                        int LA3_20 = input.LA(3);

                        if ( (((synpred5_CSample()&&synpred5_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                            alt3=1;
                        }
                        }
                        break;
                    case 92:
                        {
                        int LA3_21 = input.LA(3);

                        if ( (((synpred5_CSample()&&synpred5_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                            alt3=1;
                        }
                        }
                        break;
                    case 66:
                        {
                        int LA3_22 = input.LA(3);

                        if ( (((synpred5_CSample()&&synpred5_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                            alt3=1;
                        }
                        }
                        break;
                    case 83:
                        {
                        int LA3_23 = input.LA(3);

                        if ( (((synpred5_CSample()&&synpred5_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                            alt3=1;
                        }
                        }
                        break;
                    case 79:
                        {
                        int LA3_24 = input.LA(3);

                        if ( (((synpred5_CSample()&&synpred5_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                            alt3=1;
                        }
                        }
                        break;
                    case 80:
                        {
                        int LA3_25 = input.LA(3);

                        if ( (((synpred5_CSample()&&synpred5_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                            alt3=1;
                        }
                        }
                        break;
                    case 75:
                        {
                        int LA3_26 = input.LA(3);

                        if ( (((synpred5_CSample()&&synpred5_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                            alt3=1;
                        }
                        }
                        break;
                    case 71:
                        {
                        int LA3_27 = input.LA(3);

                        if ( (((synpred5_CSample()&&synpred5_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                            alt3=1;
                        }
                        }
                        break;
                    case 84:
                        {
                        int LA3_28 = input.LA(3);

                        if ( (((synpred5_CSample()&&synpred5_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                            alt3=1;
                        }
                        }
                        break;
                    case 91:
                        {
                        int LA3_29 = input.LA(3);

                        if ( (((synpred5_CSample()&&synpred5_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                            alt3=1;
                        }
                        }
                        break;
                    case 87:
                    case 90:
                        {
                        int LA3_30 = input.LA(3);

                        if ( (((synpred5_CSample()&&synpred5_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                            alt3=1;
                        }
                        }
                        break;
                    case 73:
                        {
                        int LA3_31 = input.LA(3);

                        if ( (((synpred5_CSample()&&synpred5_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                            alt3=1;
                        }
                        }
                        break;
                    case 67:
                    case 93:
                        {
                        int LA3_32 = input.LA(3);

                        if ( (((synpred5_CSample()&&synpred5_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                            alt3=1;
                        }
                        }
                        break;
                }

            }
            switch (alt3) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:237:2: declaration_specifiers
                    {
                    pushFollow(FOLLOW_declaration_specifiers_in_function_definition230);
                    declaration_specifiers5=declaration_specifiers();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, declaration_specifiers5.getTree());

                    }
                    break;

            }


            pushFollow(FOLLOW_declarator_in_function_definition233);
            declarator6=declarator();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, declarator6.getTree());

            if ( state.backtracking==0 ) {
            				System.out.println("IN1: " + isInFunction);
            				addFunction((declarator6!=null?input.toString(declarator6.start,declarator6.stop):null), (declaration_specifiers5!=null?input.toString(declaration_specifiers5.start,declaration_specifiers5.stop):null), (declarator6!=null?((Token)declarator6.start):null).getLine());
            				
            				}

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:242:3: ( ( declaration )+ compound_statement | compound_statement )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==IDENTIFIER||LA5_0==63||(LA5_0 >= 66 && LA5_0 <= 67)||LA5_0==71||(LA5_0 >= 73 && LA5_0 <= 75)||(LA5_0 >= 79 && LA5_0 <= 81)||(LA5_0 >= 83 && LA5_0 <= 84)||(LA5_0 >= 86 && LA5_0 <= 87)||(LA5_0 >= 89 && LA5_0 <= 93)) ) {
                alt5=1;
            }
            else if ( (LA5_0==95) ) {
                alt5=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;

            }
            switch (alt5) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:242:5: ( declaration )+ compound_statement
                    {
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:242:5: ( declaration )+
                    int cnt4=0;
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==IDENTIFIER||LA4_0==63||(LA4_0 >= 66 && LA4_0 <= 67)||LA4_0==71||(LA4_0 >= 73 && LA4_0 <= 75)||(LA4_0 >= 79 && LA4_0 <= 81)||(LA4_0 >= 83 && LA4_0 <= 84)||(LA4_0 >= 86 && LA4_0 <= 87)||(LA4_0 >= 89 && LA4_0 <= 93)) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:242:5: declaration
                    	    {
                    	    pushFollow(FOLLOW_declaration_in_function_definition241);
                    	    declaration7=declaration();

                    	    state._fsp--;
                    	    if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) adaptor.addChild(root_0, declaration7.getTree());

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt4 >= 1 ) break loop4;
                    	    if (state.backtracking>0) {state.failed=true; return retval;}
                                EarlyExitException eee =
                                    new EarlyExitException(4, input);
                                throw eee;
                        }
                        cnt4++;
                    } while (true);


                    pushFollow(FOLLOW_compound_statement_in_function_definition244);
                    compound_statement8=compound_statement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, compound_statement8.getTree());

                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:243:5: compound_statement
                    {
                    pushFollow(FOLLOW_compound_statement_in_function_definition251);
                    compound_statement9=compound_statement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, compound_statement9.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 4, function_definition_StartIndex); }

            Symbols_stack.pop();

        }
        return retval;
    }
    // $ANTLR end "function_definition"


    protected static class declaration_scope {
        boolean isTypedef;
    }
    protected Stack declaration_stack = new Stack();


    public static class declaration_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "declaration"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:247:1: declaration : ( 'typedef' ( declaration_specifiers )? init_declarator_list ';' !|op2a= declaration_specifiers ( (op2b= init_declarator_list )? ) ';' !);
    public final CSampleParser.declaration_return declaration() throws RecognitionException {
        declaration_stack.push(new declaration_scope());
        CSampleParser.declaration_return retval = new CSampleParser.declaration_return();
        retval.start = input.LT(1);

        int declaration_StartIndex = input.index();

        CommonTree root_0 = null;

        Token string_literal10=null;
        Token char_literal13=null;
        Token char_literal14=null;
        CSampleParser.declaration_specifiers_return op2a =null;

        CSampleParser.init_declarator_list_return op2b =null;

        CSampleParser.declaration_specifiers_return declaration_specifiers11 =null;

        CSampleParser.init_declarator_list_return init_declarator_list12 =null;


        CommonTree string_literal10_tree=null;
        CommonTree char_literal13_tree=null;
        CommonTree char_literal14_tree=null;


          ((declaration_scope)declaration_stack.peek()).isTypedef = false;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 5) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:254:2: ( 'typedef' ( declaration_specifiers )? init_declarator_list ';' !|op2a= declaration_specifiers ( (op2b= init_declarator_list )? ) ';' !)
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==89) ) {
                alt8=1;
            }
            else if ( (LA8_0==IDENTIFIER||LA8_0==63||(LA8_0 >= 66 && LA8_0 <= 67)||LA8_0==71||(LA8_0 >= 73 && LA8_0 <= 75)||(LA8_0 >= 79 && LA8_0 <= 81)||(LA8_0 >= 83 && LA8_0 <= 84)||(LA8_0 >= 86 && LA8_0 <= 87)||(LA8_0 >= 90 && LA8_0 <= 93)) ) {
                alt8=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;

            }
            switch (alt8) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:254:4: 'typedef' ( declaration_specifiers )? init_declarator_list ';' !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal10=(Token)match(input,89,FOLLOW_89_in_declaration280); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal10_tree = 
                    (CommonTree)adaptor.create(string_literal10)
                    ;
                    adaptor.addChild(root_0, string_literal10_tree);
                    }

                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:254:14: ( declaration_specifiers )?
                    int alt6=2;
                    int LA6_0 = input.LA(1);

                    if ( (LA6_0==63||(LA6_0 >= 66 && LA6_0 <= 67)||LA6_0==71||(LA6_0 >= 73 && LA6_0 <= 75)||(LA6_0 >= 79 && LA6_0 <= 81)||(LA6_0 >= 83 && LA6_0 <= 84)||(LA6_0 >= 86 && LA6_0 <= 87)||(LA6_0 >= 90 && LA6_0 <= 93)) ) {
                        alt6=1;
                    }
                    else if ( (LA6_0==IDENTIFIER) ) {
                        int LA6_13 = input.LA(2);

                        if ( (LA6_13==IDENTIFIER||LA6_13==32||LA6_13==63||(LA6_13 >= 66 && LA6_13 <= 67)||LA6_13==71||(LA6_13 >= 73 && LA6_13 <= 75)||(LA6_13 >= 79 && LA6_13 <= 81)||(LA6_13 >= 83 && LA6_13 <= 84)||(LA6_13 >= 86 && LA6_13 <= 87)||(LA6_13 >= 90 && LA6_13 <= 93)) ) {
                            alt6=1;
                        }
                        else if ( (LA6_13==30) ) {
                            int LA6_19 = input.LA(3);

                            if ( (((synpred8_CSample()&&synpred8_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                                alt6=1;
                            }
                        }
                    }
                    switch (alt6) {
                        case 1 :
                            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:254:14: declaration_specifiers
                            {
                            pushFollow(FOLLOW_declaration_specifiers_in_declaration282);
                            declaration_specifiers11=declaration_specifiers();

                            state._fsp--;
                            if (state.failed) return retval;
                            if ( state.backtracking==0 ) adaptor.addChild(root_0, declaration_specifiers11.getTree());

                            }
                            break;

                    }


                    if ( state.backtracking==0 ) {((declaration_scope)declaration_stack.peek()).isTypedef =true;}

                    pushFollow(FOLLOW_init_declarator_list_in_declaration290);
                    init_declarator_list12=init_declarator_list();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, init_declarator_list12.getTree());

                    char_literal13=(Token)match(input,47,FOLLOW_47_in_declaration292); if (state.failed) return retval;

                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:256:4: op2a= declaration_specifiers ( (op2b= init_declarator_list )? ) ';' !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_declaration_specifiers_in_declaration301);
                    op2a=declaration_specifiers();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, op2a.getTree());

                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:256:32: ( (op2b= init_declarator_list )? )
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:256:33: (op2b= init_declarator_list )?
                    {
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:256:37: (op2b= init_declarator_list )?
                    int alt7=2;
                    int LA7_0 = input.LA(1);

                    if ( (LA7_0==IDENTIFIER||LA7_0==30||LA7_0==32) ) {
                        alt7=1;
                    }
                    switch (alt7) {
                        case 1 :
                            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:256:37: op2b= init_declarator_list
                            {
                            pushFollow(FOLLOW_init_declarator_list_in_declaration306);
                            op2b=init_declarator_list();

                            state._fsp--;
                            if (state.failed) return retval;
                            if ( state.backtracking==0 ) adaptor.addChild(root_0, op2b.getTree());

                            }
                            break;

                    }


                    }


                    char_literal14=(Token)match(input,47,FOLLOW_47_in_declaration310); if (state.failed) return retval;

                    if ( state.backtracking==0 ) {
                    		if (!(op2a!=null?op2a.isStruct:false)){
                    			if (!isInFunction){
                    				System.out.println("ADD: " + isInFunction);
                    				addGlobalVariable((op2b!=null?input.toString(op2b.start,op2b.stop):null), (op2a!=null?input.toString(op2a.start,op2a.stop):null), (op2a!=null?((Token)op2a.start):null).getLine() );
                    			}
                    		}
                    	}

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 5, declaration_StartIndex); }

            declaration_stack.pop();
        }
        return retval;
    }
    // $ANTLR end "declaration"


    public static class declaration_specifiers_return extends ParserRuleReturnScope {
        public boolean isStruct;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "declaration_specifiers"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:267:1: declaration_specifiers returns [boolean isStruct] : ( storage_class_specifier | type_specifier | type_qualifier )+ ;
    public final CSampleParser.declaration_specifiers_return declaration_specifiers() throws RecognitionException {
        CSampleParser.declaration_specifiers_return retval = new CSampleParser.declaration_specifiers_return();
        retval.start = input.LT(1);

        int declaration_specifiers_StartIndex = input.index();

        CommonTree root_0 = null;

        CSampleParser.storage_class_specifier_return storage_class_specifier15 =null;

        CSampleParser.type_specifier_return type_specifier16 =null;

        CSampleParser.type_qualifier_return type_qualifier17 =null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 6) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:268:2: ( ( storage_class_specifier | type_specifier | type_qualifier )+ )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:268:6: ( storage_class_specifier | type_specifier | type_qualifier )+
            {
            root_0 = (CommonTree)adaptor.nil();


            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:268:6: ( storage_class_specifier | type_specifier | type_qualifier )+
            int cnt9=0;
            loop9:
            do {
                int alt9=4;
                switch ( input.LA(1) ) {
                case IDENTIFIER:
                    {
                    int LA9_2 = input.LA(2);

                    if ( (((synpred12_CSample()&&synpred12_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                        alt9=2;
                    }


                    }
                    break;
                case 63:
                case 74:
                case 81:
                case 86:
                    {
                    alt9=1;
                    }
                    break;
                case 66:
                case 71:
                case 73:
                case 75:
                case 79:
                case 80:
                case 83:
                case 84:
                case 87:
                case 90:
                case 91:
                case 92:
                    {
                    alt9=2;
                    }
                    break;
                case 67:
                case 93:
                    {
                    alt9=3;
                    }
                    break;

                }

                switch (alt9) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:268:10: storage_class_specifier
            	    {
            	    pushFollow(FOLLOW_storage_class_specifier_in_declaration_specifiers336);
            	    storage_class_specifier15=storage_class_specifier();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, storage_class_specifier15.getTree());

            	    }
            	    break;
            	case 2 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:269:6: type_specifier
            	    {
            	    pushFollow(FOLLOW_type_specifier_in_declaration_specifiers343);
            	    type_specifier16=type_specifier();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, type_specifier16.getTree());

            	    if ( state.backtracking==0 ) {retval.isStruct = (type_specifier16!=null?type_specifier16.isStruct:false);}

            	    }
            	    break;
            	case 3 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:270:13: type_qualifier
            	    {
            	    pushFollow(FOLLOW_type_qualifier_in_declaration_specifiers359);
            	    type_qualifier17=type_qualifier();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, type_qualifier17.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt9 >= 1 ) break loop9;
            	    if (state.backtracking>0) {state.failed=true; return retval;}
                        EarlyExitException eee =
                            new EarlyExitException(9, input);
                        throw eee;
                }
                cnt9++;
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 6, declaration_specifiers_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "declaration_specifiers"


    public static class init_declarator_list_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "init_declarator_list"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:274:1: init_declarator_list : init_declarator ( ',' init_declarator )* ;
    public final CSampleParser.init_declarator_list_return init_declarator_list() throws RecognitionException {
        CSampleParser.init_declarator_list_return retval = new CSampleParser.init_declarator_list_return();
        retval.start = input.LT(1);

        int init_declarator_list_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal19=null;
        CSampleParser.init_declarator_return init_declarator18 =null;

        CSampleParser.init_declarator_return init_declarator20 =null;


        CommonTree char_literal19_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 7) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:275:2: ( init_declarator ( ',' init_declarator )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:275:4: init_declarator ( ',' init_declarator )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_init_declarator_in_init_declarator_list381);
            init_declarator18=init_declarator();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, init_declarator18.getTree());

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:275:20: ( ',' init_declarator )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==37) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:275:21: ',' init_declarator
            	    {
            	    char_literal19=(Token)match(input,37,FOLLOW_37_in_init_declarator_list384); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal19_tree = 
            	    (CommonTree)adaptor.create(char_literal19)
            	    ;
            	    adaptor.addChild(root_0, char_literal19_tree);
            	    }

            	    pushFollow(FOLLOW_init_declarator_in_init_declarator_list386);
            	    init_declarator20=init_declarator();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, init_declarator20.getTree());

            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 7, init_declarator_list_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "init_declarator_list"


    public static class init_declarator_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "init_declarator"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:278:1: init_declarator : declarator ( '=' initializer )? ;
    public final CSampleParser.init_declarator_return init_declarator() throws RecognitionException {
        CSampleParser.init_declarator_return retval = new CSampleParser.init_declarator_return();
        retval.start = input.LT(1);

        int init_declarator_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal22=null;
        CSampleParser.declarator_return declarator21 =null;

        CSampleParser.initializer_return initializer23 =null;


        CommonTree char_literal22_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 8) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:279:2: ( declarator ( '=' initializer )? )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:279:4: declarator ( '=' initializer )?
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_declarator_in_init_declarator399);
            declarator21=declarator();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, declarator21.getTree());

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:279:15: ( '=' initializer )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==52) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:279:16: '=' initializer
                    {
                    char_literal22=(Token)match(input,52,FOLLOW_52_in_init_declarator402); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal22_tree = 
                    (CommonTree)adaptor.create(char_literal22)
                    ;
                    adaptor.addChild(root_0, char_literal22_tree);
                    }

                    pushFollow(FOLLOW_initializer_in_init_declarator404);
                    initializer23=initializer();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, initializer23.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 8, init_declarator_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "init_declarator"


    public static class storage_class_specifier_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "storage_class_specifier"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:282:1: storage_class_specifier : ( 'extern' | 'static' | 'auto' | 'register' );
    public final CSampleParser.storage_class_specifier_return storage_class_specifier() throws RecognitionException {
        CSampleParser.storage_class_specifier_return retval = new CSampleParser.storage_class_specifier_return();
        retval.start = input.LT(1);

        int storage_class_specifier_StartIndex = input.index();

        CommonTree root_0 = null;

        Token set24=null;

        CommonTree set24_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 9) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:283:2: ( 'extern' | 'static' | 'auto' | 'register' )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:
            {
            root_0 = (CommonTree)adaptor.nil();


            set24=(Token)input.LT(1);

            if ( input.LA(1)==63||input.LA(1)==74||input.LA(1)==81||input.LA(1)==86 ) {
                input.consume();
                if ( state.backtracking==0 ) adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(set24)
                );
                state.errorRecovery=false;
                state.failed=false;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 9, storage_class_specifier_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "storage_class_specifier"


    public static class type_specifier_return extends ParserRuleReturnScope {
        public boolean isStruct;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "type_specifier"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:289:1: type_specifier returns [boolean isStruct] : ( 'void' | 'char' | 'short' | 'int' | 'long' | 'float' | 'double' | 'signed' | 'unsigned' | struct_or_union_specifier | enum_specifier | type_id );
    public final CSampleParser.type_specifier_return type_specifier() throws RecognitionException {
        CSampleParser.type_specifier_return retval = new CSampleParser.type_specifier_return();
        retval.start = input.LT(1);

        int type_specifier_StartIndex = input.index();

        CommonTree root_0 = null;

        Token string_literal25=null;
        Token string_literal26=null;
        Token string_literal27=null;
        Token string_literal28=null;
        Token string_literal29=null;
        Token string_literal30=null;
        Token string_literal31=null;
        Token string_literal32=null;
        Token string_literal33=null;
        CSampleParser.struct_or_union_specifier_return struct_or_union_specifier34 =null;

        CSampleParser.enum_specifier_return enum_specifier35 =null;

        CSampleParser.type_id_return type_id36 =null;


        CommonTree string_literal25_tree=null;
        CommonTree string_literal26_tree=null;
        CommonTree string_literal27_tree=null;
        CommonTree string_literal28_tree=null;
        CommonTree string_literal29_tree=null;
        CommonTree string_literal30_tree=null;
        CommonTree string_literal31_tree=null;
        CommonTree string_literal32_tree=null;
        CommonTree string_literal33_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 10) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:290:2: ( 'void' | 'char' | 'short' | 'int' | 'long' | 'float' | 'double' | 'signed' | 'unsigned' | struct_or_union_specifier | enum_specifier | type_id )
            int alt12=12;
            switch ( input.LA(1) ) {
            case 92:
                {
                alt12=1;
                }
                break;
            case 66:
                {
                alt12=2;
                }
                break;
            case 83:
                {
                alt12=3;
                }
                break;
            case 79:
                {
                alt12=4;
                }
                break;
            case 80:
                {
                alt12=5;
                }
                break;
            case 75:
                {
                alt12=6;
                }
                break;
            case 71:
                {
                alt12=7;
                }
                break;
            case 84:
                {
                alt12=8;
                }
                break;
            case 91:
                {
                alt12=9;
                }
                break;
            case 87:
            case 90:
                {
                alt12=10;
                }
                break;
            case 73:
                {
                alt12=11;
                }
                break;
            case IDENTIFIER:
                {
                alt12=12;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;

            }

            switch (alt12) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:291:2: 'void'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    if ( state.backtracking==0 ) {retval.isStruct = false;}

                    string_literal25=(Token)match(input,92,FOLLOW_92_in_type_specifier452); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal25_tree = 
                    (CommonTree)adaptor.create(string_literal25)
                    ;
                    adaptor.addChild(root_0, string_literal25_tree);
                    }

                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:293:4: 'char'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal26=(Token)match(input,66,FOLLOW_66_in_type_specifier457); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal26_tree = 
                    (CommonTree)adaptor.create(string_literal26)
                    ;
                    adaptor.addChild(root_0, string_literal26_tree);
                    }

                    }
                    break;
                case 3 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:294:4: 'short'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal27=(Token)match(input,83,FOLLOW_83_in_type_specifier462); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal27_tree = 
                    (CommonTree)adaptor.create(string_literal27)
                    ;
                    adaptor.addChild(root_0, string_literal27_tree);
                    }

                    }
                    break;
                case 4 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:295:4: 'int'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal28=(Token)match(input,79,FOLLOW_79_in_type_specifier467); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal28_tree = 
                    (CommonTree)adaptor.create(string_literal28)
                    ;
                    adaptor.addChild(root_0, string_literal28_tree);
                    }

                    }
                    break;
                case 5 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:296:4: 'long'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal29=(Token)match(input,80,FOLLOW_80_in_type_specifier472); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal29_tree = 
                    (CommonTree)adaptor.create(string_literal29)
                    ;
                    adaptor.addChild(root_0, string_literal29_tree);
                    }

                    }
                    break;
                case 6 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:297:4: 'float'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal30=(Token)match(input,75,FOLLOW_75_in_type_specifier477); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal30_tree = 
                    (CommonTree)adaptor.create(string_literal30)
                    ;
                    adaptor.addChild(root_0, string_literal30_tree);
                    }

                    }
                    break;
                case 7 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:298:4: 'double'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal31=(Token)match(input,71,FOLLOW_71_in_type_specifier482); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal31_tree = 
                    (CommonTree)adaptor.create(string_literal31)
                    ;
                    adaptor.addChild(root_0, string_literal31_tree);
                    }

                    }
                    break;
                case 8 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:299:4: 'signed'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal32=(Token)match(input,84,FOLLOW_84_in_type_specifier487); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal32_tree = 
                    (CommonTree)adaptor.create(string_literal32)
                    ;
                    adaptor.addChild(root_0, string_literal32_tree);
                    }

                    }
                    break;
                case 9 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:300:4: 'unsigned'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal33=(Token)match(input,91,FOLLOW_91_in_type_specifier492); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal33_tree = 
                    (CommonTree)adaptor.create(string_literal33)
                    ;
                    adaptor.addChild(root_0, string_literal33_tree);
                    }

                    }
                    break;
                case 10 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:301:4: struct_or_union_specifier
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_struct_or_union_specifier_in_type_specifier497);
                    struct_or_union_specifier34=struct_or_union_specifier();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, struct_or_union_specifier34.getTree());

                    if ( state.backtracking==0 ) {retval.isStruct = true;}

                    }
                    break;
                case 11 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:302:4: enum_specifier
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_enum_specifier_in_type_specifier504);
                    enum_specifier35=enum_specifier();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, enum_specifier35.getTree());

                    if ( state.backtracking==0 ) {retval.isStruct = true;}

                    }
                    break;
                case 12 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:303:4: type_id
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_type_id_in_type_specifier511);
                    type_id36=type_id();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, type_id36.getTree());

                    if ( state.backtracking==0 ) {retval.isStruct = true;}

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 10, type_specifier_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "type_specifier"


    public static class type_id_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "type_id"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:306:1: type_id :{...}? IDENTIFIER ;
    public final CSampleParser.type_id_return type_id() throws RecognitionException {
        CSampleParser.type_id_return retval = new CSampleParser.type_id_return();
        retval.start = input.LT(1);

        int type_id_StartIndex = input.index();

        CommonTree root_0 = null;

        Token IDENTIFIER37=null;

        CommonTree IDENTIFIER37_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 11) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:307:5: ({...}? IDENTIFIER )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:307:9: {...}? IDENTIFIER
            {
            root_0 = (CommonTree)adaptor.nil();


            if ( !((isTypeName(input.LT(1).getText()))) ) {
                if (state.backtracking>0) {state.failed=true; return retval;}
                throw new FailedPredicateException(input, "type_id", "isTypeName(input.LT(1).getText())");
            }

            IDENTIFIER37=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_type_id531); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            IDENTIFIER37_tree = 
            (CommonTree)adaptor.create(IDENTIFIER37)
            ;
            adaptor.addChild(root_0, IDENTIFIER37_tree);
            }

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 11, type_id_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "type_id"


    public static class struct_or_union_specifier_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "struct_or_union_specifier"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:311:1: struct_or_union_specifier options {k=3; } : (op1= struct_or_union (op2= IDENTIFIER )? '{' op3= struct_declaration_list '}' | struct_or_union IDENTIFIER );
    public final CSampleParser.struct_or_union_specifier_return struct_or_union_specifier() throws RecognitionException {
        Symbols_stack.push(new Symbols_scope());

        CSampleParser.struct_or_union_specifier_return retval = new CSampleParser.struct_or_union_specifier_return();
        retval.start = input.LT(1);

        int struct_or_union_specifier_StartIndex = input.index();

        CommonTree root_0 = null;

        Token op2=null;
        Token char_literal38=null;
        Token char_literal39=null;
        Token IDENTIFIER41=null;
        CSampleParser.struct_or_union_return op1 =null;

        CSampleParser.struct_declaration_list_return op3 =null;

        CSampleParser.struct_or_union_return struct_or_union40 =null;


        CommonTree op2_tree=null;
        CommonTree char_literal38_tree=null;
        CommonTree char_literal39_tree=null;
        CommonTree IDENTIFIER41_tree=null;


          ((Symbols_scope)Symbols_stack.peek()).types = new HashSet();

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 12) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:317:2: (op1= struct_or_union (op2= IDENTIFIER )? '{' op3= struct_declaration_list '}' | struct_or_union IDENTIFIER )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==87||LA14_0==90) ) {
                int LA14_1 = input.LA(2);

                if ( (LA14_1==IDENTIFIER) ) {
                    int LA14_2 = input.LA(3);

                    if ( (LA14_2==95) ) {
                        alt14=1;
                    }
                    else if ( (LA14_2==EOF||LA14_2==IDENTIFIER||(LA14_2 >= 30 && LA14_2 <= 32)||LA14_2==37||(LA14_2 >= 46 && LA14_2 <= 47)||LA14_2==59||LA14_2==63||(LA14_2 >= 66 && LA14_2 <= 67)||LA14_2==71||(LA14_2 >= 73 && LA14_2 <= 75)||(LA14_2 >= 79 && LA14_2 <= 81)||(LA14_2 >= 83 && LA14_2 <= 84)||(LA14_2 >= 86 && LA14_2 <= 87)||(LA14_2 >= 90 && LA14_2 <= 93)) ) {
                        alt14=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return retval;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 14, 2, input);

                        throw nvae;

                    }
                }
                else if ( (LA14_1==95) ) {
                    alt14=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 14, 1, input);

                    throw nvae;

                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;

            }
            switch (alt14) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:317:4: op1= struct_or_union (op2= IDENTIFIER )? '{' op3= struct_declaration_list '}'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_struct_or_union_in_struct_or_union_specifier566);
                    op1=struct_or_union();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, op1.getTree());

                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:317:27: (op2= IDENTIFIER )?
                    int alt13=2;
                    int LA13_0 = input.LA(1);

                    if ( (LA13_0==IDENTIFIER) ) {
                        alt13=1;
                    }
                    switch (alt13) {
                        case 1 :
                            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:317:27: op2= IDENTIFIER
                            {
                            op2=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_struct_or_union_specifier570); if (state.failed) return retval;
                            if ( state.backtracking==0 ) {
                            op2_tree = 
                            (CommonTree)adaptor.create(op2)
                            ;
                            adaptor.addChild(root_0, op2_tree);
                            }

                            }
                            break;

                    }


                    char_literal38=(Token)match(input,95,FOLLOW_95_in_struct_or_union_specifier573); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal38_tree = 
                    (CommonTree)adaptor.create(char_literal38)
                    ;
                    adaptor.addChild(root_0, char_literal38_tree);
                    }

                    pushFollow(FOLLOW_struct_declaration_list_in_struct_or_union_specifier577);
                    op3=struct_declaration_list();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, op3.getTree());

                    char_literal39=(Token)match(input,99,FOLLOW_99_in_struct_or_union_specifier579); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal39_tree = 
                    (CommonTree)adaptor.create(char_literal39)
                    ;
                    adaptor.addChild(root_0, char_literal39_tree);
                    }

                    if ( state.backtracking==0 ) {
                    //		System.out.println("STRUCT ID:");
                    //		System.out.println((op2!=null?op2.getText():null));
                    		
                    //		System.out.println("STRUCT ELEMS: " + (op3!=null?op3.list:null).size());
                    		
                    //		System.out.println((op3!=null?op3.list:null).get(0).getString1());
                    //		System.out.println("STRUCT ELEMS: " + (op3!=null?op3.list:null).size());
                    		
                    //		System.out.println((op3!=null?input.toString(op3.start,op3.stop):null));
                    //		System.out.println("---STRUCT");
                    		addStruct((op3!=null?op3.list:null), (op2!=null?op2.getText():null), (op1!=null?((Token)op1.start):null).getLine());
                    		}

                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:331:4: struct_or_union IDENTIFIER
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_struct_or_union_in_struct_or_union_specifier589);
                    struct_or_union40=struct_or_union();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, struct_or_union40.getTree());

                    IDENTIFIER41=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_struct_or_union_specifier591); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    IDENTIFIER41_tree = 
                    (CommonTree)adaptor.create(IDENTIFIER41)
                    ;
                    adaptor.addChild(root_0, IDENTIFIER41_tree);
                    }

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 12, struct_or_union_specifier_StartIndex); }

            Symbols_stack.pop();

        }
        return retval;
    }
    // $ANTLR end "struct_or_union_specifier"


    public static class struct_or_union_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "struct_or_union"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:334:1: struct_or_union : ( 'struct' | 'union' );
    public final CSampleParser.struct_or_union_return struct_or_union() throws RecognitionException {
        CSampleParser.struct_or_union_return retval = new CSampleParser.struct_or_union_return();
        retval.start = input.LT(1);

        int struct_or_union_StartIndex = input.index();

        CommonTree root_0 = null;

        Token set42=null;

        CommonTree set42_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 13) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:335:2: ( 'struct' | 'union' )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:
            {
            root_0 = (CommonTree)adaptor.nil();


            set42=(Token)input.LT(1);

            if ( input.LA(1)==87||input.LA(1)==90 ) {
                input.consume();
                if ( state.backtracking==0 ) adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(set42)
                );
                state.errorRecovery=false;
                state.failed=false;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 13, struct_or_union_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "struct_or_union"


    public static class struct_declaration_list_return extends ParserRuleReturnScope {
        public List<StringPair> list;
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "struct_declaration_list"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:339:1: struct_declaration_list returns [List<StringPair> list] : (op= struct_declaration )+ ;
    public final CSampleParser.struct_declaration_list_return struct_declaration_list() throws RecognitionException {
        CSampleParser.struct_declaration_list_return retval = new CSampleParser.struct_declaration_list_return();
        retval.start = input.LT(1);

        int struct_declaration_list_StartIndex = input.index();

        CommonTree root_0 = null;

        CSampleParser.struct_declaration_return op =null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 14) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:340:2: ( (op= struct_declaration )+ )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:341:2: (op= struct_declaration )+
            {
            root_0 = (CommonTree)adaptor.nil();


            if ( state.backtracking==0 ) {
            //	System.out.println("INITqqqqqqqqqqqqq");
            	pairsList = new ArrayList<StringPair>();
            	}

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:345:4: (op= struct_declaration )+
            int cnt15=0;
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==IDENTIFIER||(LA15_0 >= 66 && LA15_0 <= 67)||LA15_0==71||LA15_0==73||LA15_0==75||(LA15_0 >= 79 && LA15_0 <= 80)||(LA15_0 >= 83 && LA15_0 <= 84)||LA15_0==87||(LA15_0 >= 90 && LA15_0 <= 93)) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:345:4: op= struct_declaration
            	    {
            	    pushFollow(FOLLOW_struct_declaration_in_struct_declaration_list628);
            	    op=struct_declaration();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, op.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt15 >= 1 ) break loop15;
            	    if (state.backtracking>0) {state.failed=true; return retval;}
                        EarlyExitException eee =
                            new EarlyExitException(15, input);
                        throw eee;
                }
                cnt15++;
            } while (true);


            if ( state.backtracking==0 ) {retval.list =pairsList; 
            //	System.out.println("ASSIGN");
            	}

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 14, struct_declaration_list_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "struct_declaration_list"


    public static class struct_declaration_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "struct_declaration"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:350:1: struct_declaration : op1= specifier_qualifier_list op2= struct_declarator_list ';' ;
    public final CSampleParser.struct_declaration_return struct_declaration() throws RecognitionException {
        CSampleParser.struct_declaration_return retval = new CSampleParser.struct_declaration_return();
        retval.start = input.LT(1);

        int struct_declaration_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal43=null;
        CSampleParser.specifier_qualifier_list_return op1 =null;

        CSampleParser.struct_declarator_list_return op2 =null;


        CommonTree char_literal43_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 15) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:351:2: (op1= specifier_qualifier_list op2= struct_declarator_list ';' )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:352:2: op1= specifier_qualifier_list op2= struct_declarator_list ';'
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_specifier_qualifier_list_in_struct_declaration646);
            op1=specifier_qualifier_list();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, op1.getTree());

            pushFollow(FOLLOW_struct_declarator_list_in_struct_declaration650);
            op2=struct_declarator_list();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, op2.getTree());

            char_literal43=(Token)match(input,47,FOLLOW_47_in_struct_declaration652); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            char_literal43_tree = 
            (CommonTree)adaptor.create(char_literal43)
            ;
            adaptor.addChild(root_0, char_literal43_tree);
            }

            if ( state.backtracking==0 ) {
            //		System.out.println("AAAA: " + (op1!=null?input.toString(op1.start,op1.stop):null));
            //		System.out.println("AAAA: " + (op2!=null?input.toString(op2.start,op2.stop):null));
            		StringPair pair = new StringPair((op1!=null?input.toString(op1.start,op1.stop):null), (op2!=null?input.toString(op2.start,op2.stop):null));
            		pairsList.add(pair);
            //		System.out.println("NEW " +pairsList.size());
            		}

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 15, struct_declaration_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "struct_declaration"


    public static class specifier_qualifier_list_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "specifier_qualifier_list"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:362:1: specifier_qualifier_list : ( type_qualifier | type_specifier )+ ;
    public final CSampleParser.specifier_qualifier_list_return specifier_qualifier_list() throws RecognitionException {
        CSampleParser.specifier_qualifier_list_return retval = new CSampleParser.specifier_qualifier_list_return();
        retval.start = input.LT(1);

        int specifier_qualifier_list_StartIndex = input.index();

        CommonTree root_0 = null;

        CSampleParser.type_qualifier_return type_qualifier44 =null;

        CSampleParser.type_specifier_return type_specifier45 =null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 16) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:363:2: ( ( type_qualifier | type_specifier )+ )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:363:4: ( type_qualifier | type_specifier )+
            {
            root_0 = (CommonTree)adaptor.nil();


            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:363:4: ( type_qualifier | type_specifier )+
            int cnt16=0;
            loop16:
            do {
                int alt16=3;
                switch ( input.LA(1) ) {
                case IDENTIFIER:
                    {
                    switch ( input.LA(2) ) {
                    case 59:
                        {
                        int LA16_19 = input.LA(3);

                        if ( (((synpred35_CSample()&&synpred35_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                            alt16=2;
                        }


                        }
                        break;
                    case 30:
                        {
                        int LA16_20 = input.LA(3);

                        if ( (((synpred35_CSample()&&synpred35_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                            alt16=2;
                        }


                        }
                        break;
                    case 46:
                        {
                        int LA16_21 = input.LA(3);

                        if ( (((synpred35_CSample()&&synpred35_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                            alt16=2;
                        }


                        }
                        break;
                    case IDENTIFIER:
                    case 31:
                    case 32:
                    case 66:
                    case 67:
                    case 71:
                    case 73:
                    case 75:
                    case 79:
                    case 80:
                    case 83:
                    case 84:
                    case 87:
                    case 90:
                    case 91:
                    case 92:
                    case 93:
                        {
                        alt16=2;
                        }
                        break;

                    }

                    }
                    break;
                case 67:
                case 93:
                    {
                    alt16=1;
                    }
                    break;
                case 66:
                case 71:
                case 73:
                case 75:
                case 79:
                case 80:
                case 83:
                case 84:
                case 87:
                case 90:
                case 91:
                case 92:
                    {
                    alt16=2;
                    }
                    break;

                }

                switch (alt16) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:363:6: type_qualifier
            	    {
            	    pushFollow(FOLLOW_type_qualifier_in_specifier_qualifier_list670);
            	    type_qualifier44=type_qualifier();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, type_qualifier44.getTree());

            	    }
            	    break;
            	case 2 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:363:23: type_specifier
            	    {
            	    pushFollow(FOLLOW_type_specifier_in_specifier_qualifier_list674);
            	    type_specifier45=type_specifier();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, type_specifier45.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt16 >= 1 ) break loop16;
            	    if (state.backtracking>0) {state.failed=true; return retval;}
                        EarlyExitException eee =
                            new EarlyExitException(16, input);
                        throw eee;
                }
                cnt16++;
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 16, specifier_qualifier_list_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "specifier_qualifier_list"


    public static class struct_declarator_list_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "struct_declarator_list"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:366:1: struct_declarator_list : struct_declarator ( ',' struct_declarator )* ;
    public final CSampleParser.struct_declarator_list_return struct_declarator_list() throws RecognitionException {
        CSampleParser.struct_declarator_list_return retval = new CSampleParser.struct_declarator_list_return();
        retval.start = input.LT(1);

        int struct_declarator_list_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal47=null;
        CSampleParser.struct_declarator_return struct_declarator46 =null;

        CSampleParser.struct_declarator_return struct_declarator48 =null;


        CommonTree char_literal47_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 17) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:367:2: ( struct_declarator ( ',' struct_declarator )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:367:4: struct_declarator ( ',' struct_declarator )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_struct_declarator_in_struct_declarator_list688);
            struct_declarator46=struct_declarator();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, struct_declarator46.getTree());

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:367:22: ( ',' struct_declarator )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==37) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:367:23: ',' struct_declarator
            	    {
            	    char_literal47=(Token)match(input,37,FOLLOW_37_in_struct_declarator_list691); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal47_tree = 
            	    (CommonTree)adaptor.create(char_literal47)
            	    ;
            	    adaptor.addChild(root_0, char_literal47_tree);
            	    }

            	    pushFollow(FOLLOW_struct_declarator_in_struct_declarator_list693);
            	    struct_declarator48=struct_declarator();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, struct_declarator48.getTree());

            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 17, struct_declarator_list_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "struct_declarator_list"


    public static class struct_declarator_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "struct_declarator"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:370:1: struct_declarator : ( declarator ( ':' constant_expression )? | ':' constant_expression );
    public final CSampleParser.struct_declarator_return struct_declarator() throws RecognitionException {
        CSampleParser.struct_declarator_return retval = new CSampleParser.struct_declarator_return();
        retval.start = input.LT(1);

        int struct_declarator_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal50=null;
        Token char_literal52=null;
        CSampleParser.declarator_return declarator49 =null;

        CSampleParser.constant_expression_return constant_expression51 =null;

        CSampleParser.constant_expression_return constant_expression53 =null;


        CommonTree char_literal50_tree=null;
        CommonTree char_literal52_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 18) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:371:2: ( declarator ( ':' constant_expression )? | ':' constant_expression )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==IDENTIFIER||LA19_0==30||LA19_0==32) ) {
                alt19=1;
            }
            else if ( (LA19_0==46) ) {
                alt19=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;

            }
            switch (alt19) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:371:4: declarator ( ':' constant_expression )?
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_declarator_in_struct_declarator706);
                    declarator49=declarator();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, declarator49.getTree());

                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:371:15: ( ':' constant_expression )?
                    int alt18=2;
                    int LA18_0 = input.LA(1);

                    if ( (LA18_0==46) ) {
                        alt18=1;
                    }
                    switch (alt18) {
                        case 1 :
                            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:371:16: ':' constant_expression
                            {
                            char_literal50=(Token)match(input,46,FOLLOW_46_in_struct_declarator709); if (state.failed) return retval;
                            if ( state.backtracking==0 ) {
                            char_literal50_tree = 
                            (CommonTree)adaptor.create(char_literal50)
                            ;
                            adaptor.addChild(root_0, char_literal50_tree);
                            }

                            pushFollow(FOLLOW_constant_expression_in_struct_declarator711);
                            constant_expression51=constant_expression();

                            state._fsp--;
                            if (state.failed) return retval;
                            if ( state.backtracking==0 ) adaptor.addChild(root_0, constant_expression51.getTree());

                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:372:4: ':' constant_expression
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal52=(Token)match(input,46,FOLLOW_46_in_struct_declarator718); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal52_tree = 
                    (CommonTree)adaptor.create(char_literal52)
                    ;
                    adaptor.addChild(root_0, char_literal52_tree);
                    }

                    pushFollow(FOLLOW_constant_expression_in_struct_declarator720);
                    constant_expression53=constant_expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, constant_expression53.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 18, struct_declarator_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "struct_declarator"


    public static class enum_specifier_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "enum_specifier"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:375:1: enum_specifier options {k=3; } : ( 'enum' '{' enumerator_list '}' | 'enum' IDENTIFIER '{' enumerator_list '}' | 'enum' IDENTIFIER );
    public final CSampleParser.enum_specifier_return enum_specifier() throws RecognitionException {
        CSampleParser.enum_specifier_return retval = new CSampleParser.enum_specifier_return();
        retval.start = input.LT(1);

        int enum_specifier_StartIndex = input.index();

        CommonTree root_0 = null;

        Token string_literal54=null;
        Token char_literal55=null;
        Token char_literal57=null;
        Token string_literal58=null;
        Token IDENTIFIER59=null;
        Token char_literal60=null;
        Token char_literal62=null;
        Token string_literal63=null;
        Token IDENTIFIER64=null;
        CSampleParser.enumerator_list_return enumerator_list56 =null;

        CSampleParser.enumerator_list_return enumerator_list61 =null;


        CommonTree string_literal54_tree=null;
        CommonTree char_literal55_tree=null;
        CommonTree char_literal57_tree=null;
        CommonTree string_literal58_tree=null;
        CommonTree IDENTIFIER59_tree=null;
        CommonTree char_literal60_tree=null;
        CommonTree char_literal62_tree=null;
        CommonTree string_literal63_tree=null;
        CommonTree IDENTIFIER64_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 19) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:377:2: ( 'enum' '{' enumerator_list '}' | 'enum' IDENTIFIER '{' enumerator_list '}' | 'enum' IDENTIFIER )
            int alt20=3;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==73) ) {
                int LA20_1 = input.LA(2);

                if ( (LA20_1==95) ) {
                    alt20=1;
                }
                else if ( (LA20_1==IDENTIFIER) ) {
                    int LA20_3 = input.LA(3);

                    if ( (LA20_3==95) ) {
                        alt20=2;
                    }
                    else if ( (LA20_3==EOF||LA20_3==IDENTIFIER||(LA20_3 >= 30 && LA20_3 <= 32)||LA20_3==37||(LA20_3 >= 46 && LA20_3 <= 47)||LA20_3==59||LA20_3==63||(LA20_3 >= 66 && LA20_3 <= 67)||LA20_3==71||(LA20_3 >= 73 && LA20_3 <= 75)||(LA20_3 >= 79 && LA20_3 <= 81)||(LA20_3 >= 83 && LA20_3 <= 84)||(LA20_3 >= 86 && LA20_3 <= 87)||(LA20_3 >= 90 && LA20_3 <= 93)) ) {
                        alt20=3;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return retval;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 20, 3, input);

                        throw nvae;

                    }
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 20, 1, input);

                    throw nvae;

                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;

            }
            switch (alt20) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:377:4: 'enum' '{' enumerator_list '}'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal54=(Token)match(input,73,FOLLOW_73_in_enum_specifier738); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal54_tree = 
                    (CommonTree)adaptor.create(string_literal54)
                    ;
                    adaptor.addChild(root_0, string_literal54_tree);
                    }

                    char_literal55=(Token)match(input,95,FOLLOW_95_in_enum_specifier740); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal55_tree = 
                    (CommonTree)adaptor.create(char_literal55)
                    ;
                    adaptor.addChild(root_0, char_literal55_tree);
                    }

                    pushFollow(FOLLOW_enumerator_list_in_enum_specifier742);
                    enumerator_list56=enumerator_list();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, enumerator_list56.getTree());

                    char_literal57=(Token)match(input,99,FOLLOW_99_in_enum_specifier744); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal57_tree = 
                    (CommonTree)adaptor.create(char_literal57)
                    ;
                    adaptor.addChild(root_0, char_literal57_tree);
                    }

                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:378:4: 'enum' IDENTIFIER '{' enumerator_list '}'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal58=(Token)match(input,73,FOLLOW_73_in_enum_specifier749); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal58_tree = 
                    (CommonTree)adaptor.create(string_literal58)
                    ;
                    adaptor.addChild(root_0, string_literal58_tree);
                    }

                    IDENTIFIER59=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_enum_specifier751); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    IDENTIFIER59_tree = 
                    (CommonTree)adaptor.create(IDENTIFIER59)
                    ;
                    adaptor.addChild(root_0, IDENTIFIER59_tree);
                    }

                    char_literal60=(Token)match(input,95,FOLLOW_95_in_enum_specifier753); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal60_tree = 
                    (CommonTree)adaptor.create(char_literal60)
                    ;
                    adaptor.addChild(root_0, char_literal60_tree);
                    }

                    pushFollow(FOLLOW_enumerator_list_in_enum_specifier755);
                    enumerator_list61=enumerator_list();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, enumerator_list61.getTree());

                    char_literal62=(Token)match(input,99,FOLLOW_99_in_enum_specifier757); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal62_tree = 
                    (CommonTree)adaptor.create(char_literal62)
                    ;
                    adaptor.addChild(root_0, char_literal62_tree);
                    }

                    }
                    break;
                case 3 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:379:4: 'enum' IDENTIFIER
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal63=(Token)match(input,73,FOLLOW_73_in_enum_specifier762); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal63_tree = 
                    (CommonTree)adaptor.create(string_literal63)
                    ;
                    adaptor.addChild(root_0, string_literal63_tree);
                    }

                    IDENTIFIER64=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_enum_specifier764); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    IDENTIFIER64_tree = 
                    (CommonTree)adaptor.create(IDENTIFIER64)
                    ;
                    adaptor.addChild(root_0, IDENTIFIER64_tree);
                    }

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 19, enum_specifier_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "enum_specifier"


    public static class enumerator_list_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "enumerator_list"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:382:1: enumerator_list : enumerator ( ',' enumerator )* ;
    public final CSampleParser.enumerator_list_return enumerator_list() throws RecognitionException {
        CSampleParser.enumerator_list_return retval = new CSampleParser.enumerator_list_return();
        retval.start = input.LT(1);

        int enumerator_list_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal66=null;
        CSampleParser.enumerator_return enumerator65 =null;

        CSampleParser.enumerator_return enumerator67 =null;


        CommonTree char_literal66_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 20) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:383:2: ( enumerator ( ',' enumerator )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:383:4: enumerator ( ',' enumerator )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_enumerator_in_enumerator_list775);
            enumerator65=enumerator();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, enumerator65.getTree());

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:383:15: ( ',' enumerator )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==37) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:383:16: ',' enumerator
            	    {
            	    char_literal66=(Token)match(input,37,FOLLOW_37_in_enumerator_list778); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal66_tree = 
            	    (CommonTree)adaptor.create(char_literal66)
            	    ;
            	    adaptor.addChild(root_0, char_literal66_tree);
            	    }

            	    pushFollow(FOLLOW_enumerator_in_enumerator_list780);
            	    enumerator67=enumerator();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, enumerator67.getTree());

            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 20, enumerator_list_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "enumerator_list"


    public static class enumerator_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "enumerator"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:386:1: enumerator : IDENTIFIER ( '=' constant_expression )? ;
    public final CSampleParser.enumerator_return enumerator() throws RecognitionException {
        CSampleParser.enumerator_return retval = new CSampleParser.enumerator_return();
        retval.start = input.LT(1);

        int enumerator_StartIndex = input.index();

        CommonTree root_0 = null;

        Token IDENTIFIER68=null;
        Token char_literal69=null;
        CSampleParser.constant_expression_return constant_expression70 =null;


        CommonTree IDENTIFIER68_tree=null;
        CommonTree char_literal69_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 21) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:387:2: ( IDENTIFIER ( '=' constant_expression )? )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:387:4: IDENTIFIER ( '=' constant_expression )?
            {
            root_0 = (CommonTree)adaptor.nil();


            IDENTIFIER68=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_enumerator793); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            IDENTIFIER68_tree = 
            (CommonTree)adaptor.create(IDENTIFIER68)
            ;
            adaptor.addChild(root_0, IDENTIFIER68_tree);
            }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:387:15: ( '=' constant_expression )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==52) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:387:16: '=' constant_expression
                    {
                    char_literal69=(Token)match(input,52,FOLLOW_52_in_enumerator796); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal69_tree = 
                    (CommonTree)adaptor.create(char_literal69)
                    ;
                    adaptor.addChild(root_0, char_literal69_tree);
                    }

                    pushFollow(FOLLOW_constant_expression_in_enumerator798);
                    constant_expression70=constant_expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, constant_expression70.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 21, enumerator_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "enumerator"


    public static class type_qualifier_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "type_qualifier"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:390:1: type_qualifier : ( 'const' | 'volatile' );
    public final CSampleParser.type_qualifier_return type_qualifier() throws RecognitionException {
        CSampleParser.type_qualifier_return retval = new CSampleParser.type_qualifier_return();
        retval.start = input.LT(1);

        int type_qualifier_StartIndex = input.index();

        CommonTree root_0 = null;

        Token set71=null;

        CommonTree set71_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 22) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:391:2: ( 'const' | 'volatile' )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:
            {
            root_0 = (CommonTree)adaptor.nil();


            set71=(Token)input.LT(1);

            if ( input.LA(1)==67||input.LA(1)==93 ) {
                input.consume();
                if ( state.backtracking==0 ) adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(set71)
                );
                state.errorRecovery=false;
                state.failed=false;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 22, type_qualifier_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "type_qualifier"


    public static class declarator_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "declarator"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:395:1: declarator : ( ( pointer )? direct_declarator | pointer );
    public final CSampleParser.declarator_return declarator() throws RecognitionException {
        CSampleParser.declarator_return retval = new CSampleParser.declarator_return();
        retval.start = input.LT(1);

        int declarator_StartIndex = input.index();

        CommonTree root_0 = null;

        CSampleParser.pointer_return pointer72 =null;

        CSampleParser.direct_declarator_return direct_declarator73 =null;

        CSampleParser.pointer_return pointer74 =null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 23) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:396:2: ( ( pointer )? direct_declarator | pointer )
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==32) ) {
                int LA24_1 = input.LA(2);

                if ( (synpred45_CSample()) ) {
                    alt24=1;
                }
                else if ( (true) ) {
                    alt24=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 24, 1, input);

                    throw nvae;

                }
            }
            else if ( (LA24_0==IDENTIFIER||LA24_0==30) ) {
                alt24=1;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;

            }
            switch (alt24) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:396:4: ( pointer )? direct_declarator
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:396:4: ( pointer )?
                    int alt23=2;
                    int LA23_0 = input.LA(1);

                    if ( (LA23_0==32) ) {
                        alt23=1;
                    }
                    switch (alt23) {
                        case 1 :
                            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:396:4: pointer
                            {
                            pushFollow(FOLLOW_pointer_in_declarator827);
                            pointer72=pointer();

                            state._fsp--;
                            if (state.failed) return retval;
                            if ( state.backtracking==0 ) adaptor.addChild(root_0, pointer72.getTree());

                            }
                            break;

                    }


                    pushFollow(FOLLOW_direct_declarator_in_declarator830);
                    direct_declarator73=direct_declarator();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, direct_declarator73.getTree());

                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:397:4: pointer
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_pointer_in_declarator835);
                    pointer74=pointer();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, pointer74.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 23, declarator_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "declarator"


    public static class direct_declarator_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "direct_declarator"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:400:1: direct_declarator : ( IDENTIFIER | '(' declarator ')' ) ( declarator_suffix )* ;
    public final CSampleParser.direct_declarator_return direct_declarator() throws RecognitionException {
        CSampleParser.direct_declarator_return retval = new CSampleParser.direct_declarator_return();
        retval.start = input.LT(1);

        int direct_declarator_StartIndex = input.index();

        CommonTree root_0 = null;

        Token IDENTIFIER75=null;
        Token char_literal76=null;
        Token char_literal78=null;
        CSampleParser.declarator_return declarator77 =null;

        CSampleParser.declarator_suffix_return declarator_suffix79 =null;


        CommonTree IDENTIFIER75_tree=null;
        CommonTree char_literal76_tree=null;
        CommonTree char_literal78_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 24) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:401:2: ( ( IDENTIFIER | '(' declarator ')' ) ( declarator_suffix )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:401:6: ( IDENTIFIER | '(' declarator ')' ) ( declarator_suffix )*
            {
            root_0 = (CommonTree)adaptor.nil();


            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:401:6: ( IDENTIFIER | '(' declarator ')' )
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==IDENTIFIER) ) {
                alt25=1;
            }
            else if ( (LA25_0==30) ) {
                alt25=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 25, 0, input);

                throw nvae;

            }
            switch (alt25) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:401:8: IDENTIFIER
                    {
                    IDENTIFIER75=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_direct_declarator850); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    IDENTIFIER75_tree = 
                    (CommonTree)adaptor.create(IDENTIFIER75)
                    ;
                    adaptor.addChild(root_0, IDENTIFIER75_tree);
                    }

                    if ( state.backtracking==0 ) {
                    			if (declaration_stack.size()>0&&((declaration_scope)declaration_stack.peek()).isTypedef) {
                    				((Symbols_scope)Symbols_stack.peek()).types.add((IDENTIFIER75!=null?IDENTIFIER75.getText():null));
                    				System.out.println("define type "+(IDENTIFIER75!=null?IDENTIFIER75.getText():null));
                    			}
                    			}

                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:408:5: '(' declarator ')'
                    {
                    char_literal76=(Token)match(input,30,FOLLOW_30_in_direct_declarator861); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal76_tree = 
                    (CommonTree)adaptor.create(char_literal76)
                    ;
                    adaptor.addChild(root_0, char_literal76_tree);
                    }

                    pushFollow(FOLLOW_declarator_in_direct_declarator863);
                    declarator77=declarator();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, declarator77.getTree());

                    char_literal78=(Token)match(input,31,FOLLOW_31_in_direct_declarator865); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal78_tree = 
                    (CommonTree)adaptor.create(char_literal78)
                    ;
                    adaptor.addChild(root_0, char_literal78_tree);
                    }

                    }
                    break;

            }


            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:410:9: ( declarator_suffix )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==30) ) {
                    switch ( input.LA(2) ) {
                    case 31:
                        {
                        int LA26_26 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;
                    case IDENTIFIER:
                        {
                        int LA26_28 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;
                    case 63:
                    case 74:
                    case 81:
                    case 86:
                        {
                        int LA26_31 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;
                    case 92:
                        {
                        int LA26_32 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;
                    case 66:
                        {
                        int LA26_33 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;
                    case 83:
                        {
                        int LA26_34 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;
                    case 79:
                        {
                        int LA26_35 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;
                    case 80:
                        {
                        int LA26_36 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;
                    case 75:
                        {
                        int LA26_37 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;
                    case 71:
                        {
                        int LA26_38 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;
                    case 84:
                        {
                        int LA26_39 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;
                    case 91:
                        {
                        int LA26_40 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;
                    case 87:
                    case 90:
                        {
                        int LA26_41 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;
                    case 73:
                        {
                        int LA26_42 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;
                    case 67:
                    case 93:
                        {
                        int LA26_43 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;

                    }

                }
                else if ( (LA26_0==59) ) {
                    switch ( input.LA(2) ) {
                    case 60:
                        {
                        int LA26_44 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;
                    case 30:
                        {
                        int LA26_45 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;
                    case IDENTIFIER:
                        {
                        int LA26_46 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;
                    case CHARACTER_LITERAL:
                    case DECIMAL_LITERAL:
                    case FLOATING_POINT_LITERAL:
                    case HEX_LITERAL:
                    case OCTAL_LITERAL:
                    case STRING_LITERAL:
                        {
                        int LA26_47 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;
                    case 35:
                        {
                        int LA26_48 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;
                    case 39:
                        {
                        int LA26_49 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;
                    case 23:
                    case 28:
                    case 32:
                    case 34:
                    case 38:
                    case 100:
                        {
                        int LA26_50 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;
                    case 85:
                        {
                        int LA26_51 = input.LA(3);

                        if ( (synpred47_CSample()) ) {
                            alt26=1;
                        }


                        }
                        break;

                    }

                }


                switch (alt26) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:410:9: declarator_suffix
            	    {
            	    pushFollow(FOLLOW_declarator_suffix_in_direct_declarator879);
            	    declarator_suffix79=declarator_suffix();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, declarator_suffix79.getTree());

            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 24, direct_declarator_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "direct_declarator"


    public static class declarator_suffix_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "declarator_suffix"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:413:1: declarator_suffix : ( '[' constant_expression ']' | '[' ']' | '(' parameter_type_list ')' | '(' identifier_list ')' | '(' ')' );
    public final CSampleParser.declarator_suffix_return declarator_suffix() throws RecognitionException {
        CSampleParser.declarator_suffix_return retval = new CSampleParser.declarator_suffix_return();
        retval.start = input.LT(1);

        int declarator_suffix_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal80=null;
        Token char_literal82=null;
        Token char_literal83=null;
        Token char_literal84=null;
        Token char_literal85=null;
        Token char_literal87=null;
        Token char_literal88=null;
        Token char_literal90=null;
        Token char_literal91=null;
        Token char_literal92=null;
        CSampleParser.constant_expression_return constant_expression81 =null;

        CSampleParser.parameter_type_list_return parameter_type_list86 =null;

        CSampleParser.identifier_list_return identifier_list89 =null;


        CommonTree char_literal80_tree=null;
        CommonTree char_literal82_tree=null;
        CommonTree char_literal83_tree=null;
        CommonTree char_literal84_tree=null;
        CommonTree char_literal85_tree=null;
        CommonTree char_literal87_tree=null;
        CommonTree char_literal88_tree=null;
        CommonTree char_literal90_tree=null;
        CommonTree char_literal91_tree=null;
        CommonTree char_literal92_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 25) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:414:2: ( '[' constant_expression ']' | '[' ']' | '(' parameter_type_list ')' | '(' identifier_list ')' | '(' ')' )
            int alt27=5;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==59) ) {
                int LA27_1 = input.LA(2);

                if ( (LA27_1==60) ) {
                    alt27=2;
                }
                else if ( (LA27_1==CHARACTER_LITERAL||LA27_1==DECIMAL_LITERAL||LA27_1==FLOATING_POINT_LITERAL||LA27_1==HEX_LITERAL||LA27_1==IDENTIFIER||LA27_1==OCTAL_LITERAL||LA27_1==STRING_LITERAL||LA27_1==23||LA27_1==28||LA27_1==30||LA27_1==32||(LA27_1 >= 34 && LA27_1 <= 35)||(LA27_1 >= 38 && LA27_1 <= 39)||LA27_1==85||LA27_1==100) ) {
                    alt27=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 27, 1, input);

                    throw nvae;

                }
            }
            else if ( (LA27_0==30) ) {
                switch ( input.LA(2) ) {
                case 31:
                    {
                    alt27=5;
                    }
                    break;
                case 63:
                case 66:
                case 67:
                case 71:
                case 73:
                case 74:
                case 75:
                case 79:
                case 80:
                case 81:
                case 83:
                case 84:
                case 86:
                case 87:
                case 90:
                case 91:
                case 92:
                case 93:
                    {
                    alt27=3;
                    }
                    break;
                case IDENTIFIER:
                    {
                    int LA27_24 = input.LA(3);

                    if ( (synpred50_CSample()) ) {
                        alt27=3;
                    }
                    else if ( (synpred51_CSample()) ) {
                        alt27=4;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return retval;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 27, 24, input);

                        throw nvae;

                    }
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 27, 2, input);

                    throw nvae;

                }

            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;

            }
            switch (alt27) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:414:6: '[' constant_expression ']'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal80=(Token)match(input,59,FOLLOW_59_in_declarator_suffix893); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal80_tree = 
                    (CommonTree)adaptor.create(char_literal80)
                    ;
                    adaptor.addChild(root_0, char_literal80_tree);
                    }

                    pushFollow(FOLLOW_constant_expression_in_declarator_suffix895);
                    constant_expression81=constant_expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, constant_expression81.getTree());

                    char_literal82=(Token)match(input,60,FOLLOW_60_in_declarator_suffix897); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal82_tree = 
                    (CommonTree)adaptor.create(char_literal82)
                    ;
                    adaptor.addChild(root_0, char_literal82_tree);
                    }

                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:415:9: '[' ']'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal83=(Token)match(input,59,FOLLOW_59_in_declarator_suffix907); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal83_tree = 
                    (CommonTree)adaptor.create(char_literal83)
                    ;
                    adaptor.addChild(root_0, char_literal83_tree);
                    }

                    char_literal84=(Token)match(input,60,FOLLOW_60_in_declarator_suffix909); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal84_tree = 
                    (CommonTree)adaptor.create(char_literal84)
                    ;
                    adaptor.addChild(root_0, char_literal84_tree);
                    }

                    }
                    break;
                case 3 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:416:9: '(' parameter_type_list ')'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal85=(Token)match(input,30,FOLLOW_30_in_declarator_suffix919); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal85_tree = 
                    (CommonTree)adaptor.create(char_literal85)
                    ;
                    adaptor.addChild(root_0, char_literal85_tree);
                    }

                    pushFollow(FOLLOW_parameter_type_list_in_declarator_suffix921);
                    parameter_type_list86=parameter_type_list();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, parameter_type_list86.getTree());

                    char_literal87=(Token)match(input,31,FOLLOW_31_in_declarator_suffix923); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal87_tree = 
                    (CommonTree)adaptor.create(char_literal87)
                    ;
                    adaptor.addChild(root_0, char_literal87_tree);
                    }

                    }
                    break;
                case 4 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:417:9: '(' identifier_list ')'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal88=(Token)match(input,30,FOLLOW_30_in_declarator_suffix933); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal88_tree = 
                    (CommonTree)adaptor.create(char_literal88)
                    ;
                    adaptor.addChild(root_0, char_literal88_tree);
                    }

                    pushFollow(FOLLOW_identifier_list_in_declarator_suffix935);
                    identifier_list89=identifier_list();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, identifier_list89.getTree());

                    char_literal90=(Token)match(input,31,FOLLOW_31_in_declarator_suffix937); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal90_tree = 
                    (CommonTree)adaptor.create(char_literal90)
                    ;
                    adaptor.addChild(root_0, char_literal90_tree);
                    }

                    }
                    break;
                case 5 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:418:9: '(' ')'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal91=(Token)match(input,30,FOLLOW_30_in_declarator_suffix947); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal91_tree = 
                    (CommonTree)adaptor.create(char_literal91)
                    ;
                    adaptor.addChild(root_0, char_literal91_tree);
                    }

                    char_literal92=(Token)match(input,31,FOLLOW_31_in_declarator_suffix949); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal92_tree = 
                    (CommonTree)adaptor.create(char_literal92)
                    ;
                    adaptor.addChild(root_0, char_literal92_tree);
                    }

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 25, declarator_suffix_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "declarator_suffix"


    public static class pointer_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "pointer"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:421:1: pointer : ( '*' ( type_qualifier )+ ( pointer )? | '*' pointer | '*' );
    public final CSampleParser.pointer_return pointer() throws RecognitionException {
        CSampleParser.pointer_return retval = new CSampleParser.pointer_return();
        retval.start = input.LT(1);

        int pointer_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal93=null;
        Token char_literal96=null;
        Token char_literal98=null;
        CSampleParser.type_qualifier_return type_qualifier94 =null;

        CSampleParser.pointer_return pointer95 =null;

        CSampleParser.pointer_return pointer97 =null;


        CommonTree char_literal93_tree=null;
        CommonTree char_literal96_tree=null;
        CommonTree char_literal98_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 26) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:422:2: ( '*' ( type_qualifier )+ ( pointer )? | '*' pointer | '*' )
            int alt30=3;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==32) ) {
                switch ( input.LA(2) ) {
                case 67:
                case 93:
                    {
                    int LA30_2 = input.LA(3);

                    if ( (synpred54_CSample()) ) {
                        alt30=1;
                    }
                    else if ( (true) ) {
                        alt30=3;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return retval;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 30, 2, input);

                        throw nvae;

                    }
                    }
                    break;
                case 32:
                    {
                    int LA30_3 = input.LA(3);

                    if ( (synpred55_CSample()) ) {
                        alt30=2;
                    }
                    else if ( (true) ) {
                        alt30=3;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return retval;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 30, 3, input);

                        throw nvae;

                    }
                    }
                    break;
                case EOF:
                case IDENTIFIER:
                case 30:
                case 31:
                case 37:
                case 46:
                case 47:
                case 52:
                case 59:
                case 63:
                case 66:
                case 71:
                case 73:
                case 74:
                case 75:
                case 79:
                case 80:
                case 81:
                case 83:
                case 84:
                case 86:
                case 87:
                case 89:
                case 90:
                case 91:
                case 92:
                case 95:
                    {
                    alt30=3;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 30, 1, input);

                    throw nvae;

                }

            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;

            }
            switch (alt30) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:422:4: '*' ( type_qualifier )+ ( pointer )?
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal93=(Token)match(input,32,FOLLOW_32_in_pointer960); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal93_tree = 
                    (CommonTree)adaptor.create(char_literal93)
                    ;
                    adaptor.addChild(root_0, char_literal93_tree);
                    }

                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:422:8: ( type_qualifier )+
                    int cnt28=0;
                    loop28:
                    do {
                        int alt28=2;
                        int LA28_0 = input.LA(1);

                        if ( (LA28_0==67||LA28_0==93) ) {
                            int LA28_17 = input.LA(2);

                            if ( (synpred52_CSample()) ) {
                                alt28=1;
                            }


                        }


                        switch (alt28) {
                    	case 1 :
                    	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:422:8: type_qualifier
                    	    {
                    	    pushFollow(FOLLOW_type_qualifier_in_pointer962);
                    	    type_qualifier94=type_qualifier();

                    	    state._fsp--;
                    	    if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) adaptor.addChild(root_0, type_qualifier94.getTree());

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt28 >= 1 ) break loop28;
                    	    if (state.backtracking>0) {state.failed=true; return retval;}
                                EarlyExitException eee =
                                    new EarlyExitException(28, input);
                                throw eee;
                        }
                        cnt28++;
                    } while (true);


                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:422:24: ( pointer )?
                    int alt29=2;
                    int LA29_0 = input.LA(1);

                    if ( (LA29_0==32) ) {
                        int LA29_1 = input.LA(2);

                        if ( (synpred53_CSample()) ) {
                            alt29=1;
                        }
                    }
                    switch (alt29) {
                        case 1 :
                            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:422:24: pointer
                            {
                            pushFollow(FOLLOW_pointer_in_pointer965);
                            pointer95=pointer();

                            state._fsp--;
                            if (state.failed) return retval;
                            if ( state.backtracking==0 ) adaptor.addChild(root_0, pointer95.getTree());

                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:423:4: '*' pointer
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal96=(Token)match(input,32,FOLLOW_32_in_pointer971); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal96_tree = 
                    (CommonTree)adaptor.create(char_literal96)
                    ;
                    adaptor.addChild(root_0, char_literal96_tree);
                    }

                    pushFollow(FOLLOW_pointer_in_pointer973);
                    pointer97=pointer();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, pointer97.getTree());

                    }
                    break;
                case 3 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:424:4: '*'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal98=(Token)match(input,32,FOLLOW_32_in_pointer978); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal98_tree = 
                    (CommonTree)adaptor.create(char_literal98)
                    ;
                    adaptor.addChild(root_0, char_literal98_tree);
                    }

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 26, pointer_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "pointer"


    public static class parameter_type_list_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "parameter_type_list"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:427:1: parameter_type_list : parameter_list ( ',' '...' )? ;
    public final CSampleParser.parameter_type_list_return parameter_type_list() throws RecognitionException {
        CSampleParser.parameter_type_list_return retval = new CSampleParser.parameter_type_list_return();
        retval.start = input.LT(1);

        int parameter_type_list_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal100=null;
        Token string_literal101=null;
        CSampleParser.parameter_list_return parameter_list99 =null;


        CommonTree char_literal100_tree=null;
        CommonTree string_literal101_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 27) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:428:2: ( parameter_list ( ',' '...' )? )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:428:4: parameter_list ( ',' '...' )?
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_parameter_list_in_parameter_type_list989);
            parameter_list99=parameter_list();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, parameter_list99.getTree());

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:428:19: ( ',' '...' )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==37) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:428:20: ',' '...'
                    {
                    char_literal100=(Token)match(input,37,FOLLOW_37_in_parameter_type_list992); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal100_tree = 
                    (CommonTree)adaptor.create(char_literal100)
                    ;
                    adaptor.addChild(root_0, char_literal100_tree);
                    }

                    string_literal101=(Token)match(input,43,FOLLOW_43_in_parameter_type_list994); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal101_tree = 
                    (CommonTree)adaptor.create(string_literal101)
                    ;
                    adaptor.addChild(root_0, string_literal101_tree);
                    }

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 27, parameter_type_list_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "parameter_type_list"


    public static class parameter_list_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "parameter_list"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:431:1: parameter_list : parameter_declaration ( ',' parameter_declaration )* ;
    public final CSampleParser.parameter_list_return parameter_list() throws RecognitionException {
        CSampleParser.parameter_list_return retval = new CSampleParser.parameter_list_return();
        retval.start = input.LT(1);

        int parameter_list_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal103=null;
        CSampleParser.parameter_declaration_return parameter_declaration102 =null;

        CSampleParser.parameter_declaration_return parameter_declaration104 =null;


        CommonTree char_literal103_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 28) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:432:2: ( parameter_declaration ( ',' parameter_declaration )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:432:4: parameter_declaration ( ',' parameter_declaration )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_parameter_declaration_in_parameter_list1007);
            parameter_declaration102=parameter_declaration();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, parameter_declaration102.getTree());

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:432:26: ( ',' parameter_declaration )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( (LA32_0==37) ) {
                    int LA32_1 = input.LA(2);

                    if ( (LA32_1==IDENTIFIER||LA32_1==63||(LA32_1 >= 66 && LA32_1 <= 67)||LA32_1==71||(LA32_1 >= 73 && LA32_1 <= 75)||(LA32_1 >= 79 && LA32_1 <= 81)||(LA32_1 >= 83 && LA32_1 <= 84)||(LA32_1 >= 86 && LA32_1 <= 87)||(LA32_1 >= 90 && LA32_1 <= 93)) ) {
                        alt32=1;
                    }


                }


                switch (alt32) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:432:27: ',' parameter_declaration
            	    {
            	    char_literal103=(Token)match(input,37,FOLLOW_37_in_parameter_list1010); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal103_tree = 
            	    (CommonTree)adaptor.create(char_literal103)
            	    ;
            	    adaptor.addChild(root_0, char_literal103_tree);
            	    }

            	    pushFollow(FOLLOW_parameter_declaration_in_parameter_list1012);
            	    parameter_declaration104=parameter_declaration();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, parameter_declaration104.getTree());

            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 28, parameter_list_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "parameter_list"


    public static class parameter_declaration_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "parameter_declaration"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:435:1: parameter_declaration : declaration_specifiers ( declarator | abstract_declarator )* ;
    public final CSampleParser.parameter_declaration_return parameter_declaration() throws RecognitionException {
        CSampleParser.parameter_declaration_return retval = new CSampleParser.parameter_declaration_return();
        retval.start = input.LT(1);

        int parameter_declaration_StartIndex = input.index();

        CommonTree root_0 = null;

        CSampleParser.declaration_specifiers_return declaration_specifiers105 =null;

        CSampleParser.declarator_return declarator106 =null;

        CSampleParser.abstract_declarator_return abstract_declarator107 =null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 29) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:436:2: ( declaration_specifiers ( declarator | abstract_declarator )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:436:4: declaration_specifiers ( declarator | abstract_declarator )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_declaration_specifiers_in_parameter_declaration1025);
            declaration_specifiers105=declaration_specifiers();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, declaration_specifiers105.getTree());

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:436:27: ( declarator | abstract_declarator )*
            loop33:
            do {
                int alt33=3;
                switch ( input.LA(1) ) {
                case 32:
                    {
                    int LA33_4 = input.LA(2);

                    if ( (synpred58_CSample()) ) {
                        alt33=1;
                    }
                    else if ( (synpred59_CSample()) ) {
                        alt33=2;
                    }


                    }
                    break;
                case IDENTIFIER:
                    {
                    alt33=1;
                    }
                    break;
                case 30:
                    {
                    switch ( input.LA(2) ) {
                    case 31:
                    case 59:
                    case 63:
                    case 66:
                    case 67:
                    case 71:
                    case 73:
                    case 74:
                    case 75:
                    case 79:
                    case 80:
                    case 81:
                    case 83:
                    case 84:
                    case 86:
                    case 87:
                    case 90:
                    case 91:
                    case 92:
                    case 93:
                        {
                        alt33=2;
                        }
                        break;
                    case 32:
                        {
                        int LA33_17 = input.LA(3);

                        if ( (synpred58_CSample()) ) {
                            alt33=1;
                        }
                        else if ( (synpred59_CSample()) ) {
                            alt33=2;
                        }


                        }
                        break;
                    case IDENTIFIER:
                        {
                        int LA33_18 = input.LA(3);

                        if ( (synpred58_CSample()) ) {
                            alt33=1;
                        }
                        else if ( (synpred59_CSample()) ) {
                            alt33=2;
                        }


                        }
                        break;
                    case 30:
                        {
                        int LA33_19 = input.LA(3);

                        if ( (synpred58_CSample()) ) {
                            alt33=1;
                        }
                        else if ( (synpred59_CSample()) ) {
                            alt33=2;
                        }


                        }
                        break;

                    }

                    }
                    break;
                case 59:
                    {
                    alt33=2;
                    }
                    break;

                }

                switch (alt33) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:436:28: declarator
            	    {
            	    pushFollow(FOLLOW_declarator_in_parameter_declaration1028);
            	    declarator106=declarator();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, declarator106.getTree());

            	    }
            	    break;
            	case 2 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:436:39: abstract_declarator
            	    {
            	    pushFollow(FOLLOW_abstract_declarator_in_parameter_declaration1030);
            	    abstract_declarator107=abstract_declarator();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, abstract_declarator107.getTree());

            	    }
            	    break;

            	default :
            	    break loop33;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 29, parameter_declaration_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "parameter_declaration"


    public static class identifier_list_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "identifier_list"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:439:1: identifier_list : IDENTIFIER ( ',' IDENTIFIER )* ;
    public final CSampleParser.identifier_list_return identifier_list() throws RecognitionException {
        CSampleParser.identifier_list_return retval = new CSampleParser.identifier_list_return();
        retval.start = input.LT(1);

        int identifier_list_StartIndex = input.index();

        CommonTree root_0 = null;

        Token IDENTIFIER108=null;
        Token char_literal109=null;
        Token IDENTIFIER110=null;

        CommonTree IDENTIFIER108_tree=null;
        CommonTree char_literal109_tree=null;
        CommonTree IDENTIFIER110_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 30) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:440:2: ( IDENTIFIER ( ',' IDENTIFIER )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:440:4: IDENTIFIER ( ',' IDENTIFIER )*
            {
            root_0 = (CommonTree)adaptor.nil();


            IDENTIFIER108=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_identifier_list1043); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            IDENTIFIER108_tree = 
            (CommonTree)adaptor.create(IDENTIFIER108)
            ;
            adaptor.addChild(root_0, IDENTIFIER108_tree);
            }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:440:15: ( ',' IDENTIFIER )*
            loop34:
            do {
                int alt34=2;
                int LA34_0 = input.LA(1);

                if ( (LA34_0==37) ) {
                    alt34=1;
                }


                switch (alt34) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:440:16: ',' IDENTIFIER
            	    {
            	    char_literal109=(Token)match(input,37,FOLLOW_37_in_identifier_list1046); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal109_tree = 
            	    (CommonTree)adaptor.create(char_literal109)
            	    ;
            	    adaptor.addChild(root_0, char_literal109_tree);
            	    }

            	    IDENTIFIER110=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_identifier_list1048); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    IDENTIFIER110_tree = 
            	    (CommonTree)adaptor.create(IDENTIFIER110)
            	    ;
            	    adaptor.addChild(root_0, IDENTIFIER110_tree);
            	    }

            	    }
            	    break;

            	default :
            	    break loop34;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 30, identifier_list_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "identifier_list"


    public static class type_name_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "type_name"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:443:1: type_name : specifier_qualifier_list ( abstract_declarator )? ;
    public final CSampleParser.type_name_return type_name() throws RecognitionException {
        CSampleParser.type_name_return retval = new CSampleParser.type_name_return();
        retval.start = input.LT(1);

        int type_name_StartIndex = input.index();

        CommonTree root_0 = null;

        CSampleParser.specifier_qualifier_list_return specifier_qualifier_list111 =null;

        CSampleParser.abstract_declarator_return abstract_declarator112 =null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 31) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:444:2: ( specifier_qualifier_list ( abstract_declarator )? )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:444:4: specifier_qualifier_list ( abstract_declarator )?
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_specifier_qualifier_list_in_type_name1061);
            specifier_qualifier_list111=specifier_qualifier_list();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, specifier_qualifier_list111.getTree());

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:444:29: ( abstract_declarator )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==30||LA35_0==32||LA35_0==59) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:444:29: abstract_declarator
                    {
                    pushFollow(FOLLOW_abstract_declarator_in_type_name1063);
                    abstract_declarator112=abstract_declarator();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, abstract_declarator112.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 31, type_name_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "type_name"


    public static class abstract_declarator_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "abstract_declarator"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:447:1: abstract_declarator : ( pointer ( direct_abstract_declarator )? | direct_abstract_declarator );
    public final CSampleParser.abstract_declarator_return abstract_declarator() throws RecognitionException {
        CSampleParser.abstract_declarator_return retval = new CSampleParser.abstract_declarator_return();
        retval.start = input.LT(1);

        int abstract_declarator_StartIndex = input.index();

        CommonTree root_0 = null;

        CSampleParser.pointer_return pointer113 =null;

        CSampleParser.direct_abstract_declarator_return direct_abstract_declarator114 =null;

        CSampleParser.direct_abstract_declarator_return direct_abstract_declarator115 =null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 32) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:448:2: ( pointer ( direct_abstract_declarator )? | direct_abstract_declarator )
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==32) ) {
                alt37=1;
            }
            else if ( (LA37_0==30||LA37_0==59) ) {
                alt37=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 37, 0, input);

                throw nvae;

            }
            switch (alt37) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:448:4: pointer ( direct_abstract_declarator )?
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_pointer_in_abstract_declarator1075);
                    pointer113=pointer();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, pointer113.getTree());

                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:448:12: ( direct_abstract_declarator )?
                    int alt36=2;
                    int LA36_0 = input.LA(1);

                    if ( (LA36_0==30) ) {
                        switch ( input.LA(2) ) {
                            case 31:
                                {
                                int LA36_8 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case 32:
                                {
                                int LA36_9 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case 30:
                                {
                                int LA36_10 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case 59:
                                {
                                int LA36_11 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case 63:
                            case 74:
                            case 81:
                            case 86:
                                {
                                int LA36_12 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case 92:
                                {
                                int LA36_13 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case 66:
                                {
                                int LA36_14 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case 83:
                                {
                                int LA36_15 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case 79:
                                {
                                int LA36_16 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case 80:
                                {
                                int LA36_17 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case 75:
                                {
                                int LA36_18 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case 71:
                                {
                                int LA36_19 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case 84:
                                {
                                int LA36_20 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case 91:
                                {
                                int LA36_21 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case 87:
                            case 90:
                                {
                                int LA36_22 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case 73:
                                {
                                int LA36_23 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case IDENTIFIER:
                                {
                                int LA36_24 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case 67:
                            case 93:
                                {
                                int LA36_25 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                        }

                    }
                    else if ( (LA36_0==59) ) {
                        switch ( input.LA(2) ) {
                            case 60:
                                {
                                int LA36_26 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case 30:
                                {
                                int LA36_27 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case IDENTIFIER:
                                {
                                int LA36_28 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case CHARACTER_LITERAL:
                            case DECIMAL_LITERAL:
                            case FLOATING_POINT_LITERAL:
                            case HEX_LITERAL:
                            case OCTAL_LITERAL:
                            case STRING_LITERAL:
                                {
                                int LA36_29 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case 35:
                                {
                                int LA36_30 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case 39:
                                {
                                int LA36_31 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case 23:
                            case 28:
                            case 32:
                            case 34:
                            case 38:
                            case 100:
                                {
                                int LA36_32 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                            case 85:
                                {
                                int LA36_33 = input.LA(3);

                                if ( (synpred62_CSample()) ) {
                                    alt36=1;
                                }
                                }
                                break;
                        }

                    }
                    switch (alt36) {
                        case 1 :
                            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:448:12: direct_abstract_declarator
                            {
                            pushFollow(FOLLOW_direct_abstract_declarator_in_abstract_declarator1077);
                            direct_abstract_declarator114=direct_abstract_declarator();

                            state._fsp--;
                            if (state.failed) return retval;
                            if ( state.backtracking==0 ) adaptor.addChild(root_0, direct_abstract_declarator114.getTree());

                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:449:4: direct_abstract_declarator
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_direct_abstract_declarator_in_abstract_declarator1083);
                    direct_abstract_declarator115=direct_abstract_declarator();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, direct_abstract_declarator115.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 32, abstract_declarator_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "abstract_declarator"


    public static class direct_abstract_declarator_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "direct_abstract_declarator"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:452:1: direct_abstract_declarator : ( '(' abstract_declarator ')' | abstract_declarator_suffix ) ( abstract_declarator_suffix )* ;
    public final CSampleParser.direct_abstract_declarator_return direct_abstract_declarator() throws RecognitionException {
        CSampleParser.direct_abstract_declarator_return retval = new CSampleParser.direct_abstract_declarator_return();
        retval.start = input.LT(1);

        int direct_abstract_declarator_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal116=null;
        Token char_literal118=null;
        CSampleParser.abstract_declarator_return abstract_declarator117 =null;

        CSampleParser.abstract_declarator_suffix_return abstract_declarator_suffix119 =null;

        CSampleParser.abstract_declarator_suffix_return abstract_declarator_suffix120 =null;


        CommonTree char_literal116_tree=null;
        CommonTree char_literal118_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 33) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:453:2: ( ( '(' abstract_declarator ')' | abstract_declarator_suffix ) ( abstract_declarator_suffix )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:453:4: ( '(' abstract_declarator ')' | abstract_declarator_suffix ) ( abstract_declarator_suffix )*
            {
            root_0 = (CommonTree)adaptor.nil();


            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:453:4: ( '(' abstract_declarator ')' | abstract_declarator_suffix )
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==30) ) {
                int LA38_1 = input.LA(2);

                if ( (LA38_1==IDENTIFIER||LA38_1==31||LA38_1==63||(LA38_1 >= 66 && LA38_1 <= 67)||LA38_1==71||(LA38_1 >= 73 && LA38_1 <= 75)||(LA38_1 >= 79 && LA38_1 <= 81)||(LA38_1 >= 83 && LA38_1 <= 84)||(LA38_1 >= 86 && LA38_1 <= 87)||(LA38_1 >= 90 && LA38_1 <= 93)) ) {
                    alt38=2;
                }
                else if ( (LA38_1==30||LA38_1==32||LA38_1==59) ) {
                    alt38=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 38, 1, input);

                    throw nvae;

                }
            }
            else if ( (LA38_0==59) ) {
                alt38=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 38, 0, input);

                throw nvae;

            }
            switch (alt38) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:453:6: '(' abstract_declarator ')'
                    {
                    char_literal116=(Token)match(input,30,FOLLOW_30_in_direct_abstract_declarator1096); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal116_tree = 
                    (CommonTree)adaptor.create(char_literal116)
                    ;
                    adaptor.addChild(root_0, char_literal116_tree);
                    }

                    pushFollow(FOLLOW_abstract_declarator_in_direct_abstract_declarator1098);
                    abstract_declarator117=abstract_declarator();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, abstract_declarator117.getTree());

                    char_literal118=(Token)match(input,31,FOLLOW_31_in_direct_abstract_declarator1100); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal118_tree = 
                    (CommonTree)adaptor.create(char_literal118)
                    ;
                    adaptor.addChild(root_0, char_literal118_tree);
                    }

                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:453:36: abstract_declarator_suffix
                    {
                    pushFollow(FOLLOW_abstract_declarator_suffix_in_direct_abstract_declarator1104);
                    abstract_declarator_suffix119=abstract_declarator_suffix();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, abstract_declarator_suffix119.getTree());

                    }
                    break;

            }


            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:453:65: ( abstract_declarator_suffix )*
            loop39:
            do {
                int alt39=2;
                int LA39_0 = input.LA(1);

                if ( (LA39_0==30) ) {
                    switch ( input.LA(2) ) {
                    case 31:
                        {
                        int LA39_8 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;
                    case IDENTIFIER:
                        {
                        int LA39_10 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;
                    case 63:
                    case 74:
                    case 81:
                    case 86:
                        {
                        int LA39_13 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;
                    case 92:
                        {
                        int LA39_14 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;
                    case 66:
                        {
                        int LA39_15 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;
                    case 83:
                        {
                        int LA39_16 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;
                    case 79:
                        {
                        int LA39_17 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;
                    case 80:
                        {
                        int LA39_18 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;
                    case 75:
                        {
                        int LA39_19 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;
                    case 71:
                        {
                        int LA39_20 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;
                    case 84:
                        {
                        int LA39_21 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;
                    case 91:
                        {
                        int LA39_22 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;
                    case 87:
                    case 90:
                        {
                        int LA39_23 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;
                    case 73:
                        {
                        int LA39_24 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;
                    case 67:
                    case 93:
                        {
                        int LA39_25 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;

                    }

                }
                else if ( (LA39_0==59) ) {
                    switch ( input.LA(2) ) {
                    case 60:
                        {
                        int LA39_26 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;
                    case 30:
                        {
                        int LA39_27 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;
                    case IDENTIFIER:
                        {
                        int LA39_28 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;
                    case CHARACTER_LITERAL:
                    case DECIMAL_LITERAL:
                    case FLOATING_POINT_LITERAL:
                    case HEX_LITERAL:
                    case OCTAL_LITERAL:
                    case STRING_LITERAL:
                        {
                        int LA39_29 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;
                    case 35:
                        {
                        int LA39_30 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;
                    case 39:
                        {
                        int LA39_31 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;
                    case 23:
                    case 28:
                    case 32:
                    case 34:
                    case 38:
                    case 100:
                        {
                        int LA39_32 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;
                    case 85:
                        {
                        int LA39_33 = input.LA(3);

                        if ( (synpred65_CSample()) ) {
                            alt39=1;
                        }


                        }
                        break;

                    }

                }


                switch (alt39) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:453:65: abstract_declarator_suffix
            	    {
            	    pushFollow(FOLLOW_abstract_declarator_suffix_in_direct_abstract_declarator1108);
            	    abstract_declarator_suffix120=abstract_declarator_suffix();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, abstract_declarator_suffix120.getTree());

            	    }
            	    break;

            	default :
            	    break loop39;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 33, direct_abstract_declarator_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "direct_abstract_declarator"


    public static class abstract_declarator_suffix_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "abstract_declarator_suffix"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:456:1: abstract_declarator_suffix : ( '[' ']' | '[' constant_expression ']' | '(' ')' | '(' parameter_type_list ')' );
    public final CSampleParser.abstract_declarator_suffix_return abstract_declarator_suffix() throws RecognitionException {
        CSampleParser.abstract_declarator_suffix_return retval = new CSampleParser.abstract_declarator_suffix_return();
        retval.start = input.LT(1);

        int abstract_declarator_suffix_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal121=null;
        Token char_literal122=null;
        Token char_literal123=null;
        Token char_literal125=null;
        Token char_literal126=null;
        Token char_literal127=null;
        Token char_literal128=null;
        Token char_literal130=null;
        CSampleParser.constant_expression_return constant_expression124 =null;

        CSampleParser.parameter_type_list_return parameter_type_list129 =null;


        CommonTree char_literal121_tree=null;
        CommonTree char_literal122_tree=null;
        CommonTree char_literal123_tree=null;
        CommonTree char_literal125_tree=null;
        CommonTree char_literal126_tree=null;
        CommonTree char_literal127_tree=null;
        CommonTree char_literal128_tree=null;
        CommonTree char_literal130_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 34) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:457:2: ( '[' ']' | '[' constant_expression ']' | '(' ')' | '(' parameter_type_list ')' )
            int alt40=4;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==59) ) {
                int LA40_1 = input.LA(2);

                if ( (LA40_1==60) ) {
                    alt40=1;
                }
                else if ( (LA40_1==CHARACTER_LITERAL||LA40_1==DECIMAL_LITERAL||LA40_1==FLOATING_POINT_LITERAL||LA40_1==HEX_LITERAL||LA40_1==IDENTIFIER||LA40_1==OCTAL_LITERAL||LA40_1==STRING_LITERAL||LA40_1==23||LA40_1==28||LA40_1==30||LA40_1==32||(LA40_1 >= 34 && LA40_1 <= 35)||(LA40_1 >= 38 && LA40_1 <= 39)||LA40_1==85||LA40_1==100) ) {
                    alt40=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 40, 1, input);

                    throw nvae;

                }
            }
            else if ( (LA40_0==30) ) {
                int LA40_2 = input.LA(2);

                if ( (LA40_2==31) ) {
                    alt40=3;
                }
                else if ( (LA40_2==IDENTIFIER||LA40_2==63||(LA40_2 >= 66 && LA40_2 <= 67)||LA40_2==71||(LA40_2 >= 73 && LA40_2 <= 75)||(LA40_2 >= 79 && LA40_2 <= 81)||(LA40_2 >= 83 && LA40_2 <= 84)||(LA40_2 >= 86 && LA40_2 <= 87)||(LA40_2 >= 90 && LA40_2 <= 93)) ) {
                    alt40=4;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 40, 2, input);

                    throw nvae;

                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 40, 0, input);

                throw nvae;

            }
            switch (alt40) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:457:4: '[' ']'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal121=(Token)match(input,59,FOLLOW_59_in_abstract_declarator_suffix1120); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal121_tree = 
                    (CommonTree)adaptor.create(char_literal121)
                    ;
                    adaptor.addChild(root_0, char_literal121_tree);
                    }

                    char_literal122=(Token)match(input,60,FOLLOW_60_in_abstract_declarator_suffix1122); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal122_tree = 
                    (CommonTree)adaptor.create(char_literal122)
                    ;
                    adaptor.addChild(root_0, char_literal122_tree);
                    }

                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:458:4: '[' constant_expression ']'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal123=(Token)match(input,59,FOLLOW_59_in_abstract_declarator_suffix1127); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal123_tree = 
                    (CommonTree)adaptor.create(char_literal123)
                    ;
                    adaptor.addChild(root_0, char_literal123_tree);
                    }

                    pushFollow(FOLLOW_constant_expression_in_abstract_declarator_suffix1129);
                    constant_expression124=constant_expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, constant_expression124.getTree());

                    char_literal125=(Token)match(input,60,FOLLOW_60_in_abstract_declarator_suffix1131); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal125_tree = 
                    (CommonTree)adaptor.create(char_literal125)
                    ;
                    adaptor.addChild(root_0, char_literal125_tree);
                    }

                    }
                    break;
                case 3 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:459:4: '(' ')'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal126=(Token)match(input,30,FOLLOW_30_in_abstract_declarator_suffix1136); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal126_tree = 
                    (CommonTree)adaptor.create(char_literal126)
                    ;
                    adaptor.addChild(root_0, char_literal126_tree);
                    }

                    char_literal127=(Token)match(input,31,FOLLOW_31_in_abstract_declarator_suffix1138); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal127_tree = 
                    (CommonTree)adaptor.create(char_literal127)
                    ;
                    adaptor.addChild(root_0, char_literal127_tree);
                    }

                    }
                    break;
                case 4 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:460:4: '(' parameter_type_list ')'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal128=(Token)match(input,30,FOLLOW_30_in_abstract_declarator_suffix1143); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal128_tree = 
                    (CommonTree)adaptor.create(char_literal128)
                    ;
                    adaptor.addChild(root_0, char_literal128_tree);
                    }

                    pushFollow(FOLLOW_parameter_type_list_in_abstract_declarator_suffix1145);
                    parameter_type_list129=parameter_type_list();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, parameter_type_list129.getTree());

                    char_literal130=(Token)match(input,31,FOLLOW_31_in_abstract_declarator_suffix1147); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal130_tree = 
                    (CommonTree)adaptor.create(char_literal130)
                    ;
                    adaptor.addChild(root_0, char_literal130_tree);
                    }

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 34, abstract_declarator_suffix_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "abstract_declarator_suffix"


    public static class initializer_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "initializer"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:463:1: initializer : ( assignment_expression | '{' initializer_list ( ',' )? '}' );
    public final CSampleParser.initializer_return initializer() throws RecognitionException {
        CSampleParser.initializer_return retval = new CSampleParser.initializer_return();
        retval.start = input.LT(1);

        int initializer_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal132=null;
        Token char_literal134=null;
        Token char_literal135=null;
        CSampleParser.assignment_expression_return assignment_expression131 =null;

        CSampleParser.initializer_list_return initializer_list133 =null;


        CommonTree char_literal132_tree=null;
        CommonTree char_literal134_tree=null;
        CommonTree char_literal135_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 35) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:464:2: ( assignment_expression | '{' initializer_list ( ',' )? '}' )
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==CHARACTER_LITERAL||LA42_0==DECIMAL_LITERAL||LA42_0==FLOATING_POINT_LITERAL||LA42_0==HEX_LITERAL||LA42_0==IDENTIFIER||LA42_0==OCTAL_LITERAL||LA42_0==STRING_LITERAL||LA42_0==23||LA42_0==28||LA42_0==30||LA42_0==32||(LA42_0 >= 34 && LA42_0 <= 35)||(LA42_0 >= 38 && LA42_0 <= 39)||LA42_0==85||LA42_0==100) ) {
                alt42=1;
            }
            else if ( (LA42_0==95) ) {
                alt42=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 42, 0, input);

                throw nvae;

            }
            switch (alt42) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:464:4: assignment_expression
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_assignment_expression_in_initializer1159);
                    assignment_expression131=assignment_expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, assignment_expression131.getTree());

                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:465:4: '{' initializer_list ( ',' )? '}'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal132=(Token)match(input,95,FOLLOW_95_in_initializer1164); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal132_tree = 
                    (CommonTree)adaptor.create(char_literal132)
                    ;
                    adaptor.addChild(root_0, char_literal132_tree);
                    }

                    pushFollow(FOLLOW_initializer_list_in_initializer1166);
                    initializer_list133=initializer_list();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, initializer_list133.getTree());

                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:465:25: ( ',' )?
                    int alt41=2;
                    int LA41_0 = input.LA(1);

                    if ( (LA41_0==37) ) {
                        alt41=1;
                    }
                    switch (alt41) {
                        case 1 :
                            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:465:25: ','
                            {
                            char_literal134=(Token)match(input,37,FOLLOW_37_in_initializer1168); if (state.failed) return retval;
                            if ( state.backtracking==0 ) {
                            char_literal134_tree = 
                            (CommonTree)adaptor.create(char_literal134)
                            ;
                            adaptor.addChild(root_0, char_literal134_tree);
                            }

                            }
                            break;

                    }


                    char_literal135=(Token)match(input,99,FOLLOW_99_in_initializer1171); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal135_tree = 
                    (CommonTree)adaptor.create(char_literal135)
                    ;
                    adaptor.addChild(root_0, char_literal135_tree);
                    }

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 35, initializer_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "initializer"


    public static class initializer_list_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "initializer_list"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:468:1: initializer_list : initializer ( ',' initializer )* ;
    public final CSampleParser.initializer_list_return initializer_list() throws RecognitionException {
        CSampleParser.initializer_list_return retval = new CSampleParser.initializer_list_return();
        retval.start = input.LT(1);

        int initializer_list_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal137=null;
        CSampleParser.initializer_return initializer136 =null;

        CSampleParser.initializer_return initializer138 =null;


        CommonTree char_literal137_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 36) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:469:2: ( initializer ( ',' initializer )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:469:4: initializer ( ',' initializer )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_initializer_in_initializer_list1182);
            initializer136=initializer();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, initializer136.getTree());

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:469:16: ( ',' initializer )*
            loop43:
            do {
                int alt43=2;
                int LA43_0 = input.LA(1);

                if ( (LA43_0==37) ) {
                    int LA43_1 = input.LA(2);

                    if ( (LA43_1==CHARACTER_LITERAL||LA43_1==DECIMAL_LITERAL||LA43_1==FLOATING_POINT_LITERAL||LA43_1==HEX_LITERAL||LA43_1==IDENTIFIER||LA43_1==OCTAL_LITERAL||LA43_1==STRING_LITERAL||LA43_1==23||LA43_1==28||LA43_1==30||LA43_1==32||(LA43_1 >= 34 && LA43_1 <= 35)||(LA43_1 >= 38 && LA43_1 <= 39)||LA43_1==85||LA43_1==95||LA43_1==100) ) {
                        alt43=1;
                    }


                }


                switch (alt43) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:469:17: ',' initializer
            	    {
            	    char_literal137=(Token)match(input,37,FOLLOW_37_in_initializer_list1185); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal137_tree = 
            	    (CommonTree)adaptor.create(char_literal137)
            	    ;
            	    adaptor.addChild(root_0, char_literal137_tree);
            	    }

            	    pushFollow(FOLLOW_initializer_in_initializer_list1187);
            	    initializer138=initializer();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, initializer138.getTree());

            	    }
            	    break;

            	default :
            	    break loop43;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 36, initializer_list_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "initializer_list"


    public static class argument_expression_list_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "argument_expression_list"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:474:1: argument_expression_list : assignment_expression ( ',' assignment_expression )* ;
    public final CSampleParser.argument_expression_list_return argument_expression_list() throws RecognitionException {
        CSampleParser.argument_expression_list_return retval = new CSampleParser.argument_expression_list_return();
        retval.start = input.LT(1);

        int argument_expression_list_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal140=null;
        CSampleParser.assignment_expression_return assignment_expression139 =null;

        CSampleParser.assignment_expression_return assignment_expression141 =null;


        CommonTree char_literal140_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 37) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:475:2: ( assignment_expression ( ',' assignment_expression )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:475:6: assignment_expression ( ',' assignment_expression )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_assignment_expression_in_argument_expression_list1204);
            assignment_expression139=assignment_expression();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, assignment_expression139.getTree());

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:475:28: ( ',' assignment_expression )*
            loop44:
            do {
                int alt44=2;
                int LA44_0 = input.LA(1);

                if ( (LA44_0==37) ) {
                    alt44=1;
                }


                switch (alt44) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:475:29: ',' assignment_expression
            	    {
            	    char_literal140=(Token)match(input,37,FOLLOW_37_in_argument_expression_list1207); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal140_tree = 
            	    (CommonTree)adaptor.create(char_literal140)
            	    ;
            	    adaptor.addChild(root_0, char_literal140_tree);
            	    }

            	    pushFollow(FOLLOW_assignment_expression_in_argument_expression_list1209);
            	    assignment_expression141=assignment_expression();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, assignment_expression141.getTree());

            	    }
            	    break;

            	default :
            	    break loop44;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 37, argument_expression_list_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "argument_expression_list"


    public static class additive_expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "additive_expression"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:478:1: additive_expression : ( multiplicative_expression ) ( '+' multiplicative_expression | '-' multiplicative_expression )* ;
    public final CSampleParser.additive_expression_return additive_expression() throws RecognitionException {
        CSampleParser.additive_expression_return retval = new CSampleParser.additive_expression_return();
        retval.start = input.LT(1);

        int additive_expression_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal143=null;
        Token char_literal145=null;
        CSampleParser.multiplicative_expression_return multiplicative_expression142 =null;

        CSampleParser.multiplicative_expression_return multiplicative_expression144 =null;

        CSampleParser.multiplicative_expression_return multiplicative_expression146 =null;


        CommonTree char_literal143_tree=null;
        CommonTree char_literal145_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 38) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:479:2: ( ( multiplicative_expression ) ( '+' multiplicative_expression | '-' multiplicative_expression )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:479:4: ( multiplicative_expression ) ( '+' multiplicative_expression | '-' multiplicative_expression )*
            {
            root_0 = (CommonTree)adaptor.nil();


            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:479:4: ( multiplicative_expression )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:479:5: multiplicative_expression
            {
            pushFollow(FOLLOW_multiplicative_expression_in_additive_expression1223);
            multiplicative_expression142=multiplicative_expression();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, multiplicative_expression142.getTree());

            }


            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:479:32: ( '+' multiplicative_expression | '-' multiplicative_expression )*
            loop45:
            do {
                int alt45=3;
                int LA45_0 = input.LA(1);

                if ( (LA45_0==34) ) {
                    alt45=1;
                }
                else if ( (LA45_0==38) ) {
                    alt45=2;
                }


                switch (alt45) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:479:33: '+' multiplicative_expression
            	    {
            	    char_literal143=(Token)match(input,34,FOLLOW_34_in_additive_expression1227); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal143_tree = 
            	    (CommonTree)adaptor.create(char_literal143)
            	    ;
            	    adaptor.addChild(root_0, char_literal143_tree);
            	    }

            	    pushFollow(FOLLOW_multiplicative_expression_in_additive_expression1229);
            	    multiplicative_expression144=multiplicative_expression();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, multiplicative_expression144.getTree());

            	    }
            	    break;
            	case 2 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:479:65: '-' multiplicative_expression
            	    {
            	    char_literal145=(Token)match(input,38,FOLLOW_38_in_additive_expression1233); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal145_tree = 
            	    (CommonTree)adaptor.create(char_literal145)
            	    ;
            	    adaptor.addChild(root_0, char_literal145_tree);
            	    }

            	    pushFollow(FOLLOW_multiplicative_expression_in_additive_expression1235);
            	    multiplicative_expression146=multiplicative_expression();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, multiplicative_expression146.getTree());

            	    }
            	    break;

            	default :
            	    break loop45;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 38, additive_expression_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "additive_expression"


    public static class multiplicative_expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "multiplicative_expression"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:482:1: multiplicative_expression : ( cast_expression ) ( '*' cast_expression | '/' cast_expression | '%' cast_expression )* ;
    public final CSampleParser.multiplicative_expression_return multiplicative_expression() throws RecognitionException {
        CSampleParser.multiplicative_expression_return retval = new CSampleParser.multiplicative_expression_return();
        retval.start = input.LT(1);

        int multiplicative_expression_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal148=null;
        Token char_literal150=null;
        Token char_literal152=null;
        CSampleParser.cast_expression_return cast_expression147 =null;

        CSampleParser.cast_expression_return cast_expression149 =null;

        CSampleParser.cast_expression_return cast_expression151 =null;

        CSampleParser.cast_expression_return cast_expression153 =null;


        CommonTree char_literal148_tree=null;
        CommonTree char_literal150_tree=null;
        CommonTree char_literal152_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 39) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:483:2: ( ( cast_expression ) ( '*' cast_expression | '/' cast_expression | '%' cast_expression )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:483:4: ( cast_expression ) ( '*' cast_expression | '/' cast_expression | '%' cast_expression )*
            {
            root_0 = (CommonTree)adaptor.nil();


            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:483:4: ( cast_expression )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:483:5: cast_expression
            {
            pushFollow(FOLLOW_cast_expression_in_multiplicative_expression1249);
            cast_expression147=cast_expression();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, cast_expression147.getTree());

            }


            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:483:22: ( '*' cast_expression | '/' cast_expression | '%' cast_expression )*
            loop46:
            do {
                int alt46=4;
                switch ( input.LA(1) ) {
                case 32:
                    {
                    alt46=1;
                    }
                    break;
                case 44:
                    {
                    alt46=2;
                    }
                    break;
                case 25:
                    {
                    alt46=3;
                    }
                    break;

                }

                switch (alt46) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:483:23: '*' cast_expression
            	    {
            	    char_literal148=(Token)match(input,32,FOLLOW_32_in_multiplicative_expression1253); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal148_tree = 
            	    (CommonTree)adaptor.create(char_literal148)
            	    ;
            	    adaptor.addChild(root_0, char_literal148_tree);
            	    }

            	    pushFollow(FOLLOW_cast_expression_in_multiplicative_expression1255);
            	    cast_expression149=cast_expression();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, cast_expression149.getTree());

            	    }
            	    break;
            	case 2 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:483:45: '/' cast_expression
            	    {
            	    char_literal150=(Token)match(input,44,FOLLOW_44_in_multiplicative_expression1259); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal150_tree = 
            	    (CommonTree)adaptor.create(char_literal150)
            	    ;
            	    adaptor.addChild(root_0, char_literal150_tree);
            	    }

            	    pushFollow(FOLLOW_cast_expression_in_multiplicative_expression1261);
            	    cast_expression151=cast_expression();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, cast_expression151.getTree());

            	    }
            	    break;
            	case 3 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:483:67: '%' cast_expression
            	    {
            	    char_literal152=(Token)match(input,25,FOLLOW_25_in_multiplicative_expression1265); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal152_tree = 
            	    (CommonTree)adaptor.create(char_literal152)
            	    ;
            	    adaptor.addChild(root_0, char_literal152_tree);
            	    }

            	    pushFollow(FOLLOW_cast_expression_in_multiplicative_expression1267);
            	    cast_expression153=cast_expression();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, cast_expression153.getTree());

            	    }
            	    break;

            	default :
            	    break loop46;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 39, multiplicative_expression_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "multiplicative_expression"


    public static class cast_expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "cast_expression"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:486:1: cast_expression : ( '(' type_name ')' cast_expression | unary_expression );
    public final CSampleParser.cast_expression_return cast_expression() throws RecognitionException {
        CSampleParser.cast_expression_return retval = new CSampleParser.cast_expression_return();
        retval.start = input.LT(1);

        int cast_expression_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal154=null;
        Token char_literal156=null;
        CSampleParser.type_name_return type_name155 =null;

        CSampleParser.cast_expression_return cast_expression157 =null;

        CSampleParser.unary_expression_return unary_expression158 =null;


        CommonTree char_literal154_tree=null;
        CommonTree char_literal156_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 40) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:487:2: ( '(' type_name ')' cast_expression | unary_expression )
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( (LA47_0==30) ) {
                switch ( input.LA(2) ) {
                case 66:
                case 67:
                case 71:
                case 73:
                case 75:
                case 79:
                case 80:
                case 83:
                case 84:
                case 87:
                case 90:
                case 91:
                case 92:
                case 93:
                    {
                    alt47=1;
                    }
                    break;
                case IDENTIFIER:
                    {
                    int LA47_20 = input.LA(3);

                    if ( (synpred78_CSample()) ) {
                        alt47=1;
                    }
                    else if ( (true) ) {
                        alt47=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return retval;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 47, 20, input);

                        throw nvae;

                    }
                    }
                    break;
                case CHARACTER_LITERAL:
                case DECIMAL_LITERAL:
                case FLOATING_POINT_LITERAL:
                case HEX_LITERAL:
                case OCTAL_LITERAL:
                case STRING_LITERAL:
                case 23:
                case 28:
                case 30:
                case 32:
                case 34:
                case 35:
                case 38:
                case 39:
                case 85:
                case 100:
                    {
                    alt47=2;
                    }
                    break;
                default:
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 47, 1, input);

                    throw nvae;

                }

            }
            else if ( (LA47_0==CHARACTER_LITERAL||LA47_0==DECIMAL_LITERAL||LA47_0==FLOATING_POINT_LITERAL||LA47_0==HEX_LITERAL||LA47_0==IDENTIFIER||LA47_0==OCTAL_LITERAL||LA47_0==STRING_LITERAL||LA47_0==23||LA47_0==28||LA47_0==32||(LA47_0 >= 34 && LA47_0 <= 35)||(LA47_0 >= 38 && LA47_0 <= 39)||LA47_0==85||LA47_0==100) ) {
                alt47=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 47, 0, input);

                throw nvae;

            }
            switch (alt47) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:487:4: '(' type_name ')' cast_expression
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal154=(Token)match(input,30,FOLLOW_30_in_cast_expression1280); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal154_tree = 
                    (CommonTree)adaptor.create(char_literal154)
                    ;
                    adaptor.addChild(root_0, char_literal154_tree);
                    }

                    pushFollow(FOLLOW_type_name_in_cast_expression1282);
                    type_name155=type_name();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, type_name155.getTree());

                    char_literal156=(Token)match(input,31,FOLLOW_31_in_cast_expression1284); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal156_tree = 
                    (CommonTree)adaptor.create(char_literal156)
                    ;
                    adaptor.addChild(root_0, char_literal156_tree);
                    }

                    pushFollow(FOLLOW_cast_expression_in_cast_expression1286);
                    cast_expression157=cast_expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, cast_expression157.getTree());

                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:488:4: unary_expression
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_unary_expression_in_cast_expression1291);
                    unary_expression158=unary_expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, unary_expression158.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 40, cast_expression_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "cast_expression"


    public static class unary_expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "unary_expression"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:491:1: unary_expression : ( postfix_expression | '++' unary_expression | '--' unary_expression | unary_operator cast_expression | 'sizeof' unary_expression | 'sizeof' '(' type_name ')' );
    public final CSampleParser.unary_expression_return unary_expression() throws RecognitionException {
        CSampleParser.unary_expression_return retval = new CSampleParser.unary_expression_return();
        retval.start = input.LT(1);

        int unary_expression_StartIndex = input.index();

        CommonTree root_0 = null;

        Token string_literal160=null;
        Token string_literal162=null;
        Token string_literal166=null;
        Token string_literal168=null;
        Token char_literal169=null;
        Token char_literal171=null;
        CSampleParser.postfix_expression_return postfix_expression159 =null;

        CSampleParser.unary_expression_return unary_expression161 =null;

        CSampleParser.unary_expression_return unary_expression163 =null;

        CSampleParser.unary_operator_return unary_operator164 =null;

        CSampleParser.cast_expression_return cast_expression165 =null;

        CSampleParser.unary_expression_return unary_expression167 =null;

        CSampleParser.type_name_return type_name170 =null;


        CommonTree string_literal160_tree=null;
        CommonTree string_literal162_tree=null;
        CommonTree string_literal166_tree=null;
        CommonTree string_literal168_tree=null;
        CommonTree char_literal169_tree=null;
        CommonTree char_literal171_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 41) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:492:2: ( postfix_expression | '++' unary_expression | '--' unary_expression | unary_operator cast_expression | 'sizeof' unary_expression | 'sizeof' '(' type_name ')' )
            int alt48=6;
            switch ( input.LA(1) ) {
            case CHARACTER_LITERAL:
            case DECIMAL_LITERAL:
            case FLOATING_POINT_LITERAL:
            case HEX_LITERAL:
            case IDENTIFIER:
            case OCTAL_LITERAL:
            case STRING_LITERAL:
            case 30:
                {
                alt48=1;
                }
                break;
            case 35:
                {
                alt48=2;
                }
                break;
            case 39:
                {
                alt48=3;
                }
                break;
            case 23:
            case 28:
            case 32:
            case 34:
            case 38:
            case 100:
                {
                alt48=4;
                }
                break;
            case 85:
                {
                int LA48_7 = input.LA(2);

                if ( (LA48_7==30) ) {
                    int LA48_8 = input.LA(3);

                    if ( (synpred83_CSample()) ) {
                        alt48=5;
                    }
                    else if ( (true) ) {
                        alt48=6;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return retval;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 48, 8, input);

                        throw nvae;

                    }
                }
                else if ( (LA48_7==CHARACTER_LITERAL||LA48_7==DECIMAL_LITERAL||LA48_7==FLOATING_POINT_LITERAL||LA48_7==HEX_LITERAL||LA48_7==IDENTIFIER||LA48_7==OCTAL_LITERAL||LA48_7==STRING_LITERAL||LA48_7==23||LA48_7==28||LA48_7==32||(LA48_7 >= 34 && LA48_7 <= 35)||(LA48_7 >= 38 && LA48_7 <= 39)||LA48_7==85||LA48_7==100) ) {
                    alt48=5;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 48, 7, input);

                    throw nvae;

                }
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 48, 0, input);

                throw nvae;

            }

            switch (alt48) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:492:4: postfix_expression
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_postfix_expression_in_unary_expression1302);
                    postfix_expression159=postfix_expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, postfix_expression159.getTree());

                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:493:4: '++' unary_expression
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal160=(Token)match(input,35,FOLLOW_35_in_unary_expression1307); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal160_tree = 
                    (CommonTree)adaptor.create(string_literal160)
                    ;
                    adaptor.addChild(root_0, string_literal160_tree);
                    }

                    pushFollow(FOLLOW_unary_expression_in_unary_expression1309);
                    unary_expression161=unary_expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, unary_expression161.getTree());

                    }
                    break;
                case 3 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:494:4: '--' unary_expression
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal162=(Token)match(input,39,FOLLOW_39_in_unary_expression1314); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal162_tree = 
                    (CommonTree)adaptor.create(string_literal162)
                    ;
                    adaptor.addChild(root_0, string_literal162_tree);
                    }

                    pushFollow(FOLLOW_unary_expression_in_unary_expression1316);
                    unary_expression163=unary_expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, unary_expression163.getTree());

                    }
                    break;
                case 4 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:495:4: unary_operator cast_expression
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_unary_operator_in_unary_expression1321);
                    unary_operator164=unary_operator();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, unary_operator164.getTree());

                    pushFollow(FOLLOW_cast_expression_in_unary_expression1323);
                    cast_expression165=cast_expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, cast_expression165.getTree());

                    }
                    break;
                case 5 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:496:4: 'sizeof' unary_expression
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal166=(Token)match(input,85,FOLLOW_85_in_unary_expression1328); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal166_tree = 
                    (CommonTree)adaptor.create(string_literal166)
                    ;
                    adaptor.addChild(root_0, string_literal166_tree);
                    }

                    pushFollow(FOLLOW_unary_expression_in_unary_expression1330);
                    unary_expression167=unary_expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, unary_expression167.getTree());

                    }
                    break;
                case 6 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:497:4: 'sizeof' '(' type_name ')'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal168=(Token)match(input,85,FOLLOW_85_in_unary_expression1335); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal168_tree = 
                    (CommonTree)adaptor.create(string_literal168)
                    ;
                    adaptor.addChild(root_0, string_literal168_tree);
                    }

                    char_literal169=(Token)match(input,30,FOLLOW_30_in_unary_expression1337); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal169_tree = 
                    (CommonTree)adaptor.create(char_literal169)
                    ;
                    adaptor.addChild(root_0, char_literal169_tree);
                    }

                    pushFollow(FOLLOW_type_name_in_unary_expression1339);
                    type_name170=type_name();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, type_name170.getTree());

                    char_literal171=(Token)match(input,31,FOLLOW_31_in_unary_expression1341); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal171_tree = 
                    (CommonTree)adaptor.create(char_literal171)
                    ;
                    adaptor.addChild(root_0, char_literal171_tree);
                    }

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 41, unary_expression_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "unary_expression"


    public static class postfix_expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "postfix_expression"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:500:1: postfix_expression : primary_expression ( '[' expression ']' | '(' ')' | '(' argument_expression_list ')' | '.' IDENTIFIER | '->' IDENTIFIER | '++' | '--' )* ;
    public final CSampleParser.postfix_expression_return postfix_expression() throws RecognitionException {
        CSampleParser.postfix_expression_return retval = new CSampleParser.postfix_expression_return();
        retval.start = input.LT(1);

        int postfix_expression_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal173=null;
        Token char_literal175=null;
        Token char_literal176=null;
        Token char_literal177=null;
        Token char_literal178=null;
        Token char_literal180=null;
        Token char_literal181=null;
        Token IDENTIFIER182=null;
        Token string_literal183=null;
        Token IDENTIFIER184=null;
        Token string_literal185=null;
        Token string_literal186=null;
        CSampleParser.primary_expression_return primary_expression172 =null;

        CSampleParser.expression_return expression174 =null;

        CSampleParser.argument_expression_list_return argument_expression_list179 =null;


        CommonTree char_literal173_tree=null;
        CommonTree char_literal175_tree=null;
        CommonTree char_literal176_tree=null;
        CommonTree char_literal177_tree=null;
        CommonTree char_literal178_tree=null;
        CommonTree char_literal180_tree=null;
        CommonTree char_literal181_tree=null;
        CommonTree IDENTIFIER182_tree=null;
        CommonTree string_literal183_tree=null;
        CommonTree IDENTIFIER184_tree=null;
        CommonTree string_literal185_tree=null;
        CommonTree string_literal186_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 42) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:501:2: ( primary_expression ( '[' expression ']' | '(' ')' | '(' argument_expression_list ')' | '.' IDENTIFIER | '->' IDENTIFIER | '++' | '--' )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:501:6: primary_expression ( '[' expression ']' | '(' ')' | '(' argument_expression_list ')' | '.' IDENTIFIER | '->' IDENTIFIER | '++' | '--' )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_primary_expression_in_postfix_expression1354);
            primary_expression172=primary_expression();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, primary_expression172.getTree());

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:502:9: ( '[' expression ']' | '(' ')' | '(' argument_expression_list ')' | '.' IDENTIFIER | '->' IDENTIFIER | '++' | '--' )*
            loop49:
            do {
                int alt49=8;
                switch ( input.LA(1) ) {
                case 59:
                    {
                    alt49=1;
                    }
                    break;
                case 30:
                    {
                    int LA49_24 = input.LA(2);

                    if ( (LA49_24==31) ) {
                        alt49=2;
                    }
                    else if ( (LA49_24==CHARACTER_LITERAL||LA49_24==DECIMAL_LITERAL||LA49_24==FLOATING_POINT_LITERAL||LA49_24==HEX_LITERAL||LA49_24==IDENTIFIER||LA49_24==OCTAL_LITERAL||LA49_24==STRING_LITERAL||LA49_24==23||LA49_24==28||LA49_24==30||LA49_24==32||(LA49_24 >= 34 && LA49_24 <= 35)||(LA49_24 >= 38 && LA49_24 <= 39)||LA49_24==85||LA49_24==100) ) {
                        alt49=3;
                    }


                    }
                    break;
                case 42:
                    {
                    alt49=4;
                    }
                    break;
                case 41:
                    {
                    alt49=5;
                    }
                    break;
                case 35:
                    {
                    alt49=6;
                    }
                    break;
                case 39:
                    {
                    alt49=7;
                    }
                    break;

                }

                switch (alt49) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:502:13: '[' expression ']'
            	    {
            	    char_literal173=(Token)match(input,59,FOLLOW_59_in_postfix_expression1368); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal173_tree = 
            	    (CommonTree)adaptor.create(char_literal173)
            	    ;
            	    adaptor.addChild(root_0, char_literal173_tree);
            	    }

            	    pushFollow(FOLLOW_expression_in_postfix_expression1370);
            	    expression174=expression();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, expression174.getTree());

            	    char_literal175=(Token)match(input,60,FOLLOW_60_in_postfix_expression1372); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal175_tree = 
            	    (CommonTree)adaptor.create(char_literal175)
            	    ;
            	    adaptor.addChild(root_0, char_literal175_tree);
            	    }

            	    }
            	    break;
            	case 2 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:503:13: '(' ')'
            	    {
            	    char_literal176=(Token)match(input,30,FOLLOW_30_in_postfix_expression1386); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal176_tree = 
            	    (CommonTree)adaptor.create(char_literal176)
            	    ;
            	    adaptor.addChild(root_0, char_literal176_tree);
            	    }

            	    char_literal177=(Token)match(input,31,FOLLOW_31_in_postfix_expression1388); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal177_tree = 
            	    (CommonTree)adaptor.create(char_literal177)
            	    ;
            	    adaptor.addChild(root_0, char_literal177_tree);
            	    }

            	    }
            	    break;
            	case 3 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:504:13: '(' argument_expression_list ')'
            	    {
            	    char_literal178=(Token)match(input,30,FOLLOW_30_in_postfix_expression1402); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal178_tree = 
            	    (CommonTree)adaptor.create(char_literal178)
            	    ;
            	    adaptor.addChild(root_0, char_literal178_tree);
            	    }

            	    pushFollow(FOLLOW_argument_expression_list_in_postfix_expression1404);
            	    argument_expression_list179=argument_expression_list();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, argument_expression_list179.getTree());

            	    char_literal180=(Token)match(input,31,FOLLOW_31_in_postfix_expression1406); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal180_tree = 
            	    (CommonTree)adaptor.create(char_literal180)
            	    ;
            	    adaptor.addChild(root_0, char_literal180_tree);
            	    }

            	    }
            	    break;
            	case 4 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:505:13: '.' IDENTIFIER
            	    {
            	    char_literal181=(Token)match(input,42,FOLLOW_42_in_postfix_expression1420); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal181_tree = 
            	    (CommonTree)adaptor.create(char_literal181)
            	    ;
            	    adaptor.addChild(root_0, char_literal181_tree);
            	    }

            	    IDENTIFIER182=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_postfix_expression1422); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    IDENTIFIER182_tree = 
            	    (CommonTree)adaptor.create(IDENTIFIER182)
            	    ;
            	    adaptor.addChild(root_0, IDENTIFIER182_tree);
            	    }

            	    }
            	    break;
            	case 5 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:506:13: '->' IDENTIFIER
            	    {
            	    string_literal183=(Token)match(input,41,FOLLOW_41_in_postfix_expression1436); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    string_literal183_tree = 
            	    (CommonTree)adaptor.create(string_literal183)
            	    ;
            	    adaptor.addChild(root_0, string_literal183_tree);
            	    }

            	    IDENTIFIER184=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_postfix_expression1438); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    IDENTIFIER184_tree = 
            	    (CommonTree)adaptor.create(IDENTIFIER184)
            	    ;
            	    adaptor.addChild(root_0, IDENTIFIER184_tree);
            	    }

            	    }
            	    break;
            	case 6 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:507:13: '++'
            	    {
            	    string_literal185=(Token)match(input,35,FOLLOW_35_in_postfix_expression1452); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    string_literal185_tree = 
            	    (CommonTree)adaptor.create(string_literal185)
            	    ;
            	    adaptor.addChild(root_0, string_literal185_tree);
            	    }

            	    }
            	    break;
            	case 7 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:508:13: '--'
            	    {
            	    string_literal186=(Token)match(input,39,FOLLOW_39_in_postfix_expression1466); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    string_literal186_tree = 
            	    (CommonTree)adaptor.create(string_literal186)
            	    ;
            	    adaptor.addChild(root_0, string_literal186_tree);
            	    }

            	    }
            	    break;

            	default :
            	    break loop49;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 42, postfix_expression_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "postfix_expression"


    public static class unary_operator_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "unary_operator"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:512:1: unary_operator : ( '&' | '*' | '+' | '-' | '~' | '!' );
    public final CSampleParser.unary_operator_return unary_operator() throws RecognitionException {
        CSampleParser.unary_operator_return retval = new CSampleParser.unary_operator_return();
        retval.start = input.LT(1);

        int unary_operator_StartIndex = input.index();

        CommonTree root_0 = null;

        Token set187=null;

        CommonTree set187_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 43) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:513:2: ( '&' | '*' | '+' | '-' | '~' | '!' )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:
            {
            root_0 = (CommonTree)adaptor.nil();


            set187=(Token)input.LT(1);

            if ( input.LA(1)==23||input.LA(1)==28||input.LA(1)==32||input.LA(1)==34||input.LA(1)==38||input.LA(1)==100 ) {
                input.consume();
                if ( state.backtracking==0 ) adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(set187)
                );
                state.errorRecovery=false;
                state.failed=false;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 43, unary_operator_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "unary_operator"


    public static class primary_expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "primary_expression"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:521:1: primary_expression : ( IDENTIFIER | constant | '(' expression ')' );
    public final CSampleParser.primary_expression_return primary_expression() throws RecognitionException {
        CSampleParser.primary_expression_return retval = new CSampleParser.primary_expression_return();
        retval.start = input.LT(1);

        int primary_expression_StartIndex = input.index();

        CommonTree root_0 = null;

        Token IDENTIFIER188=null;
        Token char_literal190=null;
        Token char_literal192=null;
        CSampleParser.constant_return constant189 =null;

        CSampleParser.expression_return expression191 =null;


        CommonTree IDENTIFIER188_tree=null;
        CommonTree char_literal190_tree=null;
        CommonTree char_literal192_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 44) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:522:2: ( IDENTIFIER | constant | '(' expression ')' )
            int alt50=3;
            switch ( input.LA(1) ) {
            case IDENTIFIER:
                {
                alt50=1;
                }
                break;
            case CHARACTER_LITERAL:
            case DECIMAL_LITERAL:
            case FLOATING_POINT_LITERAL:
            case HEX_LITERAL:
            case OCTAL_LITERAL:
            case STRING_LITERAL:
                {
                alt50=2;
                }
                break;
            case 30:
                {
                alt50=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 50, 0, input);

                throw nvae;

            }

            switch (alt50) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:522:4: IDENTIFIER
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    IDENTIFIER188=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_primary_expression1524); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    IDENTIFIER188_tree = 
                    (CommonTree)adaptor.create(IDENTIFIER188)
                    ;
                    adaptor.addChild(root_0, IDENTIFIER188_tree);
                    }

                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:523:4: constant
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_constant_in_primary_expression1529);
                    constant189=constant();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, constant189.getTree());

                    }
                    break;
                case 3 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:524:4: '(' expression ')'
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal190=(Token)match(input,30,FOLLOW_30_in_primary_expression1534); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal190_tree = 
                    (CommonTree)adaptor.create(char_literal190)
                    ;
                    adaptor.addChild(root_0, char_literal190_tree);
                    }

                    pushFollow(FOLLOW_expression_in_primary_expression1536);
                    expression191=expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, expression191.getTree());

                    char_literal192=(Token)match(input,31,FOLLOW_31_in_primary_expression1538); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal192_tree = 
                    (CommonTree)adaptor.create(char_literal192)
                    ;
                    adaptor.addChild(root_0, char_literal192_tree);
                    }

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 44, primary_expression_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "primary_expression"


    public static class constant_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "constant"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:527:1: constant : ( HEX_LITERAL | OCTAL_LITERAL | DECIMAL_LITERAL | CHARACTER_LITERAL | STRING_LITERAL | FLOATING_POINT_LITERAL );
    public final CSampleParser.constant_return constant() throws RecognitionException {
        CSampleParser.constant_return retval = new CSampleParser.constant_return();
        retval.start = input.LT(1);

        int constant_StartIndex = input.index();

        CommonTree root_0 = null;

        Token set193=null;

        CommonTree set193_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 45) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:528:5: ( HEX_LITERAL | OCTAL_LITERAL | DECIMAL_LITERAL | CHARACTER_LITERAL | STRING_LITERAL | FLOATING_POINT_LITERAL )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:
            {
            root_0 = (CommonTree)adaptor.nil();


            set193=(Token)input.LT(1);

            if ( input.LA(1)==CHARACTER_LITERAL||input.LA(1)==DECIMAL_LITERAL||input.LA(1)==FLOATING_POINT_LITERAL||input.LA(1)==HEX_LITERAL||input.LA(1)==OCTAL_LITERAL||input.LA(1)==STRING_LITERAL ) {
                input.consume();
                if ( state.backtracking==0 ) adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(set193)
                );
                state.errorRecovery=false;
                state.failed=false;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 45, constant_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "constant"


    public static class expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "expression"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:539:1: expression : assignment_expression ( ',' assignment_expression )* ;
    public final CSampleParser.expression_return expression() throws RecognitionException {
        CSampleParser.expression_return retval = new CSampleParser.expression_return();
        retval.start = input.LT(1);

        int expression_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal195=null;
        CSampleParser.assignment_expression_return assignment_expression194 =null;

        CSampleParser.assignment_expression_return assignment_expression196 =null;


        CommonTree char_literal195_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 46) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:540:2: ( assignment_expression ( ',' assignment_expression )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:540:4: assignment_expression ( ',' assignment_expression )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_assignment_expression_in_expression1614);
            assignment_expression194=assignment_expression();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, assignment_expression194.getTree());

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:540:26: ( ',' assignment_expression )*
            loop51:
            do {
                int alt51=2;
                int LA51_0 = input.LA(1);

                if ( (LA51_0==37) ) {
                    alt51=1;
                }


                switch (alt51) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:540:27: ',' assignment_expression
            	    {
            	    char_literal195=(Token)match(input,37,FOLLOW_37_in_expression1617); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal195_tree = 
            	    (CommonTree)adaptor.create(char_literal195)
            	    ;
            	    adaptor.addChild(root_0, char_literal195_tree);
            	    }

            	    pushFollow(FOLLOW_assignment_expression_in_expression1619);
            	    assignment_expression196=assignment_expression();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, assignment_expression196.getTree());

            	    }
            	    break;

            	default :
            	    break loop51;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 46, expression_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "expression"


    public static class constant_expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "constant_expression"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:543:1: constant_expression : conditional_expression ;
    public final CSampleParser.constant_expression_return constant_expression() throws RecognitionException {
        CSampleParser.constant_expression_return retval = new CSampleParser.constant_expression_return();
        retval.start = input.LT(1);

        int constant_expression_StartIndex = input.index();

        CommonTree root_0 = null;

        CSampleParser.conditional_expression_return conditional_expression197 =null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 47) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:544:2: ( conditional_expression )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:544:4: conditional_expression
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_conditional_expression_in_constant_expression1632);
            conditional_expression197=conditional_expression();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, conditional_expression197.getTree());

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 47, constant_expression_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "constant_expression"


    public static class assignment_expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "assignment_expression"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:547:1: assignment_expression : ( lvalue assignment_operator assignment_expression | conditional_expression );
    public final CSampleParser.assignment_expression_return assignment_expression() throws RecognitionException {
        CSampleParser.assignment_expression_return retval = new CSampleParser.assignment_expression_return();
        retval.start = input.LT(1);

        int assignment_expression_StartIndex = input.index();

        CommonTree root_0 = null;

        CSampleParser.lvalue_return lvalue198 =null;

        CSampleParser.assignment_operator_return assignment_operator199 =null;

        CSampleParser.assignment_expression_return assignment_expression200 =null;

        CSampleParser.conditional_expression_return conditional_expression201 =null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 48) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:548:2: ( lvalue assignment_operator assignment_expression | conditional_expression )
            int alt52=2;
            alt52 = dfa52.predict(input);
            switch (alt52) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:548:4: lvalue assignment_operator assignment_expression
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_lvalue_in_assignment_expression1643);
                    lvalue198=lvalue();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, lvalue198.getTree());

                    pushFollow(FOLLOW_assignment_operator_in_assignment_expression1645);
                    assignment_operator199=assignment_operator();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, assignment_operator199.getTree());

                    pushFollow(FOLLOW_assignment_expression_in_assignment_expression1647);
                    assignment_expression200=assignment_expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, assignment_expression200.getTree());

                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:549:4: conditional_expression
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_conditional_expression_in_assignment_expression1652);
                    conditional_expression201=conditional_expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, conditional_expression201.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 48, assignment_expression_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "assignment_expression"


    public static class lvalue_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "lvalue"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:552:1: lvalue : unary_expression ;
    public final CSampleParser.lvalue_return lvalue() throws RecognitionException {
        CSampleParser.lvalue_return retval = new CSampleParser.lvalue_return();
        retval.start = input.LT(1);

        int lvalue_StartIndex = input.index();

        CommonTree root_0 = null;

        CSampleParser.unary_expression_return unary_expression202 =null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 49) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:553:2: ( unary_expression )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:553:4: unary_expression
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_unary_expression_in_lvalue1664);
            unary_expression202=unary_expression();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, unary_expression202.getTree());

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 49, lvalue_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "lvalue"


    public static class assignment_operator_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "assignment_operator"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:556:1: assignment_operator : ( '=' | '*=' | '/=' | '%=' | '+=' | '-=' | '<<=' | '>>=' | '&=' | '^=' | '|=' );
    public final CSampleParser.assignment_operator_return assignment_operator() throws RecognitionException {
        CSampleParser.assignment_operator_return retval = new CSampleParser.assignment_operator_return();
        retval.start = input.LT(1);

        int assignment_operator_StartIndex = input.index();

        CommonTree root_0 = null;

        Token set203=null;

        CommonTree set203_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 50) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:557:2: ( '=' | '*=' | '/=' | '%=' | '+=' | '-=' | '<<=' | '>>=' | '&=' | '^=' | '|=' )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:
            {
            root_0 = (CommonTree)adaptor.nil();


            set203=(Token)input.LT(1);

            if ( input.LA(1)==26||input.LA(1)==29||input.LA(1)==33||input.LA(1)==36||input.LA(1)==40||input.LA(1)==45||input.LA(1)==50||input.LA(1)==52||input.LA(1)==57||input.LA(1)==62||input.LA(1)==97 ) {
                input.consume();
                if ( state.backtracking==0 ) adaptor.addChild(root_0, 
                (CommonTree)adaptor.create(set203)
                );
                state.errorRecovery=false;
                state.failed=false;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 50, assignment_operator_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "assignment_operator"


    public static class conditional_expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "conditional_expression"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:570:1: conditional_expression : logical_or_expression ( '?' expression ':' conditional_expression )? ;
    public final CSampleParser.conditional_expression_return conditional_expression() throws RecognitionException {
        CSampleParser.conditional_expression_return retval = new CSampleParser.conditional_expression_return();
        retval.start = input.LT(1);

        int conditional_expression_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal205=null;
        Token char_literal207=null;
        CSampleParser.logical_or_expression_return logical_or_expression204 =null;

        CSampleParser.expression_return expression206 =null;

        CSampleParser.conditional_expression_return conditional_expression208 =null;


        CommonTree char_literal205_tree=null;
        CommonTree char_literal207_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 51) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:571:2: ( logical_or_expression ( '?' expression ':' conditional_expression )? )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:571:4: logical_or_expression ( '?' expression ':' conditional_expression )?
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_logical_or_expression_in_conditional_expression1736);
            logical_or_expression204=logical_or_expression();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, logical_or_expression204.getTree());

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:571:26: ( '?' expression ':' conditional_expression )?
            int alt53=2;
            int LA53_0 = input.LA(1);

            if ( (LA53_0==58) ) {
                alt53=1;
            }
            switch (alt53) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:571:27: '?' expression ':' conditional_expression
                    {
                    char_literal205=(Token)match(input,58,FOLLOW_58_in_conditional_expression1739); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal205_tree = 
                    (CommonTree)adaptor.create(char_literal205)
                    ;
                    adaptor.addChild(root_0, char_literal205_tree);
                    }

                    pushFollow(FOLLOW_expression_in_conditional_expression1741);
                    expression206=expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, expression206.getTree());

                    char_literal207=(Token)match(input,46,FOLLOW_46_in_conditional_expression1743); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal207_tree = 
                    (CommonTree)adaptor.create(char_literal207)
                    ;
                    adaptor.addChild(root_0, char_literal207_tree);
                    }

                    pushFollow(FOLLOW_conditional_expression_in_conditional_expression1745);
                    conditional_expression208=conditional_expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, conditional_expression208.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 51, conditional_expression_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "conditional_expression"


    public static class logical_or_expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "logical_or_expression"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:574:1: logical_or_expression : logical_and_expression ( '||' logical_and_expression )* ;
    public final CSampleParser.logical_or_expression_return logical_or_expression() throws RecognitionException {
        CSampleParser.logical_or_expression_return retval = new CSampleParser.logical_or_expression_return();
        retval.start = input.LT(1);

        int logical_or_expression_StartIndex = input.index();

        CommonTree root_0 = null;

        Token string_literal210=null;
        CSampleParser.logical_and_expression_return logical_and_expression209 =null;

        CSampleParser.logical_and_expression_return logical_and_expression211 =null;


        CommonTree string_literal210_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 52) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:575:2: ( logical_and_expression ( '||' logical_and_expression )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:575:4: logical_and_expression ( '||' logical_and_expression )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_logical_and_expression_in_logical_or_expression1758);
            logical_and_expression209=logical_and_expression();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, logical_and_expression209.getTree());

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:575:27: ( '||' logical_and_expression )*
            loop54:
            do {
                int alt54=2;
                int LA54_0 = input.LA(1);

                if ( (LA54_0==98) ) {
                    alt54=1;
                }


                switch (alt54) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:575:28: '||' logical_and_expression
            	    {
            	    string_literal210=(Token)match(input,98,FOLLOW_98_in_logical_or_expression1761); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    string_literal210_tree = 
            	    (CommonTree)adaptor.create(string_literal210)
            	    ;
            	    adaptor.addChild(root_0, string_literal210_tree);
            	    }

            	    pushFollow(FOLLOW_logical_and_expression_in_logical_or_expression1763);
            	    logical_and_expression211=logical_and_expression();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, logical_and_expression211.getTree());

            	    }
            	    break;

            	default :
            	    break loop54;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 52, logical_or_expression_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "logical_or_expression"


    public static class logical_and_expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "logical_and_expression"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:578:1: logical_and_expression : inclusive_or_expression ( '&&' inclusive_or_expression )* ;
    public final CSampleParser.logical_and_expression_return logical_and_expression() throws RecognitionException {
        CSampleParser.logical_and_expression_return retval = new CSampleParser.logical_and_expression_return();
        retval.start = input.LT(1);

        int logical_and_expression_StartIndex = input.index();

        CommonTree root_0 = null;

        Token string_literal213=null;
        CSampleParser.inclusive_or_expression_return inclusive_or_expression212 =null;

        CSampleParser.inclusive_or_expression_return inclusive_or_expression214 =null;


        CommonTree string_literal213_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 53) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:579:2: ( inclusive_or_expression ( '&&' inclusive_or_expression )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:579:4: inclusive_or_expression ( '&&' inclusive_or_expression )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_inclusive_or_expression_in_logical_and_expression1776);
            inclusive_or_expression212=inclusive_or_expression();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, inclusive_or_expression212.getTree());

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:579:28: ( '&&' inclusive_or_expression )*
            loop55:
            do {
                int alt55=2;
                int LA55_0 = input.LA(1);

                if ( (LA55_0==27) ) {
                    alt55=1;
                }


                switch (alt55) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:579:29: '&&' inclusive_or_expression
            	    {
            	    string_literal213=(Token)match(input,27,FOLLOW_27_in_logical_and_expression1779); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    string_literal213_tree = 
            	    (CommonTree)adaptor.create(string_literal213)
            	    ;
            	    adaptor.addChild(root_0, string_literal213_tree);
            	    }

            	    pushFollow(FOLLOW_inclusive_or_expression_in_logical_and_expression1781);
            	    inclusive_or_expression214=inclusive_or_expression();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, inclusive_or_expression214.getTree());

            	    }
            	    break;

            	default :
            	    break loop55;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 53, logical_and_expression_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "logical_and_expression"


    public static class inclusive_or_expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "inclusive_or_expression"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:582:1: inclusive_or_expression : exclusive_or_expression ( '|' exclusive_or_expression )* ;
    public final CSampleParser.inclusive_or_expression_return inclusive_or_expression() throws RecognitionException {
        CSampleParser.inclusive_or_expression_return retval = new CSampleParser.inclusive_or_expression_return();
        retval.start = input.LT(1);

        int inclusive_or_expression_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal216=null;
        CSampleParser.exclusive_or_expression_return exclusive_or_expression215 =null;

        CSampleParser.exclusive_or_expression_return exclusive_or_expression217 =null;


        CommonTree char_literal216_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 54) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:583:2: ( exclusive_or_expression ( '|' exclusive_or_expression )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:583:4: exclusive_or_expression ( '|' exclusive_or_expression )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_exclusive_or_expression_in_inclusive_or_expression1794);
            exclusive_or_expression215=exclusive_or_expression();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, exclusive_or_expression215.getTree());

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:583:28: ( '|' exclusive_or_expression )*
            loop56:
            do {
                int alt56=2;
                int LA56_0 = input.LA(1);

                if ( (LA56_0==96) ) {
                    alt56=1;
                }


                switch (alt56) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:583:29: '|' exclusive_or_expression
            	    {
            	    char_literal216=(Token)match(input,96,FOLLOW_96_in_inclusive_or_expression1797); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal216_tree = 
            	    (CommonTree)adaptor.create(char_literal216)
            	    ;
            	    adaptor.addChild(root_0, char_literal216_tree);
            	    }

            	    pushFollow(FOLLOW_exclusive_or_expression_in_inclusive_or_expression1799);
            	    exclusive_or_expression217=exclusive_or_expression();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, exclusive_or_expression217.getTree());

            	    }
            	    break;

            	default :
            	    break loop56;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 54, inclusive_or_expression_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "inclusive_or_expression"


    public static class exclusive_or_expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "exclusive_or_expression"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:586:1: exclusive_or_expression : and_expression ( '^' and_expression )* ;
    public final CSampleParser.exclusive_or_expression_return exclusive_or_expression() throws RecognitionException {
        CSampleParser.exclusive_or_expression_return retval = new CSampleParser.exclusive_or_expression_return();
        retval.start = input.LT(1);

        int exclusive_or_expression_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal219=null;
        CSampleParser.and_expression_return and_expression218 =null;

        CSampleParser.and_expression_return and_expression220 =null;


        CommonTree char_literal219_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 55) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:587:2: ( and_expression ( '^' and_expression )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:587:4: and_expression ( '^' and_expression )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_and_expression_in_exclusive_or_expression1812);
            and_expression218=and_expression();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, and_expression218.getTree());

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:587:19: ( '^' and_expression )*
            loop57:
            do {
                int alt57=2;
                int LA57_0 = input.LA(1);

                if ( (LA57_0==61) ) {
                    alt57=1;
                }


                switch (alt57) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:587:20: '^' and_expression
            	    {
            	    char_literal219=(Token)match(input,61,FOLLOW_61_in_exclusive_or_expression1815); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal219_tree = 
            	    (CommonTree)adaptor.create(char_literal219)
            	    ;
            	    adaptor.addChild(root_0, char_literal219_tree);
            	    }

            	    pushFollow(FOLLOW_and_expression_in_exclusive_or_expression1817);
            	    and_expression220=and_expression();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, and_expression220.getTree());

            	    }
            	    break;

            	default :
            	    break loop57;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 55, exclusive_or_expression_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "exclusive_or_expression"


    public static class and_expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "and_expression"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:590:1: and_expression : equality_expression ( '&' equality_expression )* ;
    public final CSampleParser.and_expression_return and_expression() throws RecognitionException {
        CSampleParser.and_expression_return retval = new CSampleParser.and_expression_return();
        retval.start = input.LT(1);

        int and_expression_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal222=null;
        CSampleParser.equality_expression_return equality_expression221 =null;

        CSampleParser.equality_expression_return equality_expression223 =null;


        CommonTree char_literal222_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 56) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:591:2: ( equality_expression ( '&' equality_expression )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:591:4: equality_expression ( '&' equality_expression )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_equality_expression_in_and_expression1830);
            equality_expression221=equality_expression();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, equality_expression221.getTree());

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:591:24: ( '&' equality_expression )*
            loop58:
            do {
                int alt58=2;
                int LA58_0 = input.LA(1);

                if ( (LA58_0==28) ) {
                    alt58=1;
                }


                switch (alt58) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:591:25: '&' equality_expression
            	    {
            	    char_literal222=(Token)match(input,28,FOLLOW_28_in_and_expression1833); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal222_tree = 
            	    (CommonTree)adaptor.create(char_literal222)
            	    ;
            	    adaptor.addChild(root_0, char_literal222_tree);
            	    }

            	    pushFollow(FOLLOW_equality_expression_in_and_expression1835);
            	    equality_expression223=equality_expression();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, equality_expression223.getTree());

            	    }
            	    break;

            	default :
            	    break loop58;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 56, and_expression_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "and_expression"


    public static class equality_expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "equality_expression"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:593:1: equality_expression : relational_expression ( ( '==' | '!=' ) relational_expression )* ;
    public final CSampleParser.equality_expression_return equality_expression() throws RecognitionException {
        CSampleParser.equality_expression_return retval = new CSampleParser.equality_expression_return();
        retval.start = input.LT(1);

        int equality_expression_StartIndex = input.index();

        CommonTree root_0 = null;

        Token set225=null;
        CSampleParser.relational_expression_return relational_expression224 =null;

        CSampleParser.relational_expression_return relational_expression226 =null;


        CommonTree set225_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 57) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:594:2: ( relational_expression ( ( '==' | '!=' ) relational_expression )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:594:4: relational_expression ( ( '==' | '!=' ) relational_expression )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_relational_expression_in_equality_expression1847);
            relational_expression224=relational_expression();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, relational_expression224.getTree());

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:594:26: ( ( '==' | '!=' ) relational_expression )*
            loop59:
            do {
                int alt59=2;
                int LA59_0 = input.LA(1);

                if ( (LA59_0==24||LA59_0==53) ) {
                    alt59=1;
                }


                switch (alt59) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:594:27: ( '==' | '!=' ) relational_expression
            	    {
            	    set225=(Token)input.LT(1);

            	    if ( input.LA(1)==24||input.LA(1)==53 ) {
            	        input.consume();
            	        if ( state.backtracking==0 ) adaptor.addChild(root_0, 
            	        (CommonTree)adaptor.create(set225)
            	        );
            	        state.errorRecovery=false;
            	        state.failed=false;
            	    }
            	    else {
            	        if (state.backtracking>0) {state.failed=true; return retval;}
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        throw mse;
            	    }


            	    pushFollow(FOLLOW_relational_expression_in_equality_expression1856);
            	    relational_expression226=relational_expression();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, relational_expression226.getTree());

            	    }
            	    break;

            	default :
            	    break loop59;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 57, equality_expression_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "equality_expression"


    public static class relational_expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "relational_expression"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:597:1: relational_expression : shift_expression ( ( '<' | '>' | '<=' | '>=' ) shift_expression )* ;
    public final CSampleParser.relational_expression_return relational_expression() throws RecognitionException {
        CSampleParser.relational_expression_return retval = new CSampleParser.relational_expression_return();
        retval.start = input.LT(1);

        int relational_expression_StartIndex = input.index();

        CommonTree root_0 = null;

        Token set228=null;
        CSampleParser.shift_expression_return shift_expression227 =null;

        CSampleParser.shift_expression_return shift_expression229 =null;


        CommonTree set228_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 58) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:598:2: ( shift_expression ( ( '<' | '>' | '<=' | '>=' ) shift_expression )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:598:4: shift_expression ( ( '<' | '>' | '<=' | '>=' ) shift_expression )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_shift_expression_in_relational_expression1869);
            shift_expression227=shift_expression();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, shift_expression227.getTree());

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:598:21: ( ( '<' | '>' | '<=' | '>=' ) shift_expression )*
            loop60:
            do {
                int alt60=2;
                int LA60_0 = input.LA(1);

                if ( (LA60_0==48||LA60_0==51||(LA60_0 >= 54 && LA60_0 <= 55)) ) {
                    alt60=1;
                }


                switch (alt60) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:598:22: ( '<' | '>' | '<=' | '>=' ) shift_expression
            	    {
            	    set228=(Token)input.LT(1);

            	    if ( input.LA(1)==48||input.LA(1)==51||(input.LA(1) >= 54 && input.LA(1) <= 55) ) {
            	        input.consume();
            	        if ( state.backtracking==0 ) adaptor.addChild(root_0, 
            	        (CommonTree)adaptor.create(set228)
            	        );
            	        state.errorRecovery=false;
            	        state.failed=false;
            	    }
            	    else {
            	        if (state.backtracking>0) {state.failed=true; return retval;}
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        throw mse;
            	    }


            	    pushFollow(FOLLOW_shift_expression_in_relational_expression1882);
            	    shift_expression229=shift_expression();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, shift_expression229.getTree());

            	    }
            	    break;

            	default :
            	    break loop60;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 58, relational_expression_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "relational_expression"


    public static class shift_expression_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "shift_expression"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:601:1: shift_expression : additive_expression ( ( '<<' | '>>' ) additive_expression )* ;
    public final CSampleParser.shift_expression_return shift_expression() throws RecognitionException {
        CSampleParser.shift_expression_return retval = new CSampleParser.shift_expression_return();
        retval.start = input.LT(1);

        int shift_expression_StartIndex = input.index();

        CommonTree root_0 = null;

        Token set231=null;
        CSampleParser.additive_expression_return additive_expression230 =null;

        CSampleParser.additive_expression_return additive_expression232 =null;


        CommonTree set231_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 59) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:602:2: ( additive_expression ( ( '<<' | '>>' ) additive_expression )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:602:4: additive_expression ( ( '<<' | '>>' ) additive_expression )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_additive_expression_in_shift_expression1895);
            additive_expression230=additive_expression();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, additive_expression230.getTree());

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:602:24: ( ( '<<' | '>>' ) additive_expression )*
            loop61:
            do {
                int alt61=2;
                int LA61_0 = input.LA(1);

                if ( (LA61_0==49||LA61_0==56) ) {
                    alt61=1;
                }


                switch (alt61) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:602:25: ( '<<' | '>>' ) additive_expression
            	    {
            	    set231=(Token)input.LT(1);

            	    if ( input.LA(1)==49||input.LA(1)==56 ) {
            	        input.consume();
            	        if ( state.backtracking==0 ) adaptor.addChild(root_0, 
            	        (CommonTree)adaptor.create(set231)
            	        );
            	        state.errorRecovery=false;
            	        state.failed=false;
            	    }
            	    else {
            	        if (state.backtracking>0) {state.failed=true; return retval;}
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        throw mse;
            	    }


            	    pushFollow(FOLLOW_additive_expression_in_shift_expression1904);
            	    additive_expression232=additive_expression();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, additive_expression232.getTree());

            	    }
            	    break;

            	default :
            	    break loop61;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 59, shift_expression_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "shift_expression"


    public static class statement_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "statement"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:608:1: statement : ( labeled_statement | compound_statement | expression_statement | selection_statement | iteration_statement | jump_statement );
    public final CSampleParser.statement_return statement() throws RecognitionException {
        CSampleParser.statement_return retval = new CSampleParser.statement_return();
        retval.start = input.LT(1);

        int statement_StartIndex = input.index();

        CommonTree root_0 = null;

        CSampleParser.labeled_statement_return labeled_statement233 =null;

        CSampleParser.compound_statement_return compound_statement234 =null;

        CSampleParser.expression_statement_return expression_statement235 =null;

        CSampleParser.selection_statement_return selection_statement236 =null;

        CSampleParser.iteration_statement_return iteration_statement237 =null;

        CSampleParser.jump_statement_return jump_statement238 =null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 60) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:609:2: ( labeled_statement | compound_statement | expression_statement | selection_statement | iteration_statement | jump_statement )
            int alt62=6;
            switch ( input.LA(1) ) {
            case IDENTIFIER:
                {
                int LA62_1 = input.LA(2);

                if ( (LA62_1==46) ) {
                    alt62=1;
                }
                else if ( ((LA62_1 >= 24 && LA62_1 <= 30)||(LA62_1 >= 32 && LA62_1 <= 42)||(LA62_1 >= 44 && LA62_1 <= 45)||(LA62_1 >= 47 && LA62_1 <= 59)||(LA62_1 >= 61 && LA62_1 <= 62)||(LA62_1 >= 96 && LA62_1 <= 98)) ) {
                    alt62=3;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 62, 1, input);

                    throw nvae;

                }
                }
                break;
            case 65:
            case 69:
                {
                alt62=1;
                }
                break;
            case 95:
                {
                alt62=2;
                }
                break;
            case CHARACTER_LITERAL:
            case DECIMAL_LITERAL:
            case FLOATING_POINT_LITERAL:
            case HEX_LITERAL:
            case OCTAL_LITERAL:
            case STRING_LITERAL:
            case 23:
            case 28:
            case 30:
            case 32:
            case 34:
            case 35:
            case 38:
            case 39:
            case 47:
            case 85:
            case 100:
                {
                alt62=3;
                }
                break;
            case 78:
            case 88:
                {
                alt62=4;
                }
                break;
            case 70:
            case 76:
            case 94:
                {
                alt62=5;
                }
                break;
            case 64:
            case 68:
            case 77:
            case 82:
                {
                alt62=6;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 62, 0, input);

                throw nvae;

            }

            switch (alt62) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:609:4: labeled_statement
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_labeled_statement_in_statement1920);
                    labeled_statement233=labeled_statement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, labeled_statement233.getTree());

                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:610:4: compound_statement
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_compound_statement_in_statement1925);
                    compound_statement234=compound_statement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, compound_statement234.getTree());

                    }
                    break;
                case 3 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:611:4: expression_statement
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_expression_statement_in_statement1930);
                    expression_statement235=expression_statement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, expression_statement235.getTree());

                    }
                    break;
                case 4 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:612:4: selection_statement
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_selection_statement_in_statement1935);
                    selection_statement236=selection_statement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, selection_statement236.getTree());

                    }
                    break;
                case 5 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:613:4: iteration_statement
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_iteration_statement_in_statement1940);
                    iteration_statement237=iteration_statement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, iteration_statement237.getTree());

                    }
                    break;
                case 6 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:614:4: jump_statement
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_jump_statement_in_statement1945);
                    jump_statement238=jump_statement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, jump_statement238.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 60, statement_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "statement"


    public static class labeled_statement_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "labeled_statement"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:617:1: labeled_statement : ( IDENTIFIER ':' statement | 'case' constant_expression ':' statement | 'default' ':' statement );
    public final CSampleParser.labeled_statement_return labeled_statement() throws RecognitionException {
        CSampleParser.labeled_statement_return retval = new CSampleParser.labeled_statement_return();
        retval.start = input.LT(1);

        int labeled_statement_StartIndex = input.index();

        CommonTree root_0 = null;

        Token IDENTIFIER239=null;
        Token char_literal240=null;
        Token string_literal242=null;
        Token char_literal244=null;
        Token string_literal246=null;
        Token char_literal247=null;
        CSampleParser.statement_return statement241 =null;

        CSampleParser.constant_expression_return constant_expression243 =null;

        CSampleParser.statement_return statement245 =null;

        CSampleParser.statement_return statement248 =null;


        CommonTree IDENTIFIER239_tree=null;
        CommonTree char_literal240_tree=null;
        CommonTree string_literal242_tree=null;
        CommonTree char_literal244_tree=null;
        CommonTree string_literal246_tree=null;
        CommonTree char_literal247_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 61) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:618:2: ( IDENTIFIER ':' statement | 'case' constant_expression ':' statement | 'default' ':' statement )
            int alt63=3;
            switch ( input.LA(1) ) {
            case IDENTIFIER:
                {
                alt63=1;
                }
                break;
            case 65:
                {
                alt63=2;
                }
                break;
            case 69:
                {
                alt63=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 63, 0, input);

                throw nvae;

            }

            switch (alt63) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:618:4: IDENTIFIER ':' statement
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    IDENTIFIER239=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_labeled_statement1956); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    IDENTIFIER239_tree = 
                    (CommonTree)adaptor.create(IDENTIFIER239)
                    ;
                    adaptor.addChild(root_0, IDENTIFIER239_tree);
                    }

                    char_literal240=(Token)match(input,46,FOLLOW_46_in_labeled_statement1958); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal240_tree = 
                    (CommonTree)adaptor.create(char_literal240)
                    ;
                    adaptor.addChild(root_0, char_literal240_tree);
                    }

                    pushFollow(FOLLOW_statement_in_labeled_statement1960);
                    statement241=statement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, statement241.getTree());

                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:619:4: 'case' constant_expression ':' statement
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal242=(Token)match(input,65,FOLLOW_65_in_labeled_statement1965); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal242_tree = 
                    (CommonTree)adaptor.create(string_literal242)
                    ;
                    adaptor.addChild(root_0, string_literal242_tree);
                    }

                    pushFollow(FOLLOW_constant_expression_in_labeled_statement1967);
                    constant_expression243=constant_expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, constant_expression243.getTree());

                    char_literal244=(Token)match(input,46,FOLLOW_46_in_labeled_statement1969); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal244_tree = 
                    (CommonTree)adaptor.create(char_literal244)
                    ;
                    adaptor.addChild(root_0, char_literal244_tree);
                    }

                    pushFollow(FOLLOW_statement_in_labeled_statement1971);
                    statement245=statement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, statement245.getTree());

                    }
                    break;
                case 3 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:620:4: 'default' ':' statement
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal246=(Token)match(input,69,FOLLOW_69_in_labeled_statement1976); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal246_tree = 
                    (CommonTree)adaptor.create(string_literal246)
                    ;
                    adaptor.addChild(root_0, string_literal246_tree);
                    }

                    char_literal247=(Token)match(input,46,FOLLOW_46_in_labeled_statement1978); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal247_tree = 
                    (CommonTree)adaptor.create(char_literal247)
                    ;
                    adaptor.addChild(root_0, char_literal247_tree);
                    }

                    pushFollow(FOLLOW_statement_in_labeled_statement1980);
                    statement248=statement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, statement248.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 61, labeled_statement_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "labeled_statement"


    public static class compound_statement_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "compound_statement"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:623:1: compound_statement : '{' ( ( declaration )* ) ! ( statement_list )? '}' ;
    public final CSampleParser.compound_statement_return compound_statement() throws RecognitionException {
        Symbols_stack.push(new Symbols_scope());

        CSampleParser.compound_statement_return retval = new CSampleParser.compound_statement_return();
        retval.start = input.LT(1);

        int compound_statement_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal249=null;
        Token char_literal252=null;
        CSampleParser.declaration_return declaration250 =null;

        CSampleParser.statement_list_return statement_list251 =null;


        CommonTree char_literal249_tree=null;
        CommonTree char_literal252_tree=null;


          ((Symbols_scope)Symbols_stack.peek()).types = new HashSet();

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 62) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:628:2: ( '{' ( ( declaration )* ) ! ( statement_list )? '}' )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:628:4: '{' ( ( declaration )* ) ! ( statement_list )? '}'
            {
            root_0 = (CommonTree)adaptor.nil();


            char_literal249=(Token)match(input,95,FOLLOW_95_in_compound_statement2002); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            char_literal249_tree = 
            (CommonTree)adaptor.create(char_literal249)
            ;
            adaptor.addChild(root_0, char_literal249_tree);
            }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:628:8: ( ( declaration )* )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:628:9: ( declaration )*
            {
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:628:9: ( declaration )*
            loop64:
            do {
                int alt64=2;
                alt64 = dfa64.predict(input);
                switch (alt64) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:628:9: declaration
            	    {
            	    pushFollow(FOLLOW_declaration_in_compound_statement2005);
            	    declaration250=declaration();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, declaration250.getTree());

            	    }
            	    break;

            	default :
            	    break loop64;
                }
            } while (true);


            }


            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:628:24: ( statement_list )?
            int alt65=2;
            int LA65_0 = input.LA(1);

            if ( (LA65_0==CHARACTER_LITERAL||LA65_0==DECIMAL_LITERAL||LA65_0==FLOATING_POINT_LITERAL||LA65_0==HEX_LITERAL||LA65_0==IDENTIFIER||LA65_0==OCTAL_LITERAL||LA65_0==STRING_LITERAL||LA65_0==23||LA65_0==28||LA65_0==30||LA65_0==32||(LA65_0 >= 34 && LA65_0 <= 35)||(LA65_0 >= 38 && LA65_0 <= 39)||LA65_0==47||(LA65_0 >= 64 && LA65_0 <= 65)||(LA65_0 >= 68 && LA65_0 <= 70)||(LA65_0 >= 76 && LA65_0 <= 78)||LA65_0==82||LA65_0==85||LA65_0==88||(LA65_0 >= 94 && LA65_0 <= 95)||LA65_0==100) ) {
                alt65=1;
            }
            switch (alt65) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:628:24: statement_list
                    {
                    pushFollow(FOLLOW_statement_list_in_compound_statement2010);
                    statement_list251=statement_list();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, statement_list251.getTree());

                    }
                    break;

            }


            char_literal252=(Token)match(input,99,FOLLOW_99_in_compound_statement2013); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            char_literal252_tree = 
            (CommonTree)adaptor.create(char_literal252)
            ;
            adaptor.addChild(root_0, char_literal252_tree);
            }

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 62, compound_statement_StartIndex); }

            Symbols_stack.pop();

        }
        return retval;
    }
    // $ANTLR end "compound_statement"


    public static class statement_list_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "statement_list"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:631:1: statement_list : ( statement )+ ;
    public final CSampleParser.statement_list_return statement_list() throws RecognitionException {
        CSampleParser.statement_list_return retval = new CSampleParser.statement_list_return();
        retval.start = input.LT(1);

        int statement_list_StartIndex = input.index();

        CommonTree root_0 = null;

        CSampleParser.statement_return statement253 =null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 63) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:632:2: ( ( statement )+ )
            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:632:4: ( statement )+
            {
            root_0 = (CommonTree)adaptor.nil();


            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:632:4: ( statement )+
            int cnt66=0;
            loop66:
            do {
                int alt66=2;
                int LA66_0 = input.LA(1);

                if ( (LA66_0==CHARACTER_LITERAL||LA66_0==DECIMAL_LITERAL||LA66_0==FLOATING_POINT_LITERAL||LA66_0==HEX_LITERAL||LA66_0==IDENTIFIER||LA66_0==OCTAL_LITERAL||LA66_0==STRING_LITERAL||LA66_0==23||LA66_0==28||LA66_0==30||LA66_0==32||(LA66_0 >= 34 && LA66_0 <= 35)||(LA66_0 >= 38 && LA66_0 <= 39)||LA66_0==47||(LA66_0 >= 64 && LA66_0 <= 65)||(LA66_0 >= 68 && LA66_0 <= 70)||(LA66_0 >= 76 && LA66_0 <= 78)||LA66_0==82||LA66_0==85||LA66_0==88||(LA66_0 >= 94 && LA66_0 <= 95)||LA66_0==100) ) {
                    alt66=1;
                }


                switch (alt66) {
            	case 1 :
            	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:632:4: statement
            	    {
            	    pushFollow(FOLLOW_statement_in_statement_list2024);
            	    statement253=statement();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, statement253.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt66 >= 1 ) break loop66;
            	    if (state.backtracking>0) {state.failed=true; return retval;}
                        EarlyExitException eee =
                            new EarlyExitException(66, input);
                        throw eee;
                }
                cnt66++;
            } while (true);


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 63, statement_list_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "statement_list"


    public static class expression_statement_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "expression_statement"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:635:1: expression_statement : ( ';' !| expression ';' !);
    public final CSampleParser.expression_statement_return expression_statement() throws RecognitionException {
        CSampleParser.expression_statement_return retval = new CSampleParser.expression_statement_return();
        retval.start = input.LT(1);

        int expression_statement_StartIndex = input.index();

        CommonTree root_0 = null;

        Token char_literal254=null;
        Token char_literal256=null;
        CSampleParser.expression_return expression255 =null;


        CommonTree char_literal254_tree=null;
        CommonTree char_literal256_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 64) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:636:2: ( ';' !| expression ';' !)
            int alt67=2;
            int LA67_0 = input.LA(1);

            if ( (LA67_0==47) ) {
                alt67=1;
            }
            else if ( (LA67_0==CHARACTER_LITERAL||LA67_0==DECIMAL_LITERAL||LA67_0==FLOATING_POINT_LITERAL||LA67_0==HEX_LITERAL||LA67_0==IDENTIFIER||LA67_0==OCTAL_LITERAL||LA67_0==STRING_LITERAL||LA67_0==23||LA67_0==28||LA67_0==30||LA67_0==32||(LA67_0 >= 34 && LA67_0 <= 35)||(LA67_0 >= 38 && LA67_0 <= 39)||LA67_0==85||LA67_0==100) ) {
                alt67=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 67, 0, input);

                throw nvae;

            }
            switch (alt67) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:636:4: ';' !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    char_literal254=(Token)match(input,47,FOLLOW_47_in_expression_statement2036); if (state.failed) return retval;

                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:637:4: expression ';' !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    pushFollow(FOLLOW_expression_in_expression_statement2042);
                    expression255=expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, expression255.getTree());

                    char_literal256=(Token)match(input,47,FOLLOW_47_in_expression_statement2044); if (state.failed) return retval;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 64, expression_statement_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "expression_statement"


    public static class selection_statement_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "selection_statement"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:640:1: selection_statement : ( 'if' '(' expression ')' statement ( options {k=1; backtrack=false; } : 'else' statement )? | 'switch' '(' expression ')' statement );
    public final CSampleParser.selection_statement_return selection_statement() throws RecognitionException {
        CSampleParser.selection_statement_return retval = new CSampleParser.selection_statement_return();
        retval.start = input.LT(1);

        int selection_statement_StartIndex = input.index();

        CommonTree root_0 = null;

        Token string_literal257=null;
        Token char_literal258=null;
        Token char_literal260=null;
        Token string_literal262=null;
        Token string_literal264=null;
        Token char_literal265=null;
        Token char_literal267=null;
        CSampleParser.expression_return expression259 =null;

        CSampleParser.statement_return statement261 =null;

        CSampleParser.statement_return statement263 =null;

        CSampleParser.expression_return expression266 =null;

        CSampleParser.statement_return statement268 =null;


        CommonTree string_literal257_tree=null;
        CommonTree char_literal258_tree=null;
        CommonTree char_literal260_tree=null;
        CommonTree string_literal262_tree=null;
        CommonTree string_literal264_tree=null;
        CommonTree char_literal265_tree=null;
        CommonTree char_literal267_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 65) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:641:2: ( 'if' '(' expression ')' statement ( options {k=1; backtrack=false; } : 'else' statement )? | 'switch' '(' expression ')' statement )
            int alt69=2;
            int LA69_0 = input.LA(1);

            if ( (LA69_0==78) ) {
                alt69=1;
            }
            else if ( (LA69_0==88) ) {
                alt69=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 69, 0, input);

                throw nvae;

            }
            switch (alt69) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:641:4: 'if' '(' expression ')' statement ( options {k=1; backtrack=false; } : 'else' statement )?
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal257=(Token)match(input,78,FOLLOW_78_in_selection_statement2056); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal257_tree = 
                    (CommonTree)adaptor.create(string_literal257)
                    ;
                    adaptor.addChild(root_0, string_literal257_tree);
                    }

                    char_literal258=(Token)match(input,30,FOLLOW_30_in_selection_statement2058); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal258_tree = 
                    (CommonTree)adaptor.create(char_literal258)
                    ;
                    adaptor.addChild(root_0, char_literal258_tree);
                    }

                    pushFollow(FOLLOW_expression_in_selection_statement2060);
                    expression259=expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, expression259.getTree());

                    char_literal260=(Token)match(input,31,FOLLOW_31_in_selection_statement2062); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal260_tree = 
                    (CommonTree)adaptor.create(char_literal260)
                    ;
                    adaptor.addChild(root_0, char_literal260_tree);
                    }

                    pushFollow(FOLLOW_statement_in_selection_statement2064);
                    statement261=statement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, statement261.getTree());

                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:641:38: ( options {k=1; backtrack=false; } : 'else' statement )?
                    int alt68=2;
                    int LA68_0 = input.LA(1);

                    if ( (LA68_0==72) ) {
                        int LA68_1 = input.LA(2);

                        if ( (true) ) {
                            alt68=1;
                        }
                    }
                    switch (alt68) {
                        case 1 :
                            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:641:71: 'else' statement
                            {
                            string_literal262=(Token)match(input,72,FOLLOW_72_in_selection_statement2079); if (state.failed) return retval;
                            if ( state.backtracking==0 ) {
                            string_literal262_tree = 
                            (CommonTree)adaptor.create(string_literal262)
                            ;
                            adaptor.addChild(root_0, string_literal262_tree);
                            }

                            pushFollow(FOLLOW_statement_in_selection_statement2081);
                            statement263=statement();

                            state._fsp--;
                            if (state.failed) return retval;
                            if ( state.backtracking==0 ) adaptor.addChild(root_0, statement263.getTree());

                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:642:4: 'switch' '(' expression ')' statement
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal264=(Token)match(input,88,FOLLOW_88_in_selection_statement2088); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal264_tree = 
                    (CommonTree)adaptor.create(string_literal264)
                    ;
                    adaptor.addChild(root_0, string_literal264_tree);
                    }

                    char_literal265=(Token)match(input,30,FOLLOW_30_in_selection_statement2090); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal265_tree = 
                    (CommonTree)adaptor.create(char_literal265)
                    ;
                    adaptor.addChild(root_0, char_literal265_tree);
                    }

                    pushFollow(FOLLOW_expression_in_selection_statement2092);
                    expression266=expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, expression266.getTree());

                    char_literal267=(Token)match(input,31,FOLLOW_31_in_selection_statement2094); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal267_tree = 
                    (CommonTree)adaptor.create(char_literal267)
                    ;
                    adaptor.addChild(root_0, char_literal267_tree);
                    }

                    pushFollow(FOLLOW_statement_in_selection_statement2096);
                    statement268=statement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, statement268.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 65, selection_statement_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "selection_statement"


    public static class iteration_statement_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "iteration_statement"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:645:1: iteration_statement : ( 'while' '(' expression ')' statement | 'do' statement 'while' '(' expression ')' ';' !| 'for' '(' expression_statement expression_statement ( expression )? ')' statement );
    public final CSampleParser.iteration_statement_return iteration_statement() throws RecognitionException {
        CSampleParser.iteration_statement_return retval = new CSampleParser.iteration_statement_return();
        retval.start = input.LT(1);

        int iteration_statement_StartIndex = input.index();

        CommonTree root_0 = null;

        Token string_literal269=null;
        Token char_literal270=null;
        Token char_literal272=null;
        Token string_literal274=null;
        Token string_literal276=null;
        Token char_literal277=null;
        Token char_literal279=null;
        Token char_literal280=null;
        Token string_literal281=null;
        Token char_literal282=null;
        Token char_literal286=null;
        CSampleParser.expression_return expression271 =null;

        CSampleParser.statement_return statement273 =null;

        CSampleParser.statement_return statement275 =null;

        CSampleParser.expression_return expression278 =null;

        CSampleParser.expression_statement_return expression_statement283 =null;

        CSampleParser.expression_statement_return expression_statement284 =null;

        CSampleParser.expression_return expression285 =null;

        CSampleParser.statement_return statement287 =null;


        CommonTree string_literal269_tree=null;
        CommonTree char_literal270_tree=null;
        CommonTree char_literal272_tree=null;
        CommonTree string_literal274_tree=null;
        CommonTree string_literal276_tree=null;
        CommonTree char_literal277_tree=null;
        CommonTree char_literal279_tree=null;
        CommonTree char_literal280_tree=null;
        CommonTree string_literal281_tree=null;
        CommonTree char_literal282_tree=null;
        CommonTree char_literal286_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 66) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:646:2: ( 'while' '(' expression ')' statement | 'do' statement 'while' '(' expression ')' ';' !| 'for' '(' expression_statement expression_statement ( expression )? ')' statement )
            int alt71=3;
            switch ( input.LA(1) ) {
            case 94:
                {
                alt71=1;
                }
                break;
            case 70:
                {
                alt71=2;
                }
                break;
            case 76:
                {
                alt71=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 71, 0, input);

                throw nvae;

            }

            switch (alt71) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:646:4: 'while' '(' expression ')' statement
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal269=(Token)match(input,94,FOLLOW_94_in_iteration_statement2107); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal269_tree = 
                    (CommonTree)adaptor.create(string_literal269)
                    ;
                    adaptor.addChild(root_0, string_literal269_tree);
                    }

                    char_literal270=(Token)match(input,30,FOLLOW_30_in_iteration_statement2109); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal270_tree = 
                    (CommonTree)adaptor.create(char_literal270)
                    ;
                    adaptor.addChild(root_0, char_literal270_tree);
                    }

                    pushFollow(FOLLOW_expression_in_iteration_statement2111);
                    expression271=expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, expression271.getTree());

                    char_literal272=(Token)match(input,31,FOLLOW_31_in_iteration_statement2113); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal272_tree = 
                    (CommonTree)adaptor.create(char_literal272)
                    ;
                    adaptor.addChild(root_0, char_literal272_tree);
                    }

                    pushFollow(FOLLOW_statement_in_iteration_statement2115);
                    statement273=statement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, statement273.getTree());

                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:647:4: 'do' statement 'while' '(' expression ')' ';' !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal274=(Token)match(input,70,FOLLOW_70_in_iteration_statement2120); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal274_tree = 
                    (CommonTree)adaptor.create(string_literal274)
                    ;
                    adaptor.addChild(root_0, string_literal274_tree);
                    }

                    pushFollow(FOLLOW_statement_in_iteration_statement2122);
                    statement275=statement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, statement275.getTree());

                    string_literal276=(Token)match(input,94,FOLLOW_94_in_iteration_statement2124); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal276_tree = 
                    (CommonTree)adaptor.create(string_literal276)
                    ;
                    adaptor.addChild(root_0, string_literal276_tree);
                    }

                    char_literal277=(Token)match(input,30,FOLLOW_30_in_iteration_statement2126); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal277_tree = 
                    (CommonTree)adaptor.create(char_literal277)
                    ;
                    adaptor.addChild(root_0, char_literal277_tree);
                    }

                    pushFollow(FOLLOW_expression_in_iteration_statement2128);
                    expression278=expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, expression278.getTree());

                    char_literal279=(Token)match(input,31,FOLLOW_31_in_iteration_statement2130); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal279_tree = 
                    (CommonTree)adaptor.create(char_literal279)
                    ;
                    adaptor.addChild(root_0, char_literal279_tree);
                    }

                    char_literal280=(Token)match(input,47,FOLLOW_47_in_iteration_statement2132); if (state.failed) return retval;

                    }
                    break;
                case 3 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:648:4: 'for' '(' expression_statement expression_statement ( expression )? ')' statement
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal281=(Token)match(input,76,FOLLOW_76_in_iteration_statement2138); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal281_tree = 
                    (CommonTree)adaptor.create(string_literal281)
                    ;
                    adaptor.addChild(root_0, string_literal281_tree);
                    }

                    char_literal282=(Token)match(input,30,FOLLOW_30_in_iteration_statement2140); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal282_tree = 
                    (CommonTree)adaptor.create(char_literal282)
                    ;
                    adaptor.addChild(root_0, char_literal282_tree);
                    }

                    pushFollow(FOLLOW_expression_statement_in_iteration_statement2142);
                    expression_statement283=expression_statement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, expression_statement283.getTree());

                    pushFollow(FOLLOW_expression_statement_in_iteration_statement2144);
                    expression_statement284=expression_statement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, expression_statement284.getTree());

                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:648:56: ( expression )?
                    int alt70=2;
                    int LA70_0 = input.LA(1);

                    if ( (LA70_0==CHARACTER_LITERAL||LA70_0==DECIMAL_LITERAL||LA70_0==FLOATING_POINT_LITERAL||LA70_0==HEX_LITERAL||LA70_0==IDENTIFIER||LA70_0==OCTAL_LITERAL||LA70_0==STRING_LITERAL||LA70_0==23||LA70_0==28||LA70_0==30||LA70_0==32||(LA70_0 >= 34 && LA70_0 <= 35)||(LA70_0 >= 38 && LA70_0 <= 39)||LA70_0==85||LA70_0==100) ) {
                        alt70=1;
                    }
                    switch (alt70) {
                        case 1 :
                            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:648:56: expression
                            {
                            pushFollow(FOLLOW_expression_in_iteration_statement2146);
                            expression285=expression();

                            state._fsp--;
                            if (state.failed) return retval;
                            if ( state.backtracking==0 ) adaptor.addChild(root_0, expression285.getTree());

                            }
                            break;

                    }


                    char_literal286=(Token)match(input,31,FOLLOW_31_in_iteration_statement2149); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal286_tree = 
                    (CommonTree)adaptor.create(char_literal286)
                    ;
                    adaptor.addChild(root_0, char_literal286_tree);
                    }

                    pushFollow(FOLLOW_statement_in_iteration_statement2151);
                    statement287=statement();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, statement287.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 66, iteration_statement_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "iteration_statement"


    public static class jump_statement_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "jump_statement"
    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:651:1: jump_statement : ( 'goto' IDENTIFIER ';' !| 'continue' ';' !| 'break' ';' !| 'return' ';' !| 'return' expression ';' !);
    public final CSampleParser.jump_statement_return jump_statement() throws RecognitionException {
        CSampleParser.jump_statement_return retval = new CSampleParser.jump_statement_return();
        retval.start = input.LT(1);

        int jump_statement_StartIndex = input.index();

        CommonTree root_0 = null;

        Token string_literal288=null;
        Token IDENTIFIER289=null;
        Token char_literal290=null;
        Token string_literal291=null;
        Token char_literal292=null;
        Token string_literal293=null;
        Token char_literal294=null;
        Token string_literal295=null;
        Token char_literal296=null;
        Token string_literal297=null;
        Token char_literal299=null;
        CSampleParser.expression_return expression298 =null;


        CommonTree string_literal288_tree=null;
        CommonTree IDENTIFIER289_tree=null;
        CommonTree char_literal290_tree=null;
        CommonTree string_literal291_tree=null;
        CommonTree char_literal292_tree=null;
        CommonTree string_literal293_tree=null;
        CommonTree char_literal294_tree=null;
        CommonTree string_literal295_tree=null;
        CommonTree char_literal296_tree=null;
        CommonTree string_literal297_tree=null;
        CommonTree char_literal299_tree=null;

        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 67) ) { return retval; }

            // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:652:2: ( 'goto' IDENTIFIER ';' !| 'continue' ';' !| 'break' ';' !| 'return' ';' !| 'return' expression ';' !)
            int alt72=5;
            switch ( input.LA(1) ) {
            case 77:
                {
                alt72=1;
                }
                break;
            case 68:
                {
                alt72=2;
                }
                break;
            case 64:
                {
                alt72=3;
                }
                break;
            case 82:
                {
                int LA72_4 = input.LA(2);

                if ( (LA72_4==47) ) {
                    alt72=4;
                }
                else if ( (LA72_4==CHARACTER_LITERAL||LA72_4==DECIMAL_LITERAL||LA72_4==FLOATING_POINT_LITERAL||LA72_4==HEX_LITERAL||LA72_4==IDENTIFIER||LA72_4==OCTAL_LITERAL||LA72_4==STRING_LITERAL||LA72_4==23||LA72_4==28||LA72_4==30||LA72_4==32||(LA72_4 >= 34 && LA72_4 <= 35)||(LA72_4 >= 38 && LA72_4 <= 39)||LA72_4==85||LA72_4==100) ) {
                    alt72=5;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 72, 4, input);

                    throw nvae;

                }
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 72, 0, input);

                throw nvae;

            }

            switch (alt72) {
                case 1 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:652:4: 'goto' IDENTIFIER ';' !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal288=(Token)match(input,77,FOLLOW_77_in_jump_statement2162); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal288_tree = 
                    (CommonTree)adaptor.create(string_literal288)
                    ;
                    adaptor.addChild(root_0, string_literal288_tree);
                    }

                    IDENTIFIER289=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_jump_statement2164); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    IDENTIFIER289_tree = 
                    (CommonTree)adaptor.create(IDENTIFIER289)
                    ;
                    adaptor.addChild(root_0, IDENTIFIER289_tree);
                    }

                    char_literal290=(Token)match(input,47,FOLLOW_47_in_jump_statement2166); if (state.failed) return retval;

                    }
                    break;
                case 2 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:653:4: 'continue' ';' !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal291=(Token)match(input,68,FOLLOW_68_in_jump_statement2172); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal291_tree = 
                    (CommonTree)adaptor.create(string_literal291)
                    ;
                    adaptor.addChild(root_0, string_literal291_tree);
                    }

                    char_literal292=(Token)match(input,47,FOLLOW_47_in_jump_statement2174); if (state.failed) return retval;

                    }
                    break;
                case 3 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:654:4: 'break' ';' !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal293=(Token)match(input,64,FOLLOW_64_in_jump_statement2180); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal293_tree = 
                    (CommonTree)adaptor.create(string_literal293)
                    ;
                    adaptor.addChild(root_0, string_literal293_tree);
                    }

                    char_literal294=(Token)match(input,47,FOLLOW_47_in_jump_statement2182); if (state.failed) return retval;

                    }
                    break;
                case 4 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:655:4: 'return' ';' !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal295=(Token)match(input,82,FOLLOW_82_in_jump_statement2188); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal295_tree = 
                    (CommonTree)adaptor.create(string_literal295)
                    ;
                    adaptor.addChild(root_0, string_literal295_tree);
                    }

                    char_literal296=(Token)match(input,47,FOLLOW_47_in_jump_statement2190); if (state.failed) return retval;

                    }
                    break;
                case 5 :
                    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:656:4: 'return' expression ';' !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    string_literal297=(Token)match(input,82,FOLLOW_82_in_jump_statement2196); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal297_tree = 
                    (CommonTree)adaptor.create(string_literal297)
                    ;
                    adaptor.addChild(root_0, string_literal297_tree);
                    }

                    pushFollow(FOLLOW_expression_in_jump_statement2198);
                    expression298=expression();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, expression298.getTree());

                    char_literal299=(Token)match(input,47,FOLLOW_47_in_jump_statement2200); if (state.failed) return retval;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }

           catch (RecognitionException e) {
        //    System.out.println("KACZ");
        //    System.out.println(e.getMessage());
            throw e;
           }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 67, jump_statement_StartIndex); }

        }
        return retval;
    }
    // $ANTLR end "jump_statement"

    // $ANTLR start synpred2_CSample
    public final void synpred2_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:219:7: ( declaration_specifiers )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:219:7: declaration_specifiers
        {
        pushFollow(FOLLOW_declaration_specifiers_in_synpred2_CSample172);
        declaration_specifiers();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred2_CSample

    // $ANTLR start synpred4_CSample
    public final void synpred4_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:219:4: ( ( ( declaration_specifiers )? ) ! ( declarator ( declaration )* ) ! '{' !)
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:219:6: ( ( declaration_specifiers )? ) ! ( declarator ( declaration )* ) ! '{' !
        {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:219:6: ( ( declaration_specifiers )? )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:219:7: ( declaration_specifiers )?
        {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:219:7: ( declaration_specifiers )?
        int alt73=2;
        int LA73_0 = input.LA(1);

        if ( (LA73_0==63||(LA73_0 >= 66 && LA73_0 <= 67)||LA73_0==71||(LA73_0 >= 73 && LA73_0 <= 75)||(LA73_0 >= 79 && LA73_0 <= 81)||(LA73_0 >= 83 && LA73_0 <= 84)||(LA73_0 >= 86 && LA73_0 <= 87)||(LA73_0 >= 90 && LA73_0 <= 93)) ) {
            alt73=1;
        }
        else if ( (LA73_0==IDENTIFIER) ) {
            switch ( input.LA(2) ) {
                case 32:
                    {
                    alt73=1;
                    }
                    break;
                case IDENTIFIER:
                    {
                    int LA73_18 = input.LA(3);

                    if ( (((synpred2_CSample()&&synpred2_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                        alt73=1;
                    }
                    }
                    break;
                case 30:
                    {
                    int LA73_19 = input.LA(3);

                    if ( (((synpred2_CSample()&&synpred2_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                        alt73=1;
                    }
                    }
                    break;
                case 63:
                case 74:
                case 81:
                case 86:
                    {
                    int LA73_20 = input.LA(3);

                    if ( (((synpred2_CSample()&&synpred2_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                        alt73=1;
                    }
                    }
                    break;
                case 92:
                    {
                    int LA73_21 = input.LA(3);

                    if ( (((synpred2_CSample()&&synpred2_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                        alt73=1;
                    }
                    }
                    break;
                case 66:
                    {
                    int LA73_22 = input.LA(3);

                    if ( (((synpred2_CSample()&&synpred2_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                        alt73=1;
                    }
                    }
                    break;
                case 83:
                    {
                    int LA73_23 = input.LA(3);

                    if ( (((synpred2_CSample()&&synpred2_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                        alt73=1;
                    }
                    }
                    break;
                case 79:
                    {
                    int LA73_24 = input.LA(3);

                    if ( (((synpred2_CSample()&&synpred2_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                        alt73=1;
                    }
                    }
                    break;
                case 80:
                    {
                    int LA73_25 = input.LA(3);

                    if ( (((synpred2_CSample()&&synpred2_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                        alt73=1;
                    }
                    }
                    break;
                case 75:
                    {
                    int LA73_26 = input.LA(3);

                    if ( (((synpred2_CSample()&&synpred2_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                        alt73=1;
                    }
                    }
                    break;
                case 71:
                    {
                    int LA73_27 = input.LA(3);

                    if ( (((synpred2_CSample()&&synpred2_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                        alt73=1;
                    }
                    }
                    break;
                case 84:
                    {
                    int LA73_28 = input.LA(3);

                    if ( (((synpred2_CSample()&&synpred2_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                        alt73=1;
                    }
                    }
                    break;
                case 91:
                    {
                    int LA73_29 = input.LA(3);

                    if ( (((synpred2_CSample()&&synpred2_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                        alt73=1;
                    }
                    }
                    break;
                case 87:
                case 90:
                    {
                    int LA73_30 = input.LA(3);

                    if ( (((synpred2_CSample()&&synpred2_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                        alt73=1;
                    }
                    }
                    break;
                case 73:
                    {
                    int LA73_31 = input.LA(3);

                    if ( (((synpred2_CSample()&&synpred2_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                        alt73=1;
                    }
                    }
                    break;
                case 67:
                case 93:
                    {
                    int LA73_32 = input.LA(3);

                    if ( (((synpred2_CSample()&&synpred2_CSample())&&(isTypeName(input.LT(1).getText())))) ) {
                        alt73=1;
                    }
                    }
                    break;
            }

        }
        switch (alt73) {
            case 1 :
                // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:219:7: declaration_specifiers
                {
                pushFollow(FOLLOW_declaration_specifiers_in_synpred4_CSample172);
                declaration_specifiers();

                state._fsp--;
                if (state.failed) return ;

                }
                break;

        }


        }


        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:219:33: ( declarator ( declaration )* )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:219:34: declarator ( declaration )*
        {
        pushFollow(FOLLOW_declarator_in_synpred4_CSample178);
        declarator();

        state._fsp--;
        if (state.failed) return ;

        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:219:45: ( declaration )*
        loop74:
        do {
            int alt74=2;
            int LA74_0 = input.LA(1);

            if ( (LA74_0==IDENTIFIER||LA74_0==63||(LA74_0 >= 66 && LA74_0 <= 67)||LA74_0==71||(LA74_0 >= 73 && LA74_0 <= 75)||(LA74_0 >= 79 && LA74_0 <= 81)||(LA74_0 >= 83 && LA74_0 <= 84)||(LA74_0 >= 86 && LA74_0 <= 87)||(LA74_0 >= 89 && LA74_0 <= 93)) ) {
                alt74=1;
            }


            switch (alt74) {
        	case 1 :
        	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:219:45: declaration
        	    {
        	    pushFollow(FOLLOW_declaration_in_synpred4_CSample180);
        	    declaration();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    break loop74;
            }
        } while (true);


        }


        match(input,95,FOLLOW_95_in_synpred4_CSample185); if (state.failed) return ;

        }

    }
    // $ANTLR end synpred4_CSample

    // $ANTLR start synpred5_CSample
    public final void synpred5_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:237:2: ( declaration_specifiers )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:237:2: declaration_specifiers
        {
        pushFollow(FOLLOW_declaration_specifiers_in_synpred5_CSample230);
        declaration_specifiers();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred5_CSample

    // $ANTLR start synpred8_CSample
    public final void synpred8_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:254:14: ( declaration_specifiers )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:254:14: declaration_specifiers
        {
        pushFollow(FOLLOW_declaration_specifiers_in_synpred8_CSample282);
        declaration_specifiers();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred8_CSample

    // $ANTLR start synpred12_CSample
    public final void synpred12_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:269:6: ( type_specifier )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:269:6: type_specifier
        {
        pushFollow(FOLLOW_type_specifier_in_synpred12_CSample343);
        type_specifier();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred12_CSample

    // $ANTLR start synpred35_CSample
    public final void synpred35_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:363:23: ( type_specifier )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:363:23: type_specifier
        {
        pushFollow(FOLLOW_type_specifier_in_synpred35_CSample674);
        type_specifier();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred35_CSample

    // $ANTLR start synpred45_CSample
    public final void synpred45_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:396:4: ( ( pointer )? direct_declarator )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:396:4: ( pointer )? direct_declarator
        {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:396:4: ( pointer )?
        int alt79=2;
        int LA79_0 = input.LA(1);

        if ( (LA79_0==32) ) {
            alt79=1;
        }
        switch (alt79) {
            case 1 :
                // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:396:4: pointer
                {
                pushFollow(FOLLOW_pointer_in_synpred45_CSample827);
                pointer();

                state._fsp--;
                if (state.failed) return ;

                }
                break;

        }


        pushFollow(FOLLOW_direct_declarator_in_synpred45_CSample830);
        direct_declarator();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred45_CSample

    // $ANTLR start synpred47_CSample
    public final void synpred47_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:410:9: ( declarator_suffix )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:410:9: declarator_suffix
        {
        pushFollow(FOLLOW_declarator_suffix_in_synpred47_CSample879);
        declarator_suffix();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred47_CSample

    // $ANTLR start synpred50_CSample
    public final void synpred50_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:416:9: ( '(' parameter_type_list ')' )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:416:9: '(' parameter_type_list ')'
        {
        match(input,30,FOLLOW_30_in_synpred50_CSample919); if (state.failed) return ;

        pushFollow(FOLLOW_parameter_type_list_in_synpred50_CSample921);
        parameter_type_list();

        state._fsp--;
        if (state.failed) return ;

        match(input,31,FOLLOW_31_in_synpred50_CSample923); if (state.failed) return ;

        }

    }
    // $ANTLR end synpred50_CSample

    // $ANTLR start synpred51_CSample
    public final void synpred51_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:417:9: ( '(' identifier_list ')' )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:417:9: '(' identifier_list ')'
        {
        match(input,30,FOLLOW_30_in_synpred51_CSample933); if (state.failed) return ;

        pushFollow(FOLLOW_identifier_list_in_synpred51_CSample935);
        identifier_list();

        state._fsp--;
        if (state.failed) return ;

        match(input,31,FOLLOW_31_in_synpred51_CSample937); if (state.failed) return ;

        }

    }
    // $ANTLR end synpred51_CSample

    // $ANTLR start synpred52_CSample
    public final void synpred52_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:422:8: ( type_qualifier )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:422:8: type_qualifier
        {
        pushFollow(FOLLOW_type_qualifier_in_synpred52_CSample962);
        type_qualifier();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred52_CSample

    // $ANTLR start synpred53_CSample
    public final void synpred53_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:422:24: ( pointer )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:422:24: pointer
        {
        pushFollow(FOLLOW_pointer_in_synpred53_CSample965);
        pointer();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred53_CSample

    // $ANTLR start synpred54_CSample
    public final void synpred54_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:422:4: ( '*' ( type_qualifier )+ ( pointer )? )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:422:4: '*' ( type_qualifier )+ ( pointer )?
        {
        match(input,32,FOLLOW_32_in_synpred54_CSample960); if (state.failed) return ;

        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:422:8: ( type_qualifier )+
        int cnt80=0;
        loop80:
        do {
            int alt80=2;
            int LA80_0 = input.LA(1);

            if ( (LA80_0==67||LA80_0==93) ) {
                alt80=1;
            }


            switch (alt80) {
        	case 1 :
        	    // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:422:8: type_qualifier
        	    {
        	    pushFollow(FOLLOW_type_qualifier_in_synpred54_CSample962);
        	    type_qualifier();

        	    state._fsp--;
        	    if (state.failed) return ;

        	    }
        	    break;

        	default :
        	    if ( cnt80 >= 1 ) break loop80;
        	    if (state.backtracking>0) {state.failed=true; return ;}
                    EarlyExitException eee =
                        new EarlyExitException(80, input);
                    throw eee;
            }
            cnt80++;
        } while (true);


        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:422:24: ( pointer )?
        int alt81=2;
        int LA81_0 = input.LA(1);

        if ( (LA81_0==32) ) {
            alt81=1;
        }
        switch (alt81) {
            case 1 :
                // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:422:24: pointer
                {
                pushFollow(FOLLOW_pointer_in_synpred54_CSample965);
                pointer();

                state._fsp--;
                if (state.failed) return ;

                }
                break;

        }


        }

    }
    // $ANTLR end synpred54_CSample

    // $ANTLR start synpred55_CSample
    public final void synpred55_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:423:4: ( '*' pointer )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:423:4: '*' pointer
        {
        match(input,32,FOLLOW_32_in_synpred55_CSample971); if (state.failed) return ;

        pushFollow(FOLLOW_pointer_in_synpred55_CSample973);
        pointer();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred55_CSample

    // $ANTLR start synpred58_CSample
    public final void synpred58_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:436:28: ( declarator )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:436:28: declarator
        {
        pushFollow(FOLLOW_declarator_in_synpred58_CSample1028);
        declarator();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred58_CSample

    // $ANTLR start synpred59_CSample
    public final void synpred59_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:436:39: ( abstract_declarator )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:436:39: abstract_declarator
        {
        pushFollow(FOLLOW_abstract_declarator_in_synpred59_CSample1030);
        abstract_declarator();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred59_CSample

    // $ANTLR start synpred62_CSample
    public final void synpred62_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:448:12: ( direct_abstract_declarator )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:448:12: direct_abstract_declarator
        {
        pushFollow(FOLLOW_direct_abstract_declarator_in_synpred62_CSample1077);
        direct_abstract_declarator();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred62_CSample

    // $ANTLR start synpred65_CSample
    public final void synpred65_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:453:65: ( abstract_declarator_suffix )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:453:65: abstract_declarator_suffix
        {
        pushFollow(FOLLOW_abstract_declarator_suffix_in_synpred65_CSample1108);
        abstract_declarator_suffix();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred65_CSample

    // $ANTLR start synpred78_CSample
    public final void synpred78_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:487:4: ( '(' type_name ')' cast_expression )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:487:4: '(' type_name ')' cast_expression
        {
        match(input,30,FOLLOW_30_in_synpred78_CSample1280); if (state.failed) return ;

        pushFollow(FOLLOW_type_name_in_synpred78_CSample1282);
        type_name();

        state._fsp--;
        if (state.failed) return ;

        match(input,31,FOLLOW_31_in_synpred78_CSample1284); if (state.failed) return ;

        pushFollow(FOLLOW_cast_expression_in_synpred78_CSample1286);
        cast_expression();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred78_CSample

    // $ANTLR start synpred83_CSample
    public final void synpred83_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:496:4: ( 'sizeof' unary_expression )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:496:4: 'sizeof' unary_expression
        {
        match(input,85,FOLLOW_85_in_synpred83_CSample1328); if (state.failed) return ;

        pushFollow(FOLLOW_unary_expression_in_synpred83_CSample1330);
        unary_expression();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred83_CSample

    // $ANTLR start synpred104_CSample
    public final void synpred104_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:548:4: ( lvalue assignment_operator assignment_expression )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:548:4: lvalue assignment_operator assignment_expression
        {
        pushFollow(FOLLOW_lvalue_in_synpred104_CSample1643);
        lvalue();

        state._fsp--;
        if (state.failed) return ;

        pushFollow(FOLLOW_assignment_operator_in_synpred104_CSample1645);
        assignment_operator();

        state._fsp--;
        if (state.failed) return ;

        pushFollow(FOLLOW_assignment_expression_in_synpred104_CSample1647);
        assignment_expression();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred104_CSample

    // $ANTLR start synpred136_CSample
    public final void synpred136_CSample_fragment() throws RecognitionException {
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:628:9: ( declaration )
        // D:\\Programowanie\\eclipse\\java-win7-eclipse37\\TTEditor\\src\\pl\\edu\\agh\\iet\\dzioklos\\tteditor\\antlr\\grammar\\CSample.g:628:9: declaration
        {
        pushFollow(FOLLOW_declaration_in_synpred136_CSample2005);
        declaration();

        state._fsp--;
        if (state.failed) return ;

        }

    }
    // $ANTLR end synpred136_CSample

    // Delegated rules

    public final boolean synpred51_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred51_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred2_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred2_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred58_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred58_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred53_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred53_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred45_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred45_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred12_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred12_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred62_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred62_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred136_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred136_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred59_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred59_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred65_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred65_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred78_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred78_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred55_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred55_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred54_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred54_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred50_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred50_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred104_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred104_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred35_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred35_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred83_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred83_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred5_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred5_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred4_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred4_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred8_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred8_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred52_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred52_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred47_CSample() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred47_CSample_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA2 dfa2 = new DFA2(this);
    protected DFA52 dfa52 = new DFA52(this);
    protected DFA64 dfa64 = new DFA64(this);
    static final String DFA2_eotS =
        "\u00e5\uffff";
    static final String DFA2_eofS =
        "\u00e5\uffff";
    static final String DFA2_minS =
        "\17\15\3\uffff\20\0\1\uffff\20\0\1\uffff\20\0\1\uffff\20\0\1\uffff"+
        "\20\0\1\uffff\20\0\1\uffff\20\0\1\uffff\20\0\1\uffff\20\0\1\uffff"+
        "\20\0\1\uffff\24\0\4\uffff\20\0\1\uffff";
    static final String DFA2_maxS =
        "\13\135\3\137\1\135\3\uffff\20\0\1\uffff\20\0\1\uffff\20\0\1\uffff"+
        "\20\0\1\uffff\20\0\1\uffff\20\0\1\uffff\20\0\1\uffff\20\0\1\uffff"+
        "\20\0\1\uffff\20\0\1\uffff\24\0\4\uffff\20\0\1\uffff";
    static final String DFA2_acceptS =
        "\17\uffff\2\1\1\2\u00be\uffff\3\1\22\uffff";
    static final String DFA2_specialS =
        "\1\0\14\uffff\1\1\4\uffff\1\2\1\3\1\4\1\5\1\6\1\7\1\10\1\11\1\12"+
        "\1\13\1\14\1\15\1\16\1\17\1\20\1\21\1\uffff\1\22\1\23\1\24\1\25"+
        "\1\26\1\27\1\30\1\31\1\32\1\33\1\34\1\35\1\36\1\37\1\40\1\41\1\uffff"+
        "\1\42\1\43\1\44\1\45\1\46\1\47\1\50\1\51\1\52\1\53\1\54\1\55\1\56"+
        "\1\57\1\60\1\61\1\uffff\1\62\1\63\1\64\1\65\1\66\1\67\1\70\1\71"+
        "\1\72\1\73\1\74\1\75\1\76\1\77\1\100\1\101\1\uffff\1\102\1\103\1"+
        "\104\1\105\1\106\1\107\1\110\1\111\1\112\1\113\1\114\1\115\1\116"+
        "\1\117\1\120\1\121\1\uffff\1\122\1\123\1\124\1\125\1\126\1\127\1"+
        "\130\1\131\1\132\1\133\1\134\1\135\1\136\1\137\1\140\1\141\1\uffff"+
        "\1\142\1\143\1\144\1\145\1\146\1\147\1\150\1\151\1\152\1\153\1\154"+
        "\1\155\1\156\1\157\1\160\1\161\1\uffff\1\162\1\163\1\164\1\165\1"+
        "\166\1\167\1\170\1\171\1\172\1\173\1\174\1\175\1\176\1\177\1\u0080"+
        "\1\u0081\1\uffff\1\u0082\1\u0083\1\u0084\1\u0085\1\u0086\1\u0087"+
        "\1\u0088\1\u0089\1\u008a\1\u008b\1\u008c\1\u008d\1\u008e\1\u008f"+
        "\1\u0090\1\u0091\1\uffff\1\u0092\1\u0093\1\u0094\1\u0095\1\u0096"+
        "\1\u0097\1\u0098\1\u0099\1\u009a\1\u009b\1\u009c\1\u009d\1\u009e"+
        "\1\u009f\1\u00a0\1\u00a1\1\uffff\1\u00a2\1\u00a3\1\u00a4\1\u00a5"+
        "\1\u00a6\1\u00a7\1\u00a8\1\u00a9\1\u00aa\1\u00ab\1\u00ac\1\u00ad"+
        "\1\u00ae\1\u00af\1\u00b0\1\u00b1\1\u00b2\1\u00b3\1\u00b4\1\u00b5"+
        "\4\uffff\1\u00b6\1\u00b7\1\u00b8\1\u00b9\1\u00ba\1\u00bb\1\u00bc"+
        "\1\u00bd\1\u00be\1\u00bf\1\u00c0\1\u00c1\1\u00c2\1\u00c3\1\u00c4"+
        "\1\u00c5\1\uffff}>";
    static final String[] DFA2_transitionS = {
            "\1\15\20\uffff\1\20\1\uffff\1\17\36\uffff\1\1\2\uffff\1\3\1"+
            "\16\3\uffff\1\10\1\uffff\1\14\1\1\1\7\3\uffff\1\5\1\6\1\1\1"+
            "\uffff\1\4\1\11\1\uffff\1\1\1\13\1\uffff\1\21\1\13\1\12\1\2"+
            "\1\16",
            "\1\23\20\uffff\1\24\1\uffff\1\22\16\uffff\1\21\17\uffff\1\25"+
            "\2\uffff\1\27\1\41\3\uffff\1\34\1\uffff\1\40\1\25\1\33\3\uffff"+
            "\1\31\1\32\1\25\1\uffff\1\30\1\35\1\uffff\1\25\1\37\2\uffff"+
            "\1\37\1\36\1\26\1\41",
            "\1\44\20\uffff\1\45\1\uffff\1\43\16\uffff\1\21\17\uffff\1\46"+
            "\2\uffff\1\50\1\62\3\uffff\1\55\1\uffff\1\61\1\46\1\54\3\uffff"+
            "\1\52\1\53\1\46\1\uffff\1\51\1\56\1\uffff\1\46\1\60\2\uffff"+
            "\1\60\1\57\1\47\1\62",
            "\1\65\20\uffff\1\66\1\uffff\1\64\16\uffff\1\21\17\uffff\1\67"+
            "\2\uffff\1\71\1\103\3\uffff\1\76\1\uffff\1\102\1\67\1\75\3\uffff"+
            "\1\73\1\74\1\67\1\uffff\1\72\1\77\1\uffff\1\67\1\101\2\uffff"+
            "\1\101\1\100\1\70\1\103",
            "\1\106\20\uffff\1\107\1\uffff\1\105\16\uffff\1\21\17\uffff"+
            "\1\110\2\uffff\1\112\1\124\3\uffff\1\117\1\uffff\1\123\1\110"+
            "\1\116\3\uffff\1\114\1\115\1\110\1\uffff\1\113\1\120\1\uffff"+
            "\1\110\1\122\2\uffff\1\122\1\121\1\111\1\124",
            "\1\127\20\uffff\1\130\1\uffff\1\126\16\uffff\1\21\17\uffff"+
            "\1\131\2\uffff\1\133\1\145\3\uffff\1\140\1\uffff\1\144\1\131"+
            "\1\137\3\uffff\1\135\1\136\1\131\1\uffff\1\134\1\141\1\uffff"+
            "\1\131\1\143\2\uffff\1\143\1\142\1\132\1\145",
            "\1\150\20\uffff\1\151\1\uffff\1\147\16\uffff\1\21\17\uffff"+
            "\1\152\2\uffff\1\154\1\166\3\uffff\1\161\1\uffff\1\165\1\152"+
            "\1\160\3\uffff\1\156\1\157\1\152\1\uffff\1\155\1\162\1\uffff"+
            "\1\152\1\164\2\uffff\1\164\1\163\1\153\1\166",
            "\1\171\20\uffff\1\172\1\uffff\1\170\16\uffff\1\21\17\uffff"+
            "\1\173\2\uffff\1\175\1\u0087\3\uffff\1\u0082\1\uffff\1\u0086"+
            "\1\173\1\u0081\3\uffff\1\177\1\u0080\1\173\1\uffff\1\176\1\u0083"+
            "\1\uffff\1\173\1\u0085\2\uffff\1\u0085\1\u0084\1\174\1\u0087",
            "\1\u008a\20\uffff\1\u008b\1\uffff\1\u0089\16\uffff\1\21\17"+
            "\uffff\1\u008c\2\uffff\1\u008e\1\u0098\3\uffff\1\u0093\1\uffff"+
            "\1\u0097\1\u008c\1\u0092\3\uffff\1\u0090\1\u0091\1\u008c\1\uffff"+
            "\1\u008f\1\u0094\1\uffff\1\u008c\1\u0096\2\uffff\1\u0096\1\u0095"+
            "\1\u008d\1\u0098",
            "\1\u009b\20\uffff\1\u009c\1\uffff\1\u009a\16\uffff\1\21\17"+
            "\uffff\1\u009d\2\uffff\1\u009f\1\u00a9\3\uffff\1\u00a4\1\uffff"+
            "\1\u00a8\1\u009d\1\u00a3\3\uffff\1\u00a1\1\u00a2\1\u009d\1\uffff"+
            "\1\u00a0\1\u00a5\1\uffff\1\u009d\1\u00a7\2\uffff\1\u00a7\1\u00a6"+
            "\1\u009e\1\u00a9",
            "\1\u00ac\20\uffff\1\u00ad\1\uffff\1\u00ab\16\uffff\1\21\17"+
            "\uffff\1\u00ae\2\uffff\1\u00b0\1\u00ba\3\uffff\1\u00b5\1\uffff"+
            "\1\u00b9\1\u00ae\1\u00b4\3\uffff\1\u00b2\1\u00b3\1\u00ae\1\uffff"+
            "\1\u00b1\1\u00b6\1\uffff\1\u00ae\1\u00b8\2\uffff\1\u00b8\1\u00b7"+
            "\1\u00af\1\u00ba",
            "\1\u00bc\121\uffff\1\u00bd",
            "\1\u00bf\121\uffff\1\u00be",
            "\1\u00c1\20\uffff\1\u00c2\1\uffff\1\u00c0\16\uffff\1\21\13"+
            "\uffff\1\u00d0\3\uffff\1\u00c3\2\uffff\1\u00c5\1\u00cf\3\uffff"+
            "\1\u00ca\1\uffff\1\u00ce\1\u00c3\1\u00c9\3\uffff\1\u00c7\1\u00c8"+
            "\1\u00c3\1\uffff\1\u00c6\1\u00cb\1\uffff\1\u00c3\1\u00cd\1\uffff"+
            "\1\u00d1\1\u00cd\1\u00cc\1\u00c4\1\u00cf\1\uffff\1\u00d2",
            "\1\u00d5\20\uffff\1\u00d6\1\uffff\1\u00d4\16\uffff\1\21\17"+
            "\uffff\1\u00d7\2\uffff\1\u00d9\1\u00e3\3\uffff\1\u00de\1\uffff"+
            "\1\u00e2\1\u00d7\1\u00dd\3\uffff\1\u00db\1\u00dc\1\u00d7\1\uffff"+
            "\1\u00da\1\u00df\1\uffff\1\u00d7\1\u00e1\2\uffff\1\u00e1\1\u00e0"+
            "\1\u00d8\1\u00e3",
            "",
            "",
            "",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            ""
    };

    static final short[] DFA2_eot = DFA.unpackEncodedString(DFA2_eotS);
    static final short[] DFA2_eof = DFA.unpackEncodedString(DFA2_eofS);
    static final char[] DFA2_min = DFA.unpackEncodedStringToUnsignedChars(DFA2_minS);
    static final char[] DFA2_max = DFA.unpackEncodedStringToUnsignedChars(DFA2_maxS);
    static final short[] DFA2_accept = DFA.unpackEncodedString(DFA2_acceptS);
    static final short[] DFA2_special = DFA.unpackEncodedString(DFA2_specialS);
    static final short[][] DFA2_transition;

    static {
        int numStates = DFA2_transitionS.length;
        DFA2_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA2_transition[i] = DFA.unpackEncodedString(DFA2_transitionS[i]);
        }
    }

    class DFA2 extends DFA {

        public DFA2(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 2;
            this.eot = DFA2_eot;
            this.eof = DFA2_eof;
            this.min = DFA2_min;
            this.max = DFA2_max;
            this.accept = DFA2_accept;
            this.special = DFA2_special;
            this.transition = DFA2_transition;
        }
        public String getDescription() {
            return "217:1: external_declaration options {k=2; } : ( ( ( ( declaration_specifiers )? ) ! ( declarator ( declaration )* ) ! '{' !)=> function_definition !| declaration );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA2_0 = input.LA(1);

                         
                        int index2_0 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (LA2_0==63||LA2_0==74||LA2_0==81||LA2_0==86) ) {s = 1;}

                        else if ( (LA2_0==92) ) {s = 2;}

                        else if ( (LA2_0==66) ) {s = 3;}

                        else if ( (LA2_0==83) ) {s = 4;}

                        else if ( (LA2_0==79) ) {s = 5;}

                        else if ( (LA2_0==80) ) {s = 6;}

                        else if ( (LA2_0==75) ) {s = 7;}

                        else if ( (LA2_0==71) ) {s = 8;}

                        else if ( (LA2_0==84) ) {s = 9;}

                        else if ( (LA2_0==91) ) {s = 10;}

                        else if ( (LA2_0==87||LA2_0==90) ) {s = 11;}

                        else if ( (LA2_0==73) ) {s = 12;}

                        else if ( (LA2_0==IDENTIFIER) ) {s = 13;}

                        else if ( (LA2_0==67||LA2_0==93) ) {s = 14;}

                        else if ( (LA2_0==32) && (synpred4_CSample())) {s = 15;}

                        else if ( (LA2_0==30) && (synpred4_CSample())) {s = 16;}

                        else if ( (LA2_0==89) ) {s = 17;}

                         
                        input.seek(index2_0);

                        if ( s>=0 ) return s;
                        break;

                    case 1 : 
                        int LA2_13 = input.LA(1);

                         
                        int index2_13 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (LA2_13==32) ) {s = 192;}

                        else if ( (LA2_13==IDENTIFIER) ) {s = 193;}

                        else if ( (LA2_13==30) ) {s = 194;}

                        else if ( (LA2_13==63||LA2_13==74||LA2_13==81||LA2_13==86) ) {s = 195;}

                        else if ( (LA2_13==92) ) {s = 196;}

                        else if ( (LA2_13==66) ) {s = 197;}

                        else if ( (LA2_13==83) ) {s = 198;}

                        else if ( (LA2_13==79) ) {s = 199;}

                        else if ( (LA2_13==80) ) {s = 200;}

                        else if ( (LA2_13==75) ) {s = 201;}

                        else if ( (LA2_13==71) ) {s = 202;}

                        else if ( (LA2_13==84) ) {s = 203;}

                        else if ( (LA2_13==91) ) {s = 204;}

                        else if ( (LA2_13==87||LA2_13==90) ) {s = 205;}

                        else if ( (LA2_13==73) ) {s = 206;}

                        else if ( (LA2_13==67||LA2_13==93) ) {s = 207;}

                        else if ( (LA2_13==59) && (synpred4_CSample())) {s = 208;}

                        else if ( (LA2_13==89) && (synpred4_CSample())) {s = 209;}

                        else if ( (LA2_13==95) && (synpred4_CSample())) {s = 210;}

                        else if ( (LA2_13==47) ) {s = 17;}

                         
                        input.seek(index2_13);

                        if ( s>=0 ) return s;
                        break;

                    case 2 : 
                        int LA2_18 = input.LA(1);

                         
                        int index2_18 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_18);

                        if ( s>=0 ) return s;
                        break;

                    case 3 : 
                        int LA2_19 = input.LA(1);

                         
                        int index2_19 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_19);

                        if ( s>=0 ) return s;
                        break;

                    case 4 : 
                        int LA2_20 = input.LA(1);

                         
                        int index2_20 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_20);

                        if ( s>=0 ) return s;
                        break;

                    case 5 : 
                        int LA2_21 = input.LA(1);

                         
                        int index2_21 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_21);

                        if ( s>=0 ) return s;
                        break;

                    case 6 : 
                        int LA2_22 = input.LA(1);

                         
                        int index2_22 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_22);

                        if ( s>=0 ) return s;
                        break;

                    case 7 : 
                        int LA2_23 = input.LA(1);

                         
                        int index2_23 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_23);

                        if ( s>=0 ) return s;
                        break;

                    case 8 : 
                        int LA2_24 = input.LA(1);

                         
                        int index2_24 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_24);

                        if ( s>=0 ) return s;
                        break;

                    case 9 : 
                        int LA2_25 = input.LA(1);

                         
                        int index2_25 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_25);

                        if ( s>=0 ) return s;
                        break;

                    case 10 : 
                        int LA2_26 = input.LA(1);

                         
                        int index2_26 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_26);

                        if ( s>=0 ) return s;
                        break;

                    case 11 : 
                        int LA2_27 = input.LA(1);

                         
                        int index2_27 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_27);

                        if ( s>=0 ) return s;
                        break;

                    case 12 : 
                        int LA2_28 = input.LA(1);

                         
                        int index2_28 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_28);

                        if ( s>=0 ) return s;
                        break;

                    case 13 : 
                        int LA2_29 = input.LA(1);

                         
                        int index2_29 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_29);

                        if ( s>=0 ) return s;
                        break;

                    case 14 : 
                        int LA2_30 = input.LA(1);

                         
                        int index2_30 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_30);

                        if ( s>=0 ) return s;
                        break;

                    case 15 : 
                        int LA2_31 = input.LA(1);

                         
                        int index2_31 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_31);

                        if ( s>=0 ) return s;
                        break;

                    case 16 : 
                        int LA2_32 = input.LA(1);

                         
                        int index2_32 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_32);

                        if ( s>=0 ) return s;
                        break;

                    case 17 : 
                        int LA2_33 = input.LA(1);

                         
                        int index2_33 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_33);

                        if ( s>=0 ) return s;
                        break;

                    case 18 : 
                        int LA2_35 = input.LA(1);

                         
                        int index2_35 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_35);

                        if ( s>=0 ) return s;
                        break;

                    case 19 : 
                        int LA2_36 = input.LA(1);

                         
                        int index2_36 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_36);

                        if ( s>=0 ) return s;
                        break;

                    case 20 : 
                        int LA2_37 = input.LA(1);

                         
                        int index2_37 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_37);

                        if ( s>=0 ) return s;
                        break;

                    case 21 : 
                        int LA2_38 = input.LA(1);

                         
                        int index2_38 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_38);

                        if ( s>=0 ) return s;
                        break;

                    case 22 : 
                        int LA2_39 = input.LA(1);

                         
                        int index2_39 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_39);

                        if ( s>=0 ) return s;
                        break;

                    case 23 : 
                        int LA2_40 = input.LA(1);

                         
                        int index2_40 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_40);

                        if ( s>=0 ) return s;
                        break;

                    case 24 : 
                        int LA2_41 = input.LA(1);

                         
                        int index2_41 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_41);

                        if ( s>=0 ) return s;
                        break;

                    case 25 : 
                        int LA2_42 = input.LA(1);

                         
                        int index2_42 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_42);

                        if ( s>=0 ) return s;
                        break;

                    case 26 : 
                        int LA2_43 = input.LA(1);

                         
                        int index2_43 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_43);

                        if ( s>=0 ) return s;
                        break;

                    case 27 : 
                        int LA2_44 = input.LA(1);

                         
                        int index2_44 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_44);

                        if ( s>=0 ) return s;
                        break;

                    case 28 : 
                        int LA2_45 = input.LA(1);

                         
                        int index2_45 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_45);

                        if ( s>=0 ) return s;
                        break;

                    case 29 : 
                        int LA2_46 = input.LA(1);

                         
                        int index2_46 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_46);

                        if ( s>=0 ) return s;
                        break;

                    case 30 : 
                        int LA2_47 = input.LA(1);

                         
                        int index2_47 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_47);

                        if ( s>=0 ) return s;
                        break;

                    case 31 : 
                        int LA2_48 = input.LA(1);

                         
                        int index2_48 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_48);

                        if ( s>=0 ) return s;
                        break;

                    case 32 : 
                        int LA2_49 = input.LA(1);

                         
                        int index2_49 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_49);

                        if ( s>=0 ) return s;
                        break;

                    case 33 : 
                        int LA2_50 = input.LA(1);

                         
                        int index2_50 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_50);

                        if ( s>=0 ) return s;
                        break;

                    case 34 : 
                        int LA2_52 = input.LA(1);

                         
                        int index2_52 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_52);

                        if ( s>=0 ) return s;
                        break;

                    case 35 : 
                        int LA2_53 = input.LA(1);

                         
                        int index2_53 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_53);

                        if ( s>=0 ) return s;
                        break;

                    case 36 : 
                        int LA2_54 = input.LA(1);

                         
                        int index2_54 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_54);

                        if ( s>=0 ) return s;
                        break;

                    case 37 : 
                        int LA2_55 = input.LA(1);

                         
                        int index2_55 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_55);

                        if ( s>=0 ) return s;
                        break;

                    case 38 : 
                        int LA2_56 = input.LA(1);

                         
                        int index2_56 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_56);

                        if ( s>=0 ) return s;
                        break;

                    case 39 : 
                        int LA2_57 = input.LA(1);

                         
                        int index2_57 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_57);

                        if ( s>=0 ) return s;
                        break;

                    case 40 : 
                        int LA2_58 = input.LA(1);

                         
                        int index2_58 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_58);

                        if ( s>=0 ) return s;
                        break;

                    case 41 : 
                        int LA2_59 = input.LA(1);

                         
                        int index2_59 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_59);

                        if ( s>=0 ) return s;
                        break;

                    case 42 : 
                        int LA2_60 = input.LA(1);

                         
                        int index2_60 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_60);

                        if ( s>=0 ) return s;
                        break;

                    case 43 : 
                        int LA2_61 = input.LA(1);

                         
                        int index2_61 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_61);

                        if ( s>=0 ) return s;
                        break;

                    case 44 : 
                        int LA2_62 = input.LA(1);

                         
                        int index2_62 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_62);

                        if ( s>=0 ) return s;
                        break;

                    case 45 : 
                        int LA2_63 = input.LA(1);

                         
                        int index2_63 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_63);

                        if ( s>=0 ) return s;
                        break;

                    case 46 : 
                        int LA2_64 = input.LA(1);

                         
                        int index2_64 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_64);

                        if ( s>=0 ) return s;
                        break;

                    case 47 : 
                        int LA2_65 = input.LA(1);

                         
                        int index2_65 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_65);

                        if ( s>=0 ) return s;
                        break;

                    case 48 : 
                        int LA2_66 = input.LA(1);

                         
                        int index2_66 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_66);

                        if ( s>=0 ) return s;
                        break;

                    case 49 : 
                        int LA2_67 = input.LA(1);

                         
                        int index2_67 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_67);

                        if ( s>=0 ) return s;
                        break;

                    case 50 : 
                        int LA2_69 = input.LA(1);

                         
                        int index2_69 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_69);

                        if ( s>=0 ) return s;
                        break;

                    case 51 : 
                        int LA2_70 = input.LA(1);

                         
                        int index2_70 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_70);

                        if ( s>=0 ) return s;
                        break;

                    case 52 : 
                        int LA2_71 = input.LA(1);

                         
                        int index2_71 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_71);

                        if ( s>=0 ) return s;
                        break;

                    case 53 : 
                        int LA2_72 = input.LA(1);

                         
                        int index2_72 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_72);

                        if ( s>=0 ) return s;
                        break;

                    case 54 : 
                        int LA2_73 = input.LA(1);

                         
                        int index2_73 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_73);

                        if ( s>=0 ) return s;
                        break;

                    case 55 : 
                        int LA2_74 = input.LA(1);

                         
                        int index2_74 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_74);

                        if ( s>=0 ) return s;
                        break;

                    case 56 : 
                        int LA2_75 = input.LA(1);

                         
                        int index2_75 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_75);

                        if ( s>=0 ) return s;
                        break;

                    case 57 : 
                        int LA2_76 = input.LA(1);

                         
                        int index2_76 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_76);

                        if ( s>=0 ) return s;
                        break;

                    case 58 : 
                        int LA2_77 = input.LA(1);

                         
                        int index2_77 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_77);

                        if ( s>=0 ) return s;
                        break;

                    case 59 : 
                        int LA2_78 = input.LA(1);

                         
                        int index2_78 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_78);

                        if ( s>=0 ) return s;
                        break;

                    case 60 : 
                        int LA2_79 = input.LA(1);

                         
                        int index2_79 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_79);

                        if ( s>=0 ) return s;
                        break;

                    case 61 : 
                        int LA2_80 = input.LA(1);

                         
                        int index2_80 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_80);

                        if ( s>=0 ) return s;
                        break;

                    case 62 : 
                        int LA2_81 = input.LA(1);

                         
                        int index2_81 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_81);

                        if ( s>=0 ) return s;
                        break;

                    case 63 : 
                        int LA2_82 = input.LA(1);

                         
                        int index2_82 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_82);

                        if ( s>=0 ) return s;
                        break;

                    case 64 : 
                        int LA2_83 = input.LA(1);

                         
                        int index2_83 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_83);

                        if ( s>=0 ) return s;
                        break;

                    case 65 : 
                        int LA2_84 = input.LA(1);

                         
                        int index2_84 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_84);

                        if ( s>=0 ) return s;
                        break;

                    case 66 : 
                        int LA2_86 = input.LA(1);

                         
                        int index2_86 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_86);

                        if ( s>=0 ) return s;
                        break;

                    case 67 : 
                        int LA2_87 = input.LA(1);

                         
                        int index2_87 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_87);

                        if ( s>=0 ) return s;
                        break;

                    case 68 : 
                        int LA2_88 = input.LA(1);

                         
                        int index2_88 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_88);

                        if ( s>=0 ) return s;
                        break;

                    case 69 : 
                        int LA2_89 = input.LA(1);

                         
                        int index2_89 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_89);

                        if ( s>=0 ) return s;
                        break;

                    case 70 : 
                        int LA2_90 = input.LA(1);

                         
                        int index2_90 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_90);

                        if ( s>=0 ) return s;
                        break;

                    case 71 : 
                        int LA2_91 = input.LA(1);

                         
                        int index2_91 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_91);

                        if ( s>=0 ) return s;
                        break;

                    case 72 : 
                        int LA2_92 = input.LA(1);

                         
                        int index2_92 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_92);

                        if ( s>=0 ) return s;
                        break;

                    case 73 : 
                        int LA2_93 = input.LA(1);

                         
                        int index2_93 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_93);

                        if ( s>=0 ) return s;
                        break;

                    case 74 : 
                        int LA2_94 = input.LA(1);

                         
                        int index2_94 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_94);

                        if ( s>=0 ) return s;
                        break;

                    case 75 : 
                        int LA2_95 = input.LA(1);

                         
                        int index2_95 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_95);

                        if ( s>=0 ) return s;
                        break;

                    case 76 : 
                        int LA2_96 = input.LA(1);

                         
                        int index2_96 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_96);

                        if ( s>=0 ) return s;
                        break;

                    case 77 : 
                        int LA2_97 = input.LA(1);

                         
                        int index2_97 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_97);

                        if ( s>=0 ) return s;
                        break;

                    case 78 : 
                        int LA2_98 = input.LA(1);

                         
                        int index2_98 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_98);

                        if ( s>=0 ) return s;
                        break;

                    case 79 : 
                        int LA2_99 = input.LA(1);

                         
                        int index2_99 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_99);

                        if ( s>=0 ) return s;
                        break;

                    case 80 : 
                        int LA2_100 = input.LA(1);

                         
                        int index2_100 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_100);

                        if ( s>=0 ) return s;
                        break;

                    case 81 : 
                        int LA2_101 = input.LA(1);

                         
                        int index2_101 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_101);

                        if ( s>=0 ) return s;
                        break;

                    case 82 : 
                        int LA2_103 = input.LA(1);

                         
                        int index2_103 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_103);

                        if ( s>=0 ) return s;
                        break;

                    case 83 : 
                        int LA2_104 = input.LA(1);

                         
                        int index2_104 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_104);

                        if ( s>=0 ) return s;
                        break;

                    case 84 : 
                        int LA2_105 = input.LA(1);

                         
                        int index2_105 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_105);

                        if ( s>=0 ) return s;
                        break;

                    case 85 : 
                        int LA2_106 = input.LA(1);

                         
                        int index2_106 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_106);

                        if ( s>=0 ) return s;
                        break;

                    case 86 : 
                        int LA2_107 = input.LA(1);

                         
                        int index2_107 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_107);

                        if ( s>=0 ) return s;
                        break;

                    case 87 : 
                        int LA2_108 = input.LA(1);

                         
                        int index2_108 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_108);

                        if ( s>=0 ) return s;
                        break;

                    case 88 : 
                        int LA2_109 = input.LA(1);

                         
                        int index2_109 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_109);

                        if ( s>=0 ) return s;
                        break;

                    case 89 : 
                        int LA2_110 = input.LA(1);

                         
                        int index2_110 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_110);

                        if ( s>=0 ) return s;
                        break;

                    case 90 : 
                        int LA2_111 = input.LA(1);

                         
                        int index2_111 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_111);

                        if ( s>=0 ) return s;
                        break;

                    case 91 : 
                        int LA2_112 = input.LA(1);

                         
                        int index2_112 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_112);

                        if ( s>=0 ) return s;
                        break;

                    case 92 : 
                        int LA2_113 = input.LA(1);

                         
                        int index2_113 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_113);

                        if ( s>=0 ) return s;
                        break;

                    case 93 : 
                        int LA2_114 = input.LA(1);

                         
                        int index2_114 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_114);

                        if ( s>=0 ) return s;
                        break;

                    case 94 : 
                        int LA2_115 = input.LA(1);

                         
                        int index2_115 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_115);

                        if ( s>=0 ) return s;
                        break;

                    case 95 : 
                        int LA2_116 = input.LA(1);

                         
                        int index2_116 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_116);

                        if ( s>=0 ) return s;
                        break;

                    case 96 : 
                        int LA2_117 = input.LA(1);

                         
                        int index2_117 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_117);

                        if ( s>=0 ) return s;
                        break;

                    case 97 : 
                        int LA2_118 = input.LA(1);

                         
                        int index2_118 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_118);

                        if ( s>=0 ) return s;
                        break;

                    case 98 : 
                        int LA2_120 = input.LA(1);

                         
                        int index2_120 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_120);

                        if ( s>=0 ) return s;
                        break;

                    case 99 : 
                        int LA2_121 = input.LA(1);

                         
                        int index2_121 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_121);

                        if ( s>=0 ) return s;
                        break;

                    case 100 : 
                        int LA2_122 = input.LA(1);

                         
                        int index2_122 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_122);

                        if ( s>=0 ) return s;
                        break;

                    case 101 : 
                        int LA2_123 = input.LA(1);

                         
                        int index2_123 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_123);

                        if ( s>=0 ) return s;
                        break;

                    case 102 : 
                        int LA2_124 = input.LA(1);

                         
                        int index2_124 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_124);

                        if ( s>=0 ) return s;
                        break;

                    case 103 : 
                        int LA2_125 = input.LA(1);

                         
                        int index2_125 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_125);

                        if ( s>=0 ) return s;
                        break;

                    case 104 : 
                        int LA2_126 = input.LA(1);

                         
                        int index2_126 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_126);

                        if ( s>=0 ) return s;
                        break;

                    case 105 : 
                        int LA2_127 = input.LA(1);

                         
                        int index2_127 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_127);

                        if ( s>=0 ) return s;
                        break;

                    case 106 : 
                        int LA2_128 = input.LA(1);

                         
                        int index2_128 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_128);

                        if ( s>=0 ) return s;
                        break;

                    case 107 : 
                        int LA2_129 = input.LA(1);

                         
                        int index2_129 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_129);

                        if ( s>=0 ) return s;
                        break;

                    case 108 : 
                        int LA2_130 = input.LA(1);

                         
                        int index2_130 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_130);

                        if ( s>=0 ) return s;
                        break;

                    case 109 : 
                        int LA2_131 = input.LA(1);

                         
                        int index2_131 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_131);

                        if ( s>=0 ) return s;
                        break;

                    case 110 : 
                        int LA2_132 = input.LA(1);

                         
                        int index2_132 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_132);

                        if ( s>=0 ) return s;
                        break;

                    case 111 : 
                        int LA2_133 = input.LA(1);

                         
                        int index2_133 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_133);

                        if ( s>=0 ) return s;
                        break;

                    case 112 : 
                        int LA2_134 = input.LA(1);

                         
                        int index2_134 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_134);

                        if ( s>=0 ) return s;
                        break;

                    case 113 : 
                        int LA2_135 = input.LA(1);

                         
                        int index2_135 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_135);

                        if ( s>=0 ) return s;
                        break;

                    case 114 : 
                        int LA2_137 = input.LA(1);

                         
                        int index2_137 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_137);

                        if ( s>=0 ) return s;
                        break;

                    case 115 : 
                        int LA2_138 = input.LA(1);

                         
                        int index2_138 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_138);

                        if ( s>=0 ) return s;
                        break;

                    case 116 : 
                        int LA2_139 = input.LA(1);

                         
                        int index2_139 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_139);

                        if ( s>=0 ) return s;
                        break;

                    case 117 : 
                        int LA2_140 = input.LA(1);

                         
                        int index2_140 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_140);

                        if ( s>=0 ) return s;
                        break;

                    case 118 : 
                        int LA2_141 = input.LA(1);

                         
                        int index2_141 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_141);

                        if ( s>=0 ) return s;
                        break;

                    case 119 : 
                        int LA2_142 = input.LA(1);

                         
                        int index2_142 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_142);

                        if ( s>=0 ) return s;
                        break;

                    case 120 : 
                        int LA2_143 = input.LA(1);

                         
                        int index2_143 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_143);

                        if ( s>=0 ) return s;
                        break;

                    case 121 : 
                        int LA2_144 = input.LA(1);

                         
                        int index2_144 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_144);

                        if ( s>=0 ) return s;
                        break;

                    case 122 : 
                        int LA2_145 = input.LA(1);

                         
                        int index2_145 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_145);

                        if ( s>=0 ) return s;
                        break;

                    case 123 : 
                        int LA2_146 = input.LA(1);

                         
                        int index2_146 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_146);

                        if ( s>=0 ) return s;
                        break;

                    case 124 : 
                        int LA2_147 = input.LA(1);

                         
                        int index2_147 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_147);

                        if ( s>=0 ) return s;
                        break;

                    case 125 : 
                        int LA2_148 = input.LA(1);

                         
                        int index2_148 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_148);

                        if ( s>=0 ) return s;
                        break;

                    case 126 : 
                        int LA2_149 = input.LA(1);

                         
                        int index2_149 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_149);

                        if ( s>=0 ) return s;
                        break;

                    case 127 : 
                        int LA2_150 = input.LA(1);

                         
                        int index2_150 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_150);

                        if ( s>=0 ) return s;
                        break;

                    case 128 : 
                        int LA2_151 = input.LA(1);

                         
                        int index2_151 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_151);

                        if ( s>=0 ) return s;
                        break;

                    case 129 : 
                        int LA2_152 = input.LA(1);

                         
                        int index2_152 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_152);

                        if ( s>=0 ) return s;
                        break;

                    case 130 : 
                        int LA2_154 = input.LA(1);

                         
                        int index2_154 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_154);

                        if ( s>=0 ) return s;
                        break;

                    case 131 : 
                        int LA2_155 = input.LA(1);

                         
                        int index2_155 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_155);

                        if ( s>=0 ) return s;
                        break;

                    case 132 : 
                        int LA2_156 = input.LA(1);

                         
                        int index2_156 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_156);

                        if ( s>=0 ) return s;
                        break;

                    case 133 : 
                        int LA2_157 = input.LA(1);

                         
                        int index2_157 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_157);

                        if ( s>=0 ) return s;
                        break;

                    case 134 : 
                        int LA2_158 = input.LA(1);

                         
                        int index2_158 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_158);

                        if ( s>=0 ) return s;
                        break;

                    case 135 : 
                        int LA2_159 = input.LA(1);

                         
                        int index2_159 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_159);

                        if ( s>=0 ) return s;
                        break;

                    case 136 : 
                        int LA2_160 = input.LA(1);

                         
                        int index2_160 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_160);

                        if ( s>=0 ) return s;
                        break;

                    case 137 : 
                        int LA2_161 = input.LA(1);

                         
                        int index2_161 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_161);

                        if ( s>=0 ) return s;
                        break;

                    case 138 : 
                        int LA2_162 = input.LA(1);

                         
                        int index2_162 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_162);

                        if ( s>=0 ) return s;
                        break;

                    case 139 : 
                        int LA2_163 = input.LA(1);

                         
                        int index2_163 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_163);

                        if ( s>=0 ) return s;
                        break;

                    case 140 : 
                        int LA2_164 = input.LA(1);

                         
                        int index2_164 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_164);

                        if ( s>=0 ) return s;
                        break;

                    case 141 : 
                        int LA2_165 = input.LA(1);

                         
                        int index2_165 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_165);

                        if ( s>=0 ) return s;
                        break;

                    case 142 : 
                        int LA2_166 = input.LA(1);

                         
                        int index2_166 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_166);

                        if ( s>=0 ) return s;
                        break;

                    case 143 : 
                        int LA2_167 = input.LA(1);

                         
                        int index2_167 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_167);

                        if ( s>=0 ) return s;
                        break;

                    case 144 : 
                        int LA2_168 = input.LA(1);

                         
                        int index2_168 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_168);

                        if ( s>=0 ) return s;
                        break;

                    case 145 : 
                        int LA2_169 = input.LA(1);

                         
                        int index2_169 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_169);

                        if ( s>=0 ) return s;
                        break;

                    case 146 : 
                        int LA2_171 = input.LA(1);

                         
                        int index2_171 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_171);

                        if ( s>=0 ) return s;
                        break;

                    case 147 : 
                        int LA2_172 = input.LA(1);

                         
                        int index2_172 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_172);

                        if ( s>=0 ) return s;
                        break;

                    case 148 : 
                        int LA2_173 = input.LA(1);

                         
                        int index2_173 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_173);

                        if ( s>=0 ) return s;
                        break;

                    case 149 : 
                        int LA2_174 = input.LA(1);

                         
                        int index2_174 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_174);

                        if ( s>=0 ) return s;
                        break;

                    case 150 : 
                        int LA2_175 = input.LA(1);

                         
                        int index2_175 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_175);

                        if ( s>=0 ) return s;
                        break;

                    case 151 : 
                        int LA2_176 = input.LA(1);

                         
                        int index2_176 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_176);

                        if ( s>=0 ) return s;
                        break;

                    case 152 : 
                        int LA2_177 = input.LA(1);

                         
                        int index2_177 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_177);

                        if ( s>=0 ) return s;
                        break;

                    case 153 : 
                        int LA2_178 = input.LA(1);

                         
                        int index2_178 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_178);

                        if ( s>=0 ) return s;
                        break;

                    case 154 : 
                        int LA2_179 = input.LA(1);

                         
                        int index2_179 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_179);

                        if ( s>=0 ) return s;
                        break;

                    case 155 : 
                        int LA2_180 = input.LA(1);

                         
                        int index2_180 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_180);

                        if ( s>=0 ) return s;
                        break;

                    case 156 : 
                        int LA2_181 = input.LA(1);

                         
                        int index2_181 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_181);

                        if ( s>=0 ) return s;
                        break;

                    case 157 : 
                        int LA2_182 = input.LA(1);

                         
                        int index2_182 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_182);

                        if ( s>=0 ) return s;
                        break;

                    case 158 : 
                        int LA2_183 = input.LA(1);

                         
                        int index2_183 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_183);

                        if ( s>=0 ) return s;
                        break;

                    case 159 : 
                        int LA2_184 = input.LA(1);

                         
                        int index2_184 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_184);

                        if ( s>=0 ) return s;
                        break;

                    case 160 : 
                        int LA2_185 = input.LA(1);

                         
                        int index2_185 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_185);

                        if ( s>=0 ) return s;
                        break;

                    case 161 : 
                        int LA2_186 = input.LA(1);

                         
                        int index2_186 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_186);

                        if ( s>=0 ) return s;
                        break;

                    case 162 : 
                        int LA2_188 = input.LA(1);

                         
                        int index2_188 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_188);

                        if ( s>=0 ) return s;
                        break;

                    case 163 : 
                        int LA2_189 = input.LA(1);

                         
                        int index2_189 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_189);

                        if ( s>=0 ) return s;
                        break;

                    case 164 : 
                        int LA2_190 = input.LA(1);

                         
                        int index2_190 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_190);

                        if ( s>=0 ) return s;
                        break;

                    case 165 : 
                        int LA2_191 = input.LA(1);

                         
                        int index2_191 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_191);

                        if ( s>=0 ) return s;
                        break;

                    case 166 : 
                        int LA2_192 = input.LA(1);

                         
                        int index2_192 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( ((isTypeName(input.LT(1).getText()))) ) {s = 17;}

                         
                        input.seek(index2_192);

                        if ( s>=0 ) return s;
                        break;

                    case 167 : 
                        int LA2_193 = input.LA(1);

                         
                        int index2_193 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( ((isTypeName(input.LT(1).getText()))) ) {s = 17;}

                         
                        input.seek(index2_193);

                        if ( s>=0 ) return s;
                        break;

                    case 168 : 
                        int LA2_194 = input.LA(1);

                         
                        int index2_194 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( ((isTypeName(input.LT(1).getText()))) ) {s = 17;}

                         
                        input.seek(index2_194);

                        if ( s>=0 ) return s;
                        break;

                    case 169 : 
                        int LA2_195 = input.LA(1);

                         
                        int index2_195 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( ((isTypeName(input.LT(1).getText()))) ) {s = 17;}

                         
                        input.seek(index2_195);

                        if ( s>=0 ) return s;
                        break;

                    case 170 : 
                        int LA2_196 = input.LA(1);

                         
                        int index2_196 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( ((isTypeName(input.LT(1).getText()))) ) {s = 17;}

                         
                        input.seek(index2_196);

                        if ( s>=0 ) return s;
                        break;

                    case 171 : 
                        int LA2_197 = input.LA(1);

                         
                        int index2_197 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( ((isTypeName(input.LT(1).getText()))) ) {s = 17;}

                         
                        input.seek(index2_197);

                        if ( s>=0 ) return s;
                        break;

                    case 172 : 
                        int LA2_198 = input.LA(1);

                         
                        int index2_198 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( ((isTypeName(input.LT(1).getText()))) ) {s = 17;}

                         
                        input.seek(index2_198);

                        if ( s>=0 ) return s;
                        break;

                    case 173 : 
                        int LA2_199 = input.LA(1);

                         
                        int index2_199 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( ((isTypeName(input.LT(1).getText()))) ) {s = 17;}

                         
                        input.seek(index2_199);

                        if ( s>=0 ) return s;
                        break;

                    case 174 : 
                        int LA2_200 = input.LA(1);

                         
                        int index2_200 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( ((isTypeName(input.LT(1).getText()))) ) {s = 17;}

                         
                        input.seek(index2_200);

                        if ( s>=0 ) return s;
                        break;

                    case 175 : 
                        int LA2_201 = input.LA(1);

                         
                        int index2_201 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( ((isTypeName(input.LT(1).getText()))) ) {s = 17;}

                         
                        input.seek(index2_201);

                        if ( s>=0 ) return s;
                        break;

                    case 176 : 
                        int LA2_202 = input.LA(1);

                         
                        int index2_202 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( ((isTypeName(input.LT(1).getText()))) ) {s = 17;}

                         
                        input.seek(index2_202);

                        if ( s>=0 ) return s;
                        break;

                    case 177 : 
                        int LA2_203 = input.LA(1);

                         
                        int index2_203 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( ((isTypeName(input.LT(1).getText()))) ) {s = 17;}

                         
                        input.seek(index2_203);

                        if ( s>=0 ) return s;
                        break;

                    case 178 : 
                        int LA2_204 = input.LA(1);

                         
                        int index2_204 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( ((isTypeName(input.LT(1).getText()))) ) {s = 17;}

                         
                        input.seek(index2_204);

                        if ( s>=0 ) return s;
                        break;

                    case 179 : 
                        int LA2_205 = input.LA(1);

                         
                        int index2_205 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( ((isTypeName(input.LT(1).getText()))) ) {s = 17;}

                         
                        input.seek(index2_205);

                        if ( s>=0 ) return s;
                        break;

                    case 180 : 
                        int LA2_206 = input.LA(1);

                         
                        int index2_206 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( ((isTypeName(input.LT(1).getText()))) ) {s = 17;}

                         
                        input.seek(index2_206);

                        if ( s>=0 ) return s;
                        break;

                    case 181 : 
                        int LA2_207 = input.LA(1);

                         
                        int index2_207 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( ((isTypeName(input.LT(1).getText()))) ) {s = 17;}

                         
                        input.seek(index2_207);

                        if ( s>=0 ) return s;
                        break;

                    case 182 : 
                        int LA2_212 = input.LA(1);

                         
                        int index2_212 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_212);

                        if ( s>=0 ) return s;
                        break;

                    case 183 : 
                        int LA2_213 = input.LA(1);

                         
                        int index2_213 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_213);

                        if ( s>=0 ) return s;
                        break;

                    case 184 : 
                        int LA2_214 = input.LA(1);

                         
                        int index2_214 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_214);

                        if ( s>=0 ) return s;
                        break;

                    case 185 : 
                        int LA2_215 = input.LA(1);

                         
                        int index2_215 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_215);

                        if ( s>=0 ) return s;
                        break;

                    case 186 : 
                        int LA2_216 = input.LA(1);

                         
                        int index2_216 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_216);

                        if ( s>=0 ) return s;
                        break;

                    case 187 : 
                        int LA2_217 = input.LA(1);

                         
                        int index2_217 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_217);

                        if ( s>=0 ) return s;
                        break;

                    case 188 : 
                        int LA2_218 = input.LA(1);

                         
                        int index2_218 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_218);

                        if ( s>=0 ) return s;
                        break;

                    case 189 : 
                        int LA2_219 = input.LA(1);

                         
                        int index2_219 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_219);

                        if ( s>=0 ) return s;
                        break;

                    case 190 : 
                        int LA2_220 = input.LA(1);

                         
                        int index2_220 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_220);

                        if ( s>=0 ) return s;
                        break;

                    case 191 : 
                        int LA2_221 = input.LA(1);

                         
                        int index2_221 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_221);

                        if ( s>=0 ) return s;
                        break;

                    case 192 : 
                        int LA2_222 = input.LA(1);

                         
                        int index2_222 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_222);

                        if ( s>=0 ) return s;
                        break;

                    case 193 : 
                        int LA2_223 = input.LA(1);

                         
                        int index2_223 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_223);

                        if ( s>=0 ) return s;
                        break;

                    case 194 : 
                        int LA2_224 = input.LA(1);

                         
                        int index2_224 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_224);

                        if ( s>=0 ) return s;
                        break;

                    case 195 : 
                        int LA2_225 = input.LA(1);

                         
                        int index2_225 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_225);

                        if ( s>=0 ) return s;
                        break;

                    case 196 : 
                        int LA2_226 = input.LA(1);

                         
                        int index2_226 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_226);

                        if ( s>=0 ) return s;
                        break;

                    case 197 : 
                        int LA2_227 = input.LA(1);

                         
                        int index2_227 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred4_CSample()) ) {s = 210;}

                        else if ( (true) ) {s = 17;}

                         
                        input.seek(index2_227);

                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}

            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 2, _s, input);
            error(nvae);
            throw nvae;
        }

    }
    static final String DFA52_eotS =
        "\157\uffff";
    static final String DFA52_eofS =
        "\1\uffff\2\17\154\uffff";
    static final String DFA52_minS =
        "\1\4\2\30\5\4\6\0\26\uffff\6\0\26\uffff\7\0\14\uffff\34\0";
    static final String DFA52_maxS =
        "\1\144\2\143\5\144\6\0\26\uffff\6\0\26\uffff\7\0\14\uffff\34\0";
    static final String DFA52_acceptS =
        "\16\uffff\1\1\1\2\137\uffff";
    static final String DFA52_specialS =
        "\10\uffff\1\0\1\1\1\2\1\3\1\4\1\5\26\uffff\1\6\1\7\1\10\1\11\1\12"+
        "\1\13\26\uffff\1\14\1\15\1\16\1\17\1\20\1\21\1\22\14\uffff\1\23"+
        "\1\24\1\25\1\26\1\27\1\30\1\31\1\32\1\33\1\34\1\35\1\36\1\37\1\40"+
        "\1\41\1\42\1\43\1\44\1\45\1\46\1\47\1\50\1\51\1\52\1\53\1\54\1\55"+
        "\1\56}>";
    static final String[] DFA52_transitionS = {
            "\1\2\1\uffff\1\2\2\uffff\1\2\1\uffff\1\2\1\uffff\1\1\4\uffff"+
            "\1\2\1\uffff\1\2\2\uffff\1\6\4\uffff\1\6\1\uffff\1\3\1\uffff"+
            "\1\6\1\uffff\1\6\1\4\2\uffff\1\6\1\5\55\uffff\1\7\16\uffff\1"+
            "\6",
            "\2\17\1\16\2\17\1\16\1\11\2\17\1\16\1\17\1\14\1\16\2\17\1\15"+
            "\1\16\1\13\1\12\1\uffff\1\17\1\16\4\17\1\16\1\17\1\16\4\17\1"+
            "\16\1\17\1\10\2\17\1\16\41\uffff\1\17\1\16\2\17",
            "\2\17\1\16\2\17\1\16\1\45\2\17\1\16\1\17\1\50\1\16\2\17\1\51"+
            "\1\16\1\47\1\46\1\uffff\1\17\1\16\4\17\1\16\1\17\1\16\4\17\1"+
            "\16\1\17\1\44\2\17\1\16\41\uffff\1\17\1\16\2\17",
            "\1\101\1\uffff\1\101\2\uffff\1\101\1\uffff\1\101\1\uffff\1"+
            "\100\4\uffff\1\101\1\uffff\1\101\2\uffff\1\105\4\uffff\1\105"+
            "\1\uffff\1\102\1\uffff\1\105\1\uffff\1\105\1\103\2\uffff\1\105"+
            "\1\104\32\uffff\2\17\3\uffff\1\17\1\uffff\1\17\1\uffff\1\17"+
            "\3\uffff\2\17\2\uffff\2\17\1\106\1\uffff\1\17\2\uffff\4\17\6"+
            "\uffff\1\105",
            "\1\124\1\uffff\1\124\2\uffff\1\124\1\uffff\1\124\1\uffff\1"+
            "\123\4\uffff\1\124\1\uffff\1\124\2\uffff\1\130\4\uffff\1\130"+
            "\1\uffff\1\125\1\uffff\1\130\1\uffff\1\130\1\126\2\uffff\1\130"+
            "\1\127\55\uffff\1\131\16\uffff\1\130",
            "\1\133\1\uffff\1\133\2\uffff\1\133\1\uffff\1\133\1\uffff\1"+
            "\132\4\uffff\1\133\1\uffff\1\133\2\uffff\1\137\4\uffff\1\137"+
            "\1\uffff\1\134\1\uffff\1\137\1\uffff\1\137\1\135\2\uffff\1\137"+
            "\1\136\55\uffff\1\140\16\uffff\1\137",
            "\1\143\1\uffff\1\143\2\uffff\1\143\1\uffff\1\143\1\uffff\1"+
            "\142\4\uffff\1\143\1\uffff\1\143\2\uffff\1\146\4\uffff\1\146"+
            "\1\uffff\1\141\1\uffff\1\146\1\uffff\1\146\1\144\2\uffff\1\146"+
            "\1\145\55\uffff\1\147\16\uffff\1\146",
            "\1\152\1\uffff\1\152\2\uffff\1\152\1\uffff\1\152\1\uffff\1"+
            "\151\4\uffff\1\152\1\uffff\1\152\2\uffff\1\155\4\uffff\1\155"+
            "\1\uffff\1\150\1\uffff\1\155\1\uffff\1\155\1\153\2\uffff\1\155"+
            "\1\154\55\uffff\1\156\16\uffff\1\155",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff"
    };

    static final short[] DFA52_eot = DFA.unpackEncodedString(DFA52_eotS);
    static final short[] DFA52_eof = DFA.unpackEncodedString(DFA52_eofS);
    static final char[] DFA52_min = DFA.unpackEncodedStringToUnsignedChars(DFA52_minS);
    static final char[] DFA52_max = DFA.unpackEncodedStringToUnsignedChars(DFA52_maxS);
    static final short[] DFA52_accept = DFA.unpackEncodedString(DFA52_acceptS);
    static final short[] DFA52_special = DFA.unpackEncodedString(DFA52_specialS);
    static final short[][] DFA52_transition;

    static {
        int numStates = DFA52_transitionS.length;
        DFA52_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA52_transition[i] = DFA.unpackEncodedString(DFA52_transitionS[i]);
        }
    }

    class DFA52 extends DFA {

        public DFA52(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 52;
            this.eot = DFA52_eot;
            this.eof = DFA52_eof;
            this.min = DFA52_min;
            this.max = DFA52_max;
            this.accept = DFA52_accept;
            this.special = DFA52_special;
            this.transition = DFA52_transition;
        }
        public String getDescription() {
            return "547:1: assignment_expression : ( lvalue assignment_operator assignment_expression | conditional_expression );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA52_8 = input.LA(1);

                         
                        int index52_8 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_8);

                        if ( s>=0 ) return s;
                        break;

                    case 1 : 
                        int LA52_9 = input.LA(1);

                         
                        int index52_9 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_9);

                        if ( s>=0 ) return s;
                        break;

                    case 2 : 
                        int LA52_10 = input.LA(1);

                         
                        int index52_10 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_10);

                        if ( s>=0 ) return s;
                        break;

                    case 3 : 
                        int LA52_11 = input.LA(1);

                         
                        int index52_11 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_11);

                        if ( s>=0 ) return s;
                        break;

                    case 4 : 
                        int LA52_12 = input.LA(1);

                         
                        int index52_12 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_12);

                        if ( s>=0 ) return s;
                        break;

                    case 5 : 
                        int LA52_13 = input.LA(1);

                         
                        int index52_13 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_13);

                        if ( s>=0 ) return s;
                        break;

                    case 6 : 
                        int LA52_36 = input.LA(1);

                         
                        int index52_36 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_36);

                        if ( s>=0 ) return s;
                        break;

                    case 7 : 
                        int LA52_37 = input.LA(1);

                         
                        int index52_37 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_37);

                        if ( s>=0 ) return s;
                        break;

                    case 8 : 
                        int LA52_38 = input.LA(1);

                         
                        int index52_38 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_38);

                        if ( s>=0 ) return s;
                        break;

                    case 9 : 
                        int LA52_39 = input.LA(1);

                         
                        int index52_39 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_39);

                        if ( s>=0 ) return s;
                        break;

                    case 10 : 
                        int LA52_40 = input.LA(1);

                         
                        int index52_40 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_40);

                        if ( s>=0 ) return s;
                        break;

                    case 11 : 
                        int LA52_41 = input.LA(1);

                         
                        int index52_41 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_41);

                        if ( s>=0 ) return s;
                        break;

                    case 12 : 
                        int LA52_64 = input.LA(1);

                         
                        int index52_64 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_64);

                        if ( s>=0 ) return s;
                        break;

                    case 13 : 
                        int LA52_65 = input.LA(1);

                         
                        int index52_65 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_65);

                        if ( s>=0 ) return s;
                        break;

                    case 14 : 
                        int LA52_66 = input.LA(1);

                         
                        int index52_66 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_66);

                        if ( s>=0 ) return s;
                        break;

                    case 15 : 
                        int LA52_67 = input.LA(1);

                         
                        int index52_67 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_67);

                        if ( s>=0 ) return s;
                        break;

                    case 16 : 
                        int LA52_68 = input.LA(1);

                         
                        int index52_68 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_68);

                        if ( s>=0 ) return s;
                        break;

                    case 17 : 
                        int LA52_69 = input.LA(1);

                         
                        int index52_69 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_69);

                        if ( s>=0 ) return s;
                        break;

                    case 18 : 
                        int LA52_70 = input.LA(1);

                         
                        int index52_70 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_70);

                        if ( s>=0 ) return s;
                        break;

                    case 19 : 
                        int LA52_83 = input.LA(1);

                         
                        int index52_83 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_83);

                        if ( s>=0 ) return s;
                        break;

                    case 20 : 
                        int LA52_84 = input.LA(1);

                         
                        int index52_84 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_84);

                        if ( s>=0 ) return s;
                        break;

                    case 21 : 
                        int LA52_85 = input.LA(1);

                         
                        int index52_85 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_85);

                        if ( s>=0 ) return s;
                        break;

                    case 22 : 
                        int LA52_86 = input.LA(1);

                         
                        int index52_86 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_86);

                        if ( s>=0 ) return s;
                        break;

                    case 23 : 
                        int LA52_87 = input.LA(1);

                         
                        int index52_87 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_87);

                        if ( s>=0 ) return s;
                        break;

                    case 24 : 
                        int LA52_88 = input.LA(1);

                         
                        int index52_88 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_88);

                        if ( s>=0 ) return s;
                        break;

                    case 25 : 
                        int LA52_89 = input.LA(1);

                         
                        int index52_89 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_89);

                        if ( s>=0 ) return s;
                        break;

                    case 26 : 
                        int LA52_90 = input.LA(1);

                         
                        int index52_90 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_90);

                        if ( s>=0 ) return s;
                        break;

                    case 27 : 
                        int LA52_91 = input.LA(1);

                         
                        int index52_91 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_91);

                        if ( s>=0 ) return s;
                        break;

                    case 28 : 
                        int LA52_92 = input.LA(1);

                         
                        int index52_92 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_92);

                        if ( s>=0 ) return s;
                        break;

                    case 29 : 
                        int LA52_93 = input.LA(1);

                         
                        int index52_93 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_93);

                        if ( s>=0 ) return s;
                        break;

                    case 30 : 
                        int LA52_94 = input.LA(1);

                         
                        int index52_94 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_94);

                        if ( s>=0 ) return s;
                        break;

                    case 31 : 
                        int LA52_95 = input.LA(1);

                         
                        int index52_95 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_95);

                        if ( s>=0 ) return s;
                        break;

                    case 32 : 
                        int LA52_96 = input.LA(1);

                         
                        int index52_96 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_96);

                        if ( s>=0 ) return s;
                        break;

                    case 33 : 
                        int LA52_97 = input.LA(1);

                         
                        int index52_97 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_97);

                        if ( s>=0 ) return s;
                        break;

                    case 34 : 
                        int LA52_98 = input.LA(1);

                         
                        int index52_98 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_98);

                        if ( s>=0 ) return s;
                        break;

                    case 35 : 
                        int LA52_99 = input.LA(1);

                         
                        int index52_99 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_99);

                        if ( s>=0 ) return s;
                        break;

                    case 36 : 
                        int LA52_100 = input.LA(1);

                         
                        int index52_100 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_100);

                        if ( s>=0 ) return s;
                        break;

                    case 37 : 
                        int LA52_101 = input.LA(1);

                         
                        int index52_101 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_101);

                        if ( s>=0 ) return s;
                        break;

                    case 38 : 
                        int LA52_102 = input.LA(1);

                         
                        int index52_102 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_102);

                        if ( s>=0 ) return s;
                        break;

                    case 39 : 
                        int LA52_103 = input.LA(1);

                         
                        int index52_103 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_103);

                        if ( s>=0 ) return s;
                        break;

                    case 40 : 
                        int LA52_104 = input.LA(1);

                         
                        int index52_104 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_104);

                        if ( s>=0 ) return s;
                        break;

                    case 41 : 
                        int LA52_105 = input.LA(1);

                         
                        int index52_105 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_105);

                        if ( s>=0 ) return s;
                        break;

                    case 42 : 
                        int LA52_106 = input.LA(1);

                         
                        int index52_106 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_106);

                        if ( s>=0 ) return s;
                        break;

                    case 43 : 
                        int LA52_107 = input.LA(1);

                         
                        int index52_107 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_107);

                        if ( s>=0 ) return s;
                        break;

                    case 44 : 
                        int LA52_108 = input.LA(1);

                         
                        int index52_108 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_108);

                        if ( s>=0 ) return s;
                        break;

                    case 45 : 
                        int LA52_109 = input.LA(1);

                         
                        int index52_109 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_109);

                        if ( s>=0 ) return s;
                        break;

                    case 46 : 
                        int LA52_110 = input.LA(1);

                         
                        int index52_110 = input.index();
                        input.rewind();

                        s = -1;
                        if ( (synpred104_CSample()) ) {s = 14;}

                        else if ( (true) ) {s = 15;}

                         
                        input.seek(index52_110);

                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}

            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 52, _s, input);
            error(nvae);
            throw nvae;
        }

    }
    static final String DFA64_eotS =
        "\112\uffff";
    static final String DFA64_eofS =
        "\112\uffff";
    static final String DFA64_minS =
        "\1\4\1\15\44\uffff\1\0\5\uffff\1\0\16\uffff\1\0\16\uffff";
    static final String DFA64_maxS =
        "\1\144\1\142\44\uffff\1\0\5\uffff\1\0\16\uffff\1\0\16\uffff";
    static final String DFA64_acceptS =
        "\2\uffff\1\2\23\uffff\1\1\63\uffff";
    static final String DFA64_specialS =
        "\46\uffff\1\0\5\uffff\1\1\16\uffff\1\2\16\uffff}>";
    static final String[] DFA64_transitionS = {
            "\1\2\1\uffff\1\2\2\uffff\1\2\1\uffff\1\2\1\uffff\1\1\4\uffff"+
            "\1\2\1\uffff\1\2\2\uffff\1\2\4\uffff\1\2\1\uffff\1\2\1\uffff"+
            "\1\2\1\uffff\2\2\2\uffff\2\2\7\uffff\1\2\17\uffff\1\26\2\2\2"+
            "\26\3\2\1\26\1\uffff\3\26\3\2\3\26\1\2\2\26\1\2\2\26\1\2\5\26"+
            "\2\2\3\uffff\2\2",
            "\1\26\12\uffff\6\2\1\46\1\uffff\1\54\12\2\1\uffff\3\2\1\73"+
            "\14\2\1\uffff\2\2\1\26\2\uffff\2\26\3\uffff\1\26\1\uffff\3\26"+
            "\3\uffff\3\26\1\uffff\2\26\1\uffff\2\26\2\uffff\4\26\2\uffff"+
            "\3\2",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA64_eot = DFA.unpackEncodedString(DFA64_eotS);
    static final short[] DFA64_eof = DFA.unpackEncodedString(DFA64_eofS);
    static final char[] DFA64_min = DFA.unpackEncodedStringToUnsignedChars(DFA64_minS);
    static final char[] DFA64_max = DFA.unpackEncodedStringToUnsignedChars(DFA64_maxS);
    static final short[] DFA64_accept = DFA.unpackEncodedString(DFA64_acceptS);
    static final short[] DFA64_special = DFA.unpackEncodedString(DFA64_specialS);
    static final short[][] DFA64_transition;

    static {
        int numStates = DFA64_transitionS.length;
        DFA64_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA64_transition[i] = DFA.unpackEncodedString(DFA64_transitionS[i]);
        }
    }

    class DFA64 extends DFA {

        public DFA64(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 64;
            this.eot = DFA64_eot;
            this.eof = DFA64_eof;
            this.min = DFA64_min;
            this.max = DFA64_max;
            this.accept = DFA64_accept;
            this.special = DFA64_special;
            this.transition = DFA64_transition;
        }
        public String getDescription() {
            return "()* loopback of 628:9: ( declaration )*";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA64_38 = input.LA(1);

                         
                        int index64_38 = input.index();
                        input.rewind();

                        s = -1;
                        if ( ((((isTypeName(input.LT(1).getText()))&&(isTypeName(input.LT(1).getText())))&&synpred136_CSample())) ) {s = 22;}

                        else if ( (true) ) {s = 2;}

                         
                        input.seek(index64_38);

                        if ( s>=0 ) return s;
                        break;

                    case 1 : 
                        int LA64_44 = input.LA(1);

                         
                        int index64_44 = input.index();
                        input.rewind();

                        s = -1;
                        if ( ((((isTypeName(input.LT(1).getText()))&&(isTypeName(input.LT(1).getText())))&&synpred136_CSample())) ) {s = 22;}

                        else if ( (true) ) {s = 2;}

                         
                        input.seek(index64_44);

                        if ( s>=0 ) return s;
                        break;

                    case 2 : 
                        int LA64_59 = input.LA(1);

                         
                        int index64_59 = input.index();
                        input.rewind();

                        s = -1;
                        if ( ((((isTypeName(input.LT(1).getText()))&&(isTypeName(input.LT(1).getText())))&&synpred136_CSample())) ) {s = 22;}

                        else if ( (true) ) {s = 2;}

                         
                        input.seek(index64_59);

                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}

            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 64, _s, input);
            error(nvae);
            throw nvae;
        }

    }
 

    public static final BitSet FOLLOW_translation_unit_in_program125 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_external_declaration_in_translation_unit147 = new BitSet(new long[]{0x8000000140002002L,0x000000003EDB8E8CL});
    public static final BitSet FOLLOW_function_definition_in_external_declaration191 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_declaration_in_external_declaration199 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_declaration_specifiers_in_function_definition230 = new BitSet(new long[]{0x0000000140002000L});
    public static final BitSet FOLLOW_declarator_in_function_definition233 = new BitSet(new long[]{0x8000000000002000L,0x00000000BEDB8E8CL});
    public static final BitSet FOLLOW_declaration_in_function_definition241 = new BitSet(new long[]{0x8000000000002000L,0x00000000BEDB8E8CL});
    public static final BitSet FOLLOW_compound_statement_in_function_definition244 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_compound_statement_in_function_definition251 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_89_in_declaration280 = new BitSet(new long[]{0x8000000140002000L,0x000000003CDB8E8CL});
    public static final BitSet FOLLOW_declaration_specifiers_in_declaration282 = new BitSet(new long[]{0x0000000140002000L});
    public static final BitSet FOLLOW_init_declarator_list_in_declaration290 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_declaration292 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_declaration_specifiers_in_declaration301 = new BitSet(new long[]{0x0000800140002000L});
    public static final BitSet FOLLOW_init_declarator_list_in_declaration306 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_declaration310 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_storage_class_specifier_in_declaration_specifiers336 = new BitSet(new long[]{0x8000000000002002L,0x000000003CDB8E8CL});
    public static final BitSet FOLLOW_type_specifier_in_declaration_specifiers343 = new BitSet(new long[]{0x8000000000002002L,0x000000003CDB8E8CL});
    public static final BitSet FOLLOW_type_qualifier_in_declaration_specifiers359 = new BitSet(new long[]{0x8000000000002002L,0x000000003CDB8E8CL});
    public static final BitSet FOLLOW_init_declarator_in_init_declarator_list381 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_37_in_init_declarator_list384 = new BitSet(new long[]{0x0000000140002000L});
    public static final BitSet FOLLOW_init_declarator_in_init_declarator_list386 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_declarator_in_init_declarator399 = new BitSet(new long[]{0x0010000000000002L});
    public static final BitSet FOLLOW_52_in_init_declarator402 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001080200000L});
    public static final BitSet FOLLOW_initializer_in_init_declarator404 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_92_in_type_specifier452 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_66_in_type_specifier457 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_83_in_type_specifier462 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_79_in_type_specifier467 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_80_in_type_specifier472 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_75_in_type_specifier477 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_71_in_type_specifier482 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_84_in_type_specifier487 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_91_in_type_specifier492 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_struct_or_union_specifier_in_type_specifier497 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_enum_specifier_in_type_specifier504 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_type_id_in_type_specifier511 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTIFIER_in_type_id531 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_struct_or_union_in_struct_or_union_specifier566 = new BitSet(new long[]{0x0000000000002000L,0x0000000080000000L});
    public static final BitSet FOLLOW_IDENTIFIER_in_struct_or_union_specifier570 = new BitSet(new long[]{0x0000000000000000L,0x0000000080000000L});
    public static final BitSet FOLLOW_95_in_struct_or_union_specifier573 = new BitSet(new long[]{0x0000000000002000L,0x000000003C998A8CL});
    public static final BitSet FOLLOW_struct_declaration_list_in_struct_or_union_specifier577 = new BitSet(new long[]{0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_99_in_struct_or_union_specifier579 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_struct_or_union_in_struct_or_union_specifier589 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_IDENTIFIER_in_struct_or_union_specifier591 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_struct_declaration_in_struct_declaration_list628 = new BitSet(new long[]{0x0000000000002002L,0x000000003C998A8CL});
    public static final BitSet FOLLOW_specifier_qualifier_list_in_struct_declaration646 = new BitSet(new long[]{0x0000400140002000L});
    public static final BitSet FOLLOW_struct_declarator_list_in_struct_declaration650 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_struct_declaration652 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_type_qualifier_in_specifier_qualifier_list670 = new BitSet(new long[]{0x0000000000002002L,0x000000003C998A8CL});
    public static final BitSet FOLLOW_type_specifier_in_specifier_qualifier_list674 = new BitSet(new long[]{0x0000000000002002L,0x000000003C998A8CL});
    public static final BitSet FOLLOW_struct_declarator_in_struct_declarator_list688 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_37_in_struct_declarator_list691 = new BitSet(new long[]{0x0000400140002000L});
    public static final BitSet FOLLOW_struct_declarator_in_struct_declarator_list693 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_declarator_in_struct_declarator706 = new BitSet(new long[]{0x0000400000000002L});
    public static final BitSet FOLLOW_46_in_struct_declarator709 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_constant_expression_in_struct_declarator711 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_46_in_struct_declarator718 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_constant_expression_in_struct_declarator720 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_73_in_enum_specifier738 = new BitSet(new long[]{0x0000000000000000L,0x0000000080000000L});
    public static final BitSet FOLLOW_95_in_enum_specifier740 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_enumerator_list_in_enum_specifier742 = new BitSet(new long[]{0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_99_in_enum_specifier744 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_73_in_enum_specifier749 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_IDENTIFIER_in_enum_specifier751 = new BitSet(new long[]{0x0000000000000000L,0x0000000080000000L});
    public static final BitSet FOLLOW_95_in_enum_specifier753 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_enumerator_list_in_enum_specifier755 = new BitSet(new long[]{0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_99_in_enum_specifier757 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_73_in_enum_specifier762 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_IDENTIFIER_in_enum_specifier764 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_enumerator_in_enumerator_list775 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_37_in_enumerator_list778 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_enumerator_in_enumerator_list780 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_IDENTIFIER_in_enumerator793 = new BitSet(new long[]{0x0010000000000002L});
    public static final BitSet FOLLOW_52_in_enumerator796 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_constant_expression_in_enumerator798 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_pointer_in_declarator827 = new BitSet(new long[]{0x0000000040002000L});
    public static final BitSet FOLLOW_direct_declarator_in_declarator830 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_pointer_in_declarator835 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTIFIER_in_direct_declarator850 = new BitSet(new long[]{0x0800000040000002L});
    public static final BitSet FOLLOW_30_in_direct_declarator861 = new BitSet(new long[]{0x0000000140002000L});
    public static final BitSet FOLLOW_declarator_in_direct_declarator863 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_direct_declarator865 = new BitSet(new long[]{0x0800000040000002L});
    public static final BitSet FOLLOW_declarator_suffix_in_direct_declarator879 = new BitSet(new long[]{0x0800000040000002L});
    public static final BitSet FOLLOW_59_in_declarator_suffix893 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_constant_expression_in_declarator_suffix895 = new BitSet(new long[]{0x1000000000000000L});
    public static final BitSet FOLLOW_60_in_declarator_suffix897 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_59_in_declarator_suffix907 = new BitSet(new long[]{0x1000000000000000L});
    public static final BitSet FOLLOW_60_in_declarator_suffix909 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_declarator_suffix919 = new BitSet(new long[]{0x8000000000002000L,0x000000003CDB8E8CL});
    public static final BitSet FOLLOW_parameter_type_list_in_declarator_suffix921 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_declarator_suffix923 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_declarator_suffix933 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_identifier_list_in_declarator_suffix935 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_declarator_suffix937 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_declarator_suffix947 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_declarator_suffix949 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_pointer960 = new BitSet(new long[]{0x0000000000000000L,0x0000000020000008L});
    public static final BitSet FOLLOW_type_qualifier_in_pointer962 = new BitSet(new long[]{0x0000000100000002L,0x0000000020000008L});
    public static final BitSet FOLLOW_pointer_in_pointer965 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_pointer971 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_pointer_in_pointer973 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_pointer978 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parameter_list_in_parameter_type_list989 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_37_in_parameter_type_list992 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_parameter_type_list994 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parameter_declaration_in_parameter_list1007 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_37_in_parameter_list1010 = new BitSet(new long[]{0x8000000000002000L,0x000000003CDB8E8CL});
    public static final BitSet FOLLOW_parameter_declaration_in_parameter_list1012 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_declaration_specifiers_in_parameter_declaration1025 = new BitSet(new long[]{0x0800000140002002L});
    public static final BitSet FOLLOW_declarator_in_parameter_declaration1028 = new BitSet(new long[]{0x0800000140002002L});
    public static final BitSet FOLLOW_abstract_declarator_in_parameter_declaration1030 = new BitSet(new long[]{0x0800000140002002L});
    public static final BitSet FOLLOW_IDENTIFIER_in_identifier_list1043 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_37_in_identifier_list1046 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_IDENTIFIER_in_identifier_list1048 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_specifier_qualifier_list_in_type_name1061 = new BitSet(new long[]{0x0800000140000002L});
    public static final BitSet FOLLOW_abstract_declarator_in_type_name1063 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_pointer_in_abstract_declarator1075 = new BitSet(new long[]{0x0800000040000002L});
    public static final BitSet FOLLOW_direct_abstract_declarator_in_abstract_declarator1077 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_direct_abstract_declarator_in_abstract_declarator1083 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_direct_abstract_declarator1096 = new BitSet(new long[]{0x0800000140000000L});
    public static final BitSet FOLLOW_abstract_declarator_in_direct_abstract_declarator1098 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_direct_abstract_declarator1100 = new BitSet(new long[]{0x0800000040000002L});
    public static final BitSet FOLLOW_abstract_declarator_suffix_in_direct_abstract_declarator1104 = new BitSet(new long[]{0x0800000040000002L});
    public static final BitSet FOLLOW_abstract_declarator_suffix_in_direct_abstract_declarator1108 = new BitSet(new long[]{0x0800000040000002L});
    public static final BitSet FOLLOW_59_in_abstract_declarator_suffix1120 = new BitSet(new long[]{0x1000000000000000L});
    public static final BitSet FOLLOW_60_in_abstract_declarator_suffix1122 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_59_in_abstract_declarator_suffix1127 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_constant_expression_in_abstract_declarator_suffix1129 = new BitSet(new long[]{0x1000000000000000L});
    public static final BitSet FOLLOW_60_in_abstract_declarator_suffix1131 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_abstract_declarator_suffix1136 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_abstract_declarator_suffix1138 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_abstract_declarator_suffix1143 = new BitSet(new long[]{0x8000000000002000L,0x000000003CDB8E8CL});
    public static final BitSet FOLLOW_parameter_type_list_in_abstract_declarator_suffix1145 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_abstract_declarator_suffix1147 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_assignment_expression_in_initializer1159 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_95_in_initializer1164 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001080200000L});
    public static final BitSet FOLLOW_initializer_list_in_initializer1166 = new BitSet(new long[]{0x0000002000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_37_in_initializer1168 = new BitSet(new long[]{0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_99_in_initializer1171 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_initializer_in_initializer_list1182 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_37_in_initializer_list1185 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001080200000L});
    public static final BitSet FOLLOW_initializer_in_initializer_list1187 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_assignment_expression_in_argument_expression_list1204 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_37_in_argument_expression_list1207 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_assignment_expression_in_argument_expression_list1209 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_multiplicative_expression_in_additive_expression1223 = new BitSet(new long[]{0x0000004400000002L});
    public static final BitSet FOLLOW_34_in_additive_expression1227 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_multiplicative_expression_in_additive_expression1229 = new BitSet(new long[]{0x0000004400000002L});
    public static final BitSet FOLLOW_38_in_additive_expression1233 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_multiplicative_expression_in_additive_expression1235 = new BitSet(new long[]{0x0000004400000002L});
    public static final BitSet FOLLOW_cast_expression_in_multiplicative_expression1249 = new BitSet(new long[]{0x0000100102000002L});
    public static final BitSet FOLLOW_32_in_multiplicative_expression1253 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_cast_expression_in_multiplicative_expression1255 = new BitSet(new long[]{0x0000100102000002L});
    public static final BitSet FOLLOW_44_in_multiplicative_expression1259 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_cast_expression_in_multiplicative_expression1261 = new BitSet(new long[]{0x0000100102000002L});
    public static final BitSet FOLLOW_25_in_multiplicative_expression1265 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_cast_expression_in_multiplicative_expression1267 = new BitSet(new long[]{0x0000100102000002L});
    public static final BitSet FOLLOW_30_in_cast_expression1280 = new BitSet(new long[]{0x0000000000002000L,0x000000003C998A8CL});
    public static final BitSet FOLLOW_type_name_in_cast_expression1282 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_cast_expression1284 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_cast_expression_in_cast_expression1286 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_unary_expression_in_cast_expression1291 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_postfix_expression_in_unary_expression1302 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_unary_expression1307 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_unary_expression_in_unary_expression1309 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_unary_expression1314 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_unary_expression_in_unary_expression1316 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_unary_operator_in_unary_expression1321 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_cast_expression_in_unary_expression1323 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_85_in_unary_expression1328 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_unary_expression_in_unary_expression1330 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_85_in_unary_expression1335 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_30_in_unary_expression1337 = new BitSet(new long[]{0x0000000000002000L,0x000000003C998A8CL});
    public static final BitSet FOLLOW_type_name_in_unary_expression1339 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_unary_expression1341 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_primary_expression_in_postfix_expression1354 = new BitSet(new long[]{0x0800068840000002L});
    public static final BitSet FOLLOW_59_in_postfix_expression1368 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_expression_in_postfix_expression1370 = new BitSet(new long[]{0x1000000000000000L});
    public static final BitSet FOLLOW_60_in_postfix_expression1372 = new BitSet(new long[]{0x0800068840000002L});
    public static final BitSet FOLLOW_30_in_postfix_expression1386 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_postfix_expression1388 = new BitSet(new long[]{0x0800068840000002L});
    public static final BitSet FOLLOW_30_in_postfix_expression1402 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_argument_expression_list_in_postfix_expression1404 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_postfix_expression1406 = new BitSet(new long[]{0x0800068840000002L});
    public static final BitSet FOLLOW_42_in_postfix_expression1420 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_IDENTIFIER_in_postfix_expression1422 = new BitSet(new long[]{0x0800068840000002L});
    public static final BitSet FOLLOW_41_in_postfix_expression1436 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_IDENTIFIER_in_postfix_expression1438 = new BitSet(new long[]{0x0800068840000002L});
    public static final BitSet FOLLOW_35_in_postfix_expression1452 = new BitSet(new long[]{0x0800068840000002L});
    public static final BitSet FOLLOW_39_in_postfix_expression1466 = new BitSet(new long[]{0x0800068840000002L});
    public static final BitSet FOLLOW_IDENTIFIER_in_primary_expression1524 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_constant_in_primary_expression1529 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_primary_expression1534 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_expression_in_primary_expression1536 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_primary_expression1538 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_assignment_expression_in_expression1614 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_37_in_expression1617 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_assignment_expression_in_expression1619 = new BitSet(new long[]{0x0000002000000002L});
    public static final BitSet FOLLOW_conditional_expression_in_constant_expression1632 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_lvalue_in_assignment_expression1643 = new BitSet(new long[]{0x4214211224000000L,0x0000000200000000L});
    public static final BitSet FOLLOW_assignment_operator_in_assignment_expression1645 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_assignment_expression_in_assignment_expression1647 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_conditional_expression_in_assignment_expression1652 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_unary_expression_in_lvalue1664 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_logical_or_expression_in_conditional_expression1736 = new BitSet(new long[]{0x0400000000000002L});
    public static final BitSet FOLLOW_58_in_conditional_expression1739 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_expression_in_conditional_expression1741 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46_in_conditional_expression1743 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_conditional_expression_in_conditional_expression1745 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_logical_and_expression_in_logical_or_expression1758 = new BitSet(new long[]{0x0000000000000002L,0x0000000400000000L});
    public static final BitSet FOLLOW_98_in_logical_or_expression1761 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_logical_and_expression_in_logical_or_expression1763 = new BitSet(new long[]{0x0000000000000002L,0x0000000400000000L});
    public static final BitSet FOLLOW_inclusive_or_expression_in_logical_and_expression1776 = new BitSet(new long[]{0x0000000008000002L});
    public static final BitSet FOLLOW_27_in_logical_and_expression1779 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_inclusive_or_expression_in_logical_and_expression1781 = new BitSet(new long[]{0x0000000008000002L});
    public static final BitSet FOLLOW_exclusive_or_expression_in_inclusive_or_expression1794 = new BitSet(new long[]{0x0000000000000002L,0x0000000100000000L});
    public static final BitSet FOLLOW_96_in_inclusive_or_expression1797 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_exclusive_or_expression_in_inclusive_or_expression1799 = new BitSet(new long[]{0x0000000000000002L,0x0000000100000000L});
    public static final BitSet FOLLOW_and_expression_in_exclusive_or_expression1812 = new BitSet(new long[]{0x2000000000000002L});
    public static final BitSet FOLLOW_61_in_exclusive_or_expression1815 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_and_expression_in_exclusive_or_expression1817 = new BitSet(new long[]{0x2000000000000002L});
    public static final BitSet FOLLOW_equality_expression_in_and_expression1830 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_28_in_and_expression1833 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_equality_expression_in_and_expression1835 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_relational_expression_in_equality_expression1847 = new BitSet(new long[]{0x0020000001000002L});
    public static final BitSet FOLLOW_set_in_equality_expression1850 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_relational_expression_in_equality_expression1856 = new BitSet(new long[]{0x0020000001000002L});
    public static final BitSet FOLLOW_shift_expression_in_relational_expression1869 = new BitSet(new long[]{0x00C9000000000002L});
    public static final BitSet FOLLOW_set_in_relational_expression1872 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_shift_expression_in_relational_expression1882 = new BitSet(new long[]{0x00C9000000000002L});
    public static final BitSet FOLLOW_additive_expression_in_shift_expression1895 = new BitSet(new long[]{0x0102000000000002L});
    public static final BitSet FOLLOW_set_in_shift_expression1898 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_additive_expression_in_shift_expression1904 = new BitSet(new long[]{0x0102000000000002L});
    public static final BitSet FOLLOW_labeled_statement_in_statement1920 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_compound_statement_in_statement1925 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expression_statement_in_statement1930 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_selection_statement_in_statement1935 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_iteration_statement_in_statement1940 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_jump_statement_in_statement1945 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTIFIER_in_labeled_statement1956 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46_in_labeled_statement1958 = new BitSet(new long[]{0x000080CD50942A50L,0x00000010C1247073L});
    public static final BitSet FOLLOW_statement_in_labeled_statement1960 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_65_in_labeled_statement1965 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_constant_expression_in_labeled_statement1967 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46_in_labeled_statement1969 = new BitSet(new long[]{0x000080CD50942A50L,0x00000010C1247073L});
    public static final BitSet FOLLOW_statement_in_labeled_statement1971 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_69_in_labeled_statement1976 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_46_in_labeled_statement1978 = new BitSet(new long[]{0x000080CD50942A50L,0x00000010C1247073L});
    public static final BitSet FOLLOW_statement_in_labeled_statement1980 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_95_in_compound_statement2002 = new BitSet(new long[]{0x800080CD50942A50L,0x00000018FFFFFEFFL});
    public static final BitSet FOLLOW_declaration_in_compound_statement2005 = new BitSet(new long[]{0x800080CD50942A50L,0x00000018FFFFFEFFL});
    public static final BitSet FOLLOW_statement_list_in_compound_statement2010 = new BitSet(new long[]{0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_99_in_compound_statement2013 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_statement_in_statement_list2024 = new BitSet(new long[]{0x000080CD50942A52L,0x00000010C1247073L});
    public static final BitSet FOLLOW_47_in_expression_statement2036 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expression_in_expression_statement2042 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_expression_statement2044 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_78_in_selection_statement2056 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_30_in_selection_statement2058 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_expression_in_selection_statement2060 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_selection_statement2062 = new BitSet(new long[]{0x000080CD50942A50L,0x00000010C1247073L});
    public static final BitSet FOLLOW_statement_in_selection_statement2064 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000100L});
    public static final BitSet FOLLOW_72_in_selection_statement2079 = new BitSet(new long[]{0x000080CD50942A50L,0x00000010C1247073L});
    public static final BitSet FOLLOW_statement_in_selection_statement2081 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_88_in_selection_statement2088 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_30_in_selection_statement2090 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_expression_in_selection_statement2092 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_selection_statement2094 = new BitSet(new long[]{0x000080CD50942A50L,0x00000010C1247073L});
    public static final BitSet FOLLOW_statement_in_selection_statement2096 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_94_in_iteration_statement2107 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_30_in_iteration_statement2109 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_expression_in_iteration_statement2111 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_iteration_statement2113 = new BitSet(new long[]{0x000080CD50942A50L,0x00000010C1247073L});
    public static final BitSet FOLLOW_statement_in_iteration_statement2115 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_70_in_iteration_statement2120 = new BitSet(new long[]{0x000080CD50942A50L,0x00000010C1247073L});
    public static final BitSet FOLLOW_statement_in_iteration_statement2122 = new BitSet(new long[]{0x0000000000000000L,0x0000000040000000L});
    public static final BitSet FOLLOW_94_in_iteration_statement2124 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_30_in_iteration_statement2126 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_expression_in_iteration_statement2128 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_iteration_statement2130 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_iteration_statement2132 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_76_in_iteration_statement2138 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_30_in_iteration_statement2140 = new BitSet(new long[]{0x000080CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_expression_statement_in_iteration_statement2142 = new BitSet(new long[]{0x000080CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_expression_statement_in_iteration_statement2144 = new BitSet(new long[]{0x000000CDD0942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_expression_in_iteration_statement2146 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_iteration_statement2149 = new BitSet(new long[]{0x000080CD50942A50L,0x00000010C1247073L});
    public static final BitSet FOLLOW_statement_in_iteration_statement2151 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_77_in_jump_statement2162 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_IDENTIFIER_in_jump_statement2164 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_jump_statement2166 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_68_in_jump_statement2172 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_jump_statement2174 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_64_in_jump_statement2180 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_jump_statement2182 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_82_in_jump_statement2188 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_jump_statement2190 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_82_in_jump_statement2196 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_expression_in_jump_statement2198 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_jump_statement2200 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_declaration_specifiers_in_synpred2_CSample172 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_declaration_specifiers_in_synpred4_CSample172 = new BitSet(new long[]{0x0000000140002000L});
    public static final BitSet FOLLOW_declarator_in_synpred4_CSample178 = new BitSet(new long[]{0x8000000000002000L,0x00000000BEDB8E8CL});
    public static final BitSet FOLLOW_declaration_in_synpred4_CSample180 = new BitSet(new long[]{0x8000000000002000L,0x00000000BEDB8E8CL});
    public static final BitSet FOLLOW_95_in_synpred4_CSample185 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_declaration_specifiers_in_synpred5_CSample230 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_declaration_specifiers_in_synpred8_CSample282 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_type_specifier_in_synpred12_CSample343 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_type_specifier_in_synpred35_CSample674 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_pointer_in_synpred45_CSample827 = new BitSet(new long[]{0x0000000040002000L});
    public static final BitSet FOLLOW_direct_declarator_in_synpred45_CSample830 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_declarator_suffix_in_synpred47_CSample879 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_synpred50_CSample919 = new BitSet(new long[]{0x8000000000002000L,0x000000003CDB8E8CL});
    public static final BitSet FOLLOW_parameter_type_list_in_synpred50_CSample921 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_synpred50_CSample923 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_synpred51_CSample933 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_identifier_list_in_synpred51_CSample935 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_synpred51_CSample937 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_type_qualifier_in_synpred52_CSample962 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_pointer_in_synpred53_CSample965 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_synpred54_CSample960 = new BitSet(new long[]{0x0000000000000000L,0x0000000020000008L});
    public static final BitSet FOLLOW_type_qualifier_in_synpred54_CSample962 = new BitSet(new long[]{0x0000000100000002L,0x0000000020000008L});
    public static final BitSet FOLLOW_pointer_in_synpred54_CSample965 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_synpred55_CSample971 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_pointer_in_synpred55_CSample973 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_declarator_in_synpred58_CSample1028 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_abstract_declarator_in_synpred59_CSample1030 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_direct_abstract_declarator_in_synpred62_CSample1077 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_abstract_declarator_suffix_in_synpred65_CSample1108 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_synpred78_CSample1280 = new BitSet(new long[]{0x0000000000002000L,0x000000003C998A8CL});
    public static final BitSet FOLLOW_type_name_in_synpred78_CSample1282 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_31_in_synpred78_CSample1284 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_cast_expression_in_synpred78_CSample1286 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_85_in_synpred83_CSample1328 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_unary_expression_in_synpred83_CSample1330 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_lvalue_in_synpred104_CSample1643 = new BitSet(new long[]{0x4214211224000000L,0x0000000200000000L});
    public static final BitSet FOLLOW_assignment_operator_in_synpred104_CSample1645 = new BitSet(new long[]{0x000000CD50942A50L,0x0000001000200000L});
    public static final BitSet FOLLOW_assignment_expression_in_synpred104_CSample1647 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_declaration_in_synpred136_CSample2005 = new BitSet(new long[]{0x0000000000000002L});

}