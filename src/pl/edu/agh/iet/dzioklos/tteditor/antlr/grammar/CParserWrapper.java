package pl.edu.agh.iet.dzioklos.tteditor.antlr.grammar;

import java.util.ArrayList;
import java.util.List;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.TokenStream;
import org.antlr.runtime.tree.BaseTree;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;

import pl.edu.agh.iet.dzioklos.tteditor.antlr.grammar.CSampleParser.program_return;
import pl.edu.agh.iet.dzioklos.tteditor.antlr.grammar.CSampleParser.translation_unit_return;
import pl.edu.agh.iet.dzioklos.tteditor.beans.CElement;
import pl.edu.agh.iet.dzioklos.tteditor.beans.CElement.Type;
import pl.edu.agh.iet.dzioklos.tteditor.beans.CError;

public class CParserWrapper {

	private String input;
	private List<CElement> elements;

	private static List<String> tokens = null;

	public static List<String> getKeywords() {
		if (tokens == null) {
			tokens = new ArrayList<String>();
			CSampleLexer lexer = new CSampleLexer();
			TokenStream tokenStream = new CommonTokenStream(lexer);
			CSampleParser parser = new CSampleParser(tokenStream);

			for (String s : parser.getTokenNames()) {
				if (s.matches("'[a-z]*'")) {
					String token = s.substring(1, s.length() - 1);
					tokens.add(token);
				}
			}
		}

		return tokens;
	}

	public CParserWrapper(String input) {
		this.input = input;
	}

	public void parse() throws ExtendedRecognitionException {
//		System.out.println("PARSE: =============== " + input.length());
//		System.out.println(input);
//		System.out.println("END: ------------------");
		
		CharStream stream = new ANTLRStringStream(input);
		//
		CSampleLexer lexer = new CSampleLexer(stream);
		TokenStream tokenStream = new CommonTokenStream(lexer);
		CSampleParser parser = new CSampleParser(tokenStream);

		try {
			program_return eval = parser.program();

//			CommonTree tree = (CommonTree) eval.getTree();
//			addGlobalVariables(tree, parser);

			// System.out.println("ok: " + tree.toStringTree());
//			 printTree(tree, 1);
			// System.out.println("TE: " + tree.getText());
			//
			// for (int i = 0; i < tree.getChildCount(); i++) {
			// Tree child = tree.getChild(i);
			// System.out.println("  " + child.getType() + " text:"
			// + child.getText() + "   "
			// + parser.getTokenNames()[child.getType()]
			// + ", " );
			// // System.out.println(tree.getChild(i).toStringTree());
			//
			// }

			// DOTTreeGenerator gen = new DOTTreeGenerator();
			// StringTemplate st = gen.toDOT(tree);
			// System.out.println(st);

			List<CError> errorsList = parser.getErrorsList();
			if (errorsList.size() > 0) {
				throw new ExtendedRecognitionException(errorsList);
			}
			parser.addComments(lexer.getComments());
			elements = parser.getCElements();

		} catch (RecognitionException e) {
			String hdr = parser.getErrorHeader(e);
			String[] splits = hdr.split(" |:");
			int lineNo = Integer.parseInt(splits[1]);
			int columnNo = Integer.parseInt(splits[2]);
			String msg = "Error when parsing, fix your input pease.";
			CError error = new CError(lineNo, columnNo, msg);
			ExtendedRecognitionException exc = new ExtendedRecognitionException(
					error);
			throw exc;
		}
	}

	private void addGlobalVariables(CommonTree tree, CSampleParser parser) {
		if (tree == null){
			return;
		}
		for (int i = 0; i < tree.getChildCount(); i++) {
			try {
				Tree child = tree.getChild(i);
				String name = child.getText();
				Tree child2 = child.getChild(0);
				String type;
				if (child2 != null){
					type = child2.getText();
				}else{
					Tree parent = child.getParent();
					type = parent.getText();
				}
				
				
				int line = child.getLine();
				parser.addGlobalVariable(name, type, line);
			} catch (NullPointerException e) {
				System.out.println("Null in addGlobalVariable, " + e.getMessage());
			}
		}
	}

	public List<CElement> getCElements() {
		return elements;
	}

	public void printTree(CommonTree t, int indent) {
		if (t != null) {
			StringBuffer sb = new StringBuffer(indent);

			// if (t.getParent() == null){
			// System.out.println(sb.toString() + t.getText().toString());
			// }
			for (int i = 0; i < indent; i++)
				sb = sb.append("   ");
			for (int i = 0; i < t.getChildCount(); i++) {
				System.out.println(sb.toString() + t.getChild(i).toString());
//				printTree((CommonTree) t.getChild(i), indent + 1);
			}
		}
	}

}
