package pl.edu.agh.iet.dzioklos.tteditor.antlr.sample;

import java.util.List;

import org.antlr.runtime.RecognitionException;

import pl.edu.agh.iet.dzioklos.tteditor.antlr.grammar.CParserWrapper;
import pl.edu.agh.iet.dzioklos.tteditor.antlr.grammar.ExtendedRecognitionException;
import pl.edu.agh.iet.dzioklos.tteditor.beans.CElement;

public class MainParserSample {

	private CParserWrapper parser;

//	public static void main(String[] args) {
//		MainParserSample sample = new MainParserSample();
//		// sample.test("int a;\n int b;\n void foo(){}\nint bar(float x, float y){}\n");
//		// sample.test("int a;\n //intaaa b;\n void foo(){}\n//int bar(float x, float y){}\n");
//		// sample.test("int bar(float x, float y){\na()\nb()\n}\n");
//		// sample.test("int a;\n intaaa b;\n void foo(){}\nint bar(float x, float y){}\n");
//		// sample.getKeywords();
////		sample.test("int a;\n");
////		sample.test("int a;\nfloat b;\n");
//		// sample.test("int a;\n" +
//		// "int main(){\n" +
//		// "int b;}\n");


	private void getKeywords() {
		List<String> keywords = CParserWrapper.getKeywords();
		for (String s : keywords) {
			System.out.println(s);
		}
	}

	private void test(String input) {
		System.out.println("===================================");
		System.out.println(input);
		parser = new CParserWrapper(input);
		try {
			parser.parse();
			System.out.println("parsing done");

			List<CElement> elements = parser.getCElements();
			printCElements(elements);
		} catch (ExtendedRecognitionException e) {
			System.out.println("Error when parsing");
			System.out.println("Line: " + e.getLineNo());
			System.out.println("Column: " + e.getColumnNo());
			System.out.println("Msg: " + e.getParserMsg());
		}
	}

	private void printCElements(List<CElement> elements) {
		System.out.println("Printing CElements (total: " + elements.size()
				+ "):");
		for (CElement e : elements) {
			System.out.println("===== elem:");
			System.out.println("type: " + e.getType());
			System.out.println("name: " + e.getName());
			System.out.println("line: " + e.getLineNo());
			switch (e.getType()) {
			case DEFINE_SATEMENT: {

			}
			case FUNCTION: {
				System.out.println("return type: " + e.getReturnType());
				break;
			}
			case GLOBAL_VARIABLE: {
				System.out.println("type: " + e.getReturnType());
				break;
			}
			case STRUCT: {
				for (String key : e.getParamsAndTypes().keySet()){
					System.out.println("   " + key + " : " + e.getParamsAndTypes().get(key));
				}
				break;
			}
			}
		}

	}

}
