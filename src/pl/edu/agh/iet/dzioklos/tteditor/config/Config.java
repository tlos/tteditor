package pl.edu.agh.iet.dzioklos.tteditor.config;

import org.eclipse.swt.SWT;

public class Config {

	protected static String LOCALE_FILE 	= "localeEN.xml";
	protected static String VERSION			= "0.9";
	
	
	protected static int			 	UNDO_REDO_STACK_SIZE = 100; /* if <=0 infinity stack */ 

	protected final static String[] 	DEFAULT_FILES_EXTENSIONS =  new String[]{"*.c","*.*"};
	
	protected static boolean 		 	SHOW_LINE_NUMBERS = true;
	protected static int 				LINE_NUMBERS_NUBER_COUNT = 3;
	
	
	protected static int 				DAO_BUFFER_SIZE = 4192;
	protected static int 				THREADS_IN_POOL= 10;
	
	protected static String 			COMPILEDFILE_EXT = ".exe";
	protected static String 			COMPILATOR_PATH = "gcc";
	protected static String 			COMPILATOR_CFLAGS = "-Wall";
	protected static String 			COMPILATOR_LFLAGS = "";
	protected static String 			TAB_SEQUENCE = "\t"; // can change to spaces TODO: not work properiaty
	
	
	protected static int 				CONSOLE_WIDTH = 600;
	protected static int 				CONSOLE_HEIGH = 100;
	protected static int 				CONSOLE_BACKGROUND = SWT.COLOR_BLACK;
	protected static int 				CONSOLE_FOREGROUND = SWT.COLOR_GREEN;
	protected static String 			CONSOLE_FONT = "Consolas";
	
	
	protected static String 			MAIN_ICON = "emacs.png";
	
	protected static int 				CKEYWORDS_COLOR =  SWT.COLOR_DARK_RED;
	protected static int 				PREPROCESOR_KEYWORDS_COLOR = SWT.COLOR_BLUE;
	protected static int 				COMMENT_COLOR = SWT.COLOR_DARK_GRAY;
	
	protected static int 				EDITOR_BACKGROUND_COLOR = SWT.COLOR_GRAY;
	protected static int 				EDITOR_FONT_COLOR =  SWT.COLOR_BLACK;
	protected static String 			EDITOR_FONT = "Consolas";
	protected static int 				EDITOR_FONT_SIZE = 10;
	protected static int 				EDITOR_LINENO_BACKGROUND = SWT.COLOR_WHITE;
	protected static int				EDITOR_ERRORS_BACKGROUND_COLOR = SWT.COLOR_YELLOW;	
	

	protected static int 				COMPLETITION_POPUP_BACKGROUND = SWT.COLOR_DARK_CYAN; 	
	
	
	protected static int 				TREE_WIDTH = 180;
	protected static int 				TREE_BACKGROUND = SWT.COLOR_GRAY;
	protected static int 				TREE_FUNCTION_COLOR = SWT.COLOR_DARK_BLUE;	
	protected static int 				TREE_STRUCT_COLOR = SWT.COLOR_DARK_RED;	
	protected static int 				TREE_GLOBALVAR_COLOR = SWT.COLOR_DARK_GREEN;	
	protected static int 				TREE_DEFINE_COLOR = SWT.COLOR_DARK_GREEN;	
	protected static int 				SIZE_X = 900;
	protected static int 				SIZE_Y = 800; 
	
	
	protected static long 				AUTOPARSE_TIMER_INTERVAL_MSECS = 500; /*0,5sek*/	
	protected static char[] 			AUTOPARSE_TRIGGERING_CHARS = new char[]{';','{'};	
	
	
	
	
	
	

	public static String GET_LOCALE_FILE() {						return LOCALE_FILE;} 
	
	public static String VERSION(){ 								return VERSION; }
	
	public static int GET_UNDO_REDO_STACK_SIZE(){					return UNDO_REDO_STACK_SIZE; }
	
	public static String[] DEFAULT_FILES_EXTENSIONS() {				return DEFAULT_FILES_EXTENSIONS;}
	
	public static boolean SHOW_LINE_NUMBERS() {						return SHOW_LINE_NUMBERS;}
	public static int GET_LINE_NUMBERS_NUBER_COUNT() {				return LINE_NUMBERS_NUBER_COUNT;}
	
	
	public static int DAO_BUFFER_SIZE() {							return DAO_BUFFER_SIZE;}
	public static int THREADS_IN_POOL() {							return THREADS_IN_POOL;	}
	
	public static String GET_COMPILEDFILE_EXT() {					return COMPILEDFILE_EXT;}
	public static String GET_COMPILATOR_PATH(){						return COMPILATOR_PATH;}
	public static String GET_COMPILATOR_CFLAGS(){					return COMPILATOR_CFLAGS;}
	public static String GET_COMPILATOR_LFLAGS(){					return COMPILATOR_LFLAGS;}
	public static String GET_TAB_SEQUENCE(){						return TAB_SEQUENCE;} 
	
	
	public static int GET_CONSOLE_WIDTH() {							return CONSOLE_WIDTH;}
	public static int GET_CONSOLE_HEIGH() {							return CONSOLE_HEIGH;}
	public static int GET_CONSOLE_BACKGROUND() {					return CONSOLE_BACKGROUND;}
	public static int GET_CONSOLE_FOREGROUND() {					return CONSOLE_FOREGROUND;}
	public static String GET_CONSOLE_FONT() {						return CONSOLE_FONT;}
	
	
	public static String GET_MAIN_ICON(){							return MAIN_ICON;}
	
	
	
	public static int GET_CKEYWORDS_COLOR() {						return CKEYWORDS_COLOR;}
	public static int GET_PREPROCESOR_KEYWORDS_COLOR() {			return PREPROCESOR_KEYWORDS_COLOR;}
	public static int GET_COMMENT_COLOR() {							return COMMENT_COLOR;}
	
	public static int GET_EDITOR_BACKGROUND_COLOR(){				return EDITOR_BACKGROUND_COLOR;}
	public static int GET_EDITOR_FONT_COLOR(){						return EDITOR_FONT_COLOR;}
	public static String GET_EDITOR_FONT() {						return EDITOR_FONT;}
	public static int GET_EDITOR_FONT_SIZE() {						return EDITOR_FONT_SIZE;}
	public static int GET_EDITOR_LINENO_BACKGROUND() {				return EDITOR_LINENO_BACKGROUND;}
	public static int GET_EDITOR_ERRORS_BACKGROUND_COLOR(){			return EDITOR_ERRORS_BACKGROUND_COLOR;	}
	

	public static int GET_COMPLETITION_POPUP_BACKGROUND() {			return COMPLETITION_POPUP_BACKGROUND; 	}
	
	
	public static int GET_TREE_WIDTH() {							return TREE_WIDTH;}
	public static int GET_TREE_BACKGROUND(){ 						return TREE_BACKGROUND;}
	public static int GET_TREE_FUNCTION_COLOR() {					return TREE_FUNCTION_COLOR;	}
	public static int GET_TREE_STRUCT_COLOR() {						return TREE_STRUCT_COLOR;	}
	public static int GET_TREE_GLOBALVAR_COLOR() {					return TREE_GLOBALVAR_COLOR;	}
	public static int GET_TREE_DEFINE_COLOR() {						return TREE_DEFINE_COLOR;	}
	public static int SIZE_X() {									return SIZE_X;	}
	public static int SIZE_Y() {									return SIZE_Y; }
	
	
	public static long GET_AUTOPARSE_TIMER_INTERVAL_MSECS() {		return AUTOPARSE_TIMER_INTERVAL_MSECS;}
	public static char[] GET_AUTOPARSE_TRIGGERING_CHARS() {			return AUTOPARSE_TRIGGERING_CHARS;	}


	public static String getSampleStartText(){
		return "/* \n" + 
				"sample c - program\n" + 
				"@ Tomasz �o�\n" + 
				"@ Tomasz Dziok\n" + 
				"TK2 - projekt, 2012/2013\n" + 
				"*/\n" + 
				"\n" + 
				"// some info about sample structure\n" + 
				"\n" + 
				"struct str1{\n" + 
				"	int s1_int1;\n" + 
				"	int s1_int2;\n" + 
				"	char str1_char;\n" + 
				"	\n" + 
				"};\n" + 
				"\n" + 
				"struct str2{\n" + 
				"	float s2_float1;\n" + 
				"		\n" + 
				"};\n" + 
				"\n" + 
				"int global;\n" + 
				"float global2 = 9;\n" + 
				"\n" + 
				"void foo(){\n" + 
				"	int local1;\n" + 
				"	float local2=9.9;\n" + 
				"}\n" + 
				"\n" + 
				"float bar(){\n" + 
				"	return 8.9;	\n" + 
				"}\n" + 
				"\n" + 
				"int main(char** args){\n" + 
				"	str2.s2_float1;\n" + 
				"	\n" + 
				"}\n" + 
				"";
	}
	
	
	
}
