package pl.edu.agh.iet.dzioklos.tteditor.gui;

import static pl.edu.agh.iet.dzioklos.tteditor.utils.GuiTextUtils.guiText;




import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import pl.edu.agh.iet.dzioklos.tteditor.beans.CElement;
import pl.edu.agh.iet.dzioklos.tteditor.beans.CElement.Type;
import pl.edu.agh.iet.dzioklos.tteditor.config.Config;

public class TreeComposite extends TTComposite {
	private static final Logger log = Logger.getLogger(TreeComposite.class);
	private Tree tree;
	private CEditorComposite cEditorComposite;
	private List<CElement> elements = new LinkedList<CElement>();
	private Map<TreeItem, CElement> treeItemsElements = new HashMap<TreeItem, CElement>();
	
	public TreeComposite(Composite arg0, int arg1, CEditorComposite cEditorComposite) {
		super(arg0, arg1);
		this.cEditorComposite = cEditorComposite;
		init();
	}

	/**
	 * ThreadSafe
	 */
	public void updateTree() {
		Runnable action = new Runnable() {
			@Override
			public void run() {
				treeItemsElements.clear();
				tree.removeAll();
				__initItemsForChannelTree();
			}
		};
		performGuiAsyncActionOnCorrectThread(action);
	}

	/**
	 * ThreadSafe
	 * 
	 * @param elements
	 *            - CElements list to be shown in tree
	 */
	public void setCElementsAndRefreshTree(List<CElement> elements) {
		setCElements(elements);
		updateTree();
	}

	public synchronized void setCElements(List<CElement> elements) {
		this.elements = elements;
	}

	private void init() {
		this.setLayout(new GridLayout(2, false));
		Label label = new Label(this, SWT.BORDER);
		label.setText(guiText("TreeComposite.label"));
		GridData gridData = new GridData(GridData.BEGINNING,
				GridData.BEGINNING, false, false, 1, 1);
		label.setLayoutData(gridData);

		tree = new Tree(this, SWT.V_SCROLL);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1);
		gridData.widthHint = Config.GET_TREE_WIDTH();
		tree.setLayoutData(gridData);

		
		tree.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseUp(MouseEvent arg0) {}
			
			@Override
			public void mouseDown(MouseEvent arg0) {}			
			@Override
			public void mouseDoubleClick(MouseEvent arg0) {
				if( tree.getSelection().length != 1 ) return;
				TreeItem item = tree.getSelection()[0];
				while( item.getParentItem() != null)
					item = item.getParentItem();
				CElement element = treeItemsElements.get(item);
				if( element !=null){
					log.info("Clicked on element: " + element.getName());
					cEditorComposite.goToAndSelectLine(element.getLineNo());
				}
			}
		});
		
		//initTestData();

		
		

		__initItemsForChannelTree();
	}

	protected void initTestData() {
		elements.add(new CElement().setLineNo(1).setName("main")
				.setType(Type.FUNCTION).setReturnType("int")
				.addParam("a", "char*"));
	}

	private synchronized void __initItemsForChannelTree() {
		
		for (CElement el : elements) {
			if( el.getType() == Type.COMMENT ) continue;
			TreeItem item = new TreeItem(tree, SWT.NONE);
			treeItemsElements.put(item, el);
			switch (el.getType()) {
			case FUNCTION:
				for (Map.Entry<String, String> param : el.getParamsAndTypes()
						.entrySet()) {
					if (param.getValue().equals("void"))
						continue;
					new TreeItem(item, SWT.NONE).setText("+" + param.getKey()
							+ ": " + param.getValue());
				}
				item.setText("FUN " + el.getName() + "->" + el.getReturnType());
				item.setForeground(getDisplay().getSystemColor(Config.GET_TREE_FUNCTION_COLOR()));
				break;
			case DEFINE_SATEMENT:
				item.setText("DEFINE " + el.getName());
				item.setForeground(getDisplay().getSystemColor(Config.GET_TREE_DEFINE_COLOR()));
				break;
			case STRUCT:
				item.setText("struct " + el.getName());
				for (Map.Entry<String, String> param : el.getParamsAndTypes()
						.entrySet()) {
					new TreeItem(item, SWT.NONE).setText(param.getKey() + ":\t"
							+ param.getValue());
				}
				item.setForeground(getDisplay().getSystemColor(Config.GET_TREE_STRUCT_COLOR()));
				break;
			case GLOBAL_VARIABLE:
				item.setText(el.getName() + " : " + el.getReturnType());
				break;
			case COMMENT:
				break;
			default:
				break;

			}
			item.setExpanded(true);
		}
	}
}
