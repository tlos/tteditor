package pl.edu.agh.iet.dzioklos.tteditor.gui;

import org.eclipse.swt.widgets.Composite;

public abstract class TTComposite extends Composite {

	public TTComposite(Composite arg0, int arg1) {
		super(arg0, arg1);
	}

	/**
	 * guarantee that the action is invoked on ui thread
	 * 
	 * @param action - action changing gui
	 * 
	 */
	public void performGuiAsyncActionOnCorrectThread(Runnable action) {
		if( getDisplay().getThread() == Thread.currentThread() )
			action.run();
		else
			getDisplay().asyncExec(action);
	}
	/**
	 * guarantee that the action is invoked on ui thread
	 * 
	 * @param action - action changing gui
	 * 
	 */
	public void performGuiSyncActionOnCorrectThread(Runnable action) {
		if( getDisplay().getThread() == Thread.currentThread() )
			action.run();
		else
			getDisplay().syncExec(action);
	}

}