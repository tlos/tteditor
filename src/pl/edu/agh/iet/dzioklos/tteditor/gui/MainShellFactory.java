package pl.edu.agh.iet.dzioklos.tteditor.gui;

import static pl.edu.agh.iet.dzioklos.tteditor.utils.GuiTextUtils.guiText;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import pl.edu.agh.iet.dzioklos.tteditor.config.Config;
import pl.edu.agh.iet.dzioklos.tteditor.utils.CCompilerUtils;
import pl.edu.agh.iet.dzioklos.tteditor.utils.CParserUtils;
import pl.edu.agh.iet.dzioklos.tteditor.utils.DocumentDao;
import pl.edu.agh.iet.dzioklos.tteditor.utils.WorkersHolder;

public class MainShellFactory {

	private static final Logger log = Logger.getLogger(MainShellFactory.class);

	private TreeComposite treeComposite;
	private ConsoleComposite consoleComposite;
	private CEditorComposite cEditorComposite;
	private Shell shell;
	private String loadedFileName = null;
	private Object loadedFileNameLock = new Object();
	private Menu menuBar;

	private CParserUtils cParserUtils;

	public MainShellFactory(Display d) {
		shell = new Shell(d);
		init();
		composeMenu();
	}

	public Shell getComposedShell() {
		return shell;
	}

	public CEditorComposite getcEditorComposite() {
		return cEditorComposite;
	}

	public ConsoleComposite getConsoleComposite() {
		return consoleComposite;
	}

	public void openFile(final String fileName) {
		
		if (cEditorComposite.isDirty()) {
			int style = SWT.ICON_QUESTION | SWT.YES | SWT.NO | SWT.CANCEL;
			// String text = cEditorComposite.getText();
			MessageBox messageBox = new MessageBox(shell, style);
			messageBox
					.setMessage(guiText("MainShellFactory.closeConfirmDialogMsg"));
			int rc = messageBox.open();
			switch (rc) {
			case SWT.NO:
				log.info("No saving");
				break;
			case SWT.YES:
				log.info("Saving...");
				saveFile(false);
				break;
			case SWT.CANCEL:
				log.info("Staying...");
				return;
			}
		}
	
		
		
		WorkersHolder.THREAD_POOL.execute(new Runnable() {

			@Override
			public void run() {
				try {
					cEditorComposite.setText(DocumentDao.loadDocument(fileName));
					synchronized (loadedFileNameLock) {
						// loadedFileName = fileName;
						setLoadedFileName(fileName);
					}
					cEditorComposite.setDirty(false);
				} catch (IOException e) {
					log.error("faied with loading file", e);
					consoleComposite.setText("Error: " + e.getMessage());
					return;
				}
			}
		});

	}

	private void init() {
		shell.setText(guiText("Shell.title"));
		// two columns, not same size
		shell.setLayout(new GridLayout(2, false));

		org.eclipse.swt.graphics.Image mainIcon = new Image(shell.getDisplay(),
				Config.GET_MAIN_ICON());
		shell.setImage(mainIcon);

		// extends 1 column, greedy :)
		cEditorComposite = new CEditorComposite(shell, SWT.BORDER);
		GridData gridData = new GridData(GridData.FILL, GridData.FILL, true,
				true, 1, 1);

		cEditorComposite.setLayoutData(gridData);

		treeComposite = new TreeComposite(shell, SWT.BORDER, cEditorComposite);
		gridData = new GridData(GridData.FILL, GridData.FILL, false, true, 1, 1);
		treeComposite.setLayoutData(gridData);

		consoleComposite = new ConsoleComposite(shell, SWT.NONE);
		gridData = new GridData(GridData.FILL, GridData.FILL, true, false, 2, 1);
		consoleComposite.setLayoutData(gridData);
		shell.setSize(Config.SIZE_X(), Config.SIZE_Y());

		cParserUtils = CParserUtils.initialize(consoleComposite, treeComposite,
				cEditorComposite);

		shell.addListener(SWT.Close, new Listener() {
			@Override
			public void handleEvent(Event arg0) {
				if (cEditorComposite.isDirty()) {
					int style = SWT.ICON_QUESTION | SWT.YES | SWT.NO | SWT.CANCEL;
					// String text = cEditorComposite.getText();
					MessageBox messageBox = new MessageBox(shell, style);
					messageBox
							.setMessage(guiText("MainShellFactory.closeConfirmDialogMsg"));
					int rc = messageBox.open();
					switch (rc) {
					case SWT.NO:
						log.info("No saving");
						break;
					case SWT.YES:
						log.info("Saving...");
						saveFile(false);
						break;
					case SWT.CANCEL:
						log.info("Staying...");
						arg0.doit = false;
						return;
					}
				}

				// CParserUtils.getInstance().stopTimer();
				WorkersHolder.THREAD_POOL.shutdownNow();

				try {
					while (!WorkersHolder.THREAD_POOL.isTerminated())
						WorkersHolder.THREAD_POOL.awaitTermination(10L,
								TimeUnit.SECONDS);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});

		CParserUtils.getInstance().addAutoParseLogic(
				cEditorComposite.getStyledText());
	}

	private void composeMenu() {
		menuBar = new Menu(shell, SWT.BAR);
		shell.setMenuBar(menuBar);

		prepareMenuFile();
		prepareMenuProject();
		prepareMenuDEV();
	}

	private void prepareMenuDEV() {
		// TODO: Hide !
		// Developers menu, to hide in release

		MenuItem devMenu = new MenuItem(menuBar, SWT.CASCADE);

		devMenu.setText(guiText("MainShell.menuDev"));

		Menu fileItem = new Menu(shell, SWT.DROP_DOWN);
		devMenu.setMenu(fileItem);
//
//		MenuItem checkLineSelection = new MenuItem(fileItem, SWT.NONE);
//		checkLineSelection.setText("Select line test");
//		checkLineSelection.addSelectionListener(new SelectionAdapter() {
//			@Override
//			public void widgetSelected(SelectionEvent arg0) {
//
//				cEditorComposite.goToAndSelectLine(1);
//			}
//		});

		MenuItem runParserItem = new MenuItem(fileItem, SWT.NONE);
		runParserItem.setText("Run parser");
		runParserItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				log.info("Project->Parse");
				parseProject();
			}
		});
		runParserItem.setAccelerator(SWT.MOD1 + 'P');
	}

	private void prepareMenuProject() {

		MenuItem projectMenu = new MenuItem(menuBar, SWT.CASCADE);

		projectMenu.setText(guiText("MainShell.menuProject"));

		Menu projectMenuItem = new Menu(shell, SWT.DROP_DOWN);
		projectMenu.setMenu(projectMenuItem);

		MenuItem buildItem = new MenuItem(projectMenuItem, SWT.NONE);
		buildItem.setText(guiText("MainShell.menuProject.build") + " \tCtrl+B");
		buildItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				log.info("Project->Build");
				buildProject();
			}
		});
		buildItem.setAccelerator(SWT.MOD1 + 'B');
	}

	private void prepareMenuFile() {

		MenuItem fileMenu = new MenuItem(menuBar, SWT.CASCADE);

		fileMenu.setText(guiText("MainShell.menuFile"));

		Menu fileItem = new Menu(shell, SWT.DROP_DOWN);
		fileMenu.setMenu(fileItem);

		MenuItem openFileItem = new MenuItem(fileItem, SWT.NONE);
		openFileItem.setText(guiText("MainShell.menuFile.open") + " \tCtrl+O");
		openFileItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				log.debug("File->open");
				openFile();
			}
		});
		openFileItem.setAccelerator(SWT.MOD1 + 'O');

		MenuItem saveFileItem = new MenuItem(fileItem, SWT.NONE);
		saveFileItem.setText(guiText("MainShell.menuFile.save") + " \tCtrl+S");
		saveFileItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				log.debug("File->save");
				saveFile(false);
			}
		});
		saveFileItem.setAccelerator(SWT.MOD1 + 'S');

		MenuItem saveAsFileItem = new MenuItem(fileItem, SWT.NONE);
		saveAsFileItem.setText(guiText("MainShell.menuFile.saveAs"));
		saveAsFileItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				log.debug("File->saveAs");
				saveFile(true);
			}
		});

		MenuItem closeFileFileItem = new MenuItem(fileItem, SWT.NONE);
		closeFileFileItem.setText(guiText("MainShell.menuFile.closeFile")
				+ " \tCtrl+W");
		closeFileFileItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				log.debug("File->closeFile");
				closeFile();
			}
		});
		closeFileFileItem.setAccelerator(SWT.MOD1 + 'W');

		new MenuItem(fileItem, SWT.SEPARATOR);

		MenuItem exitFileItem = new MenuItem(fileItem, SWT.NONE);
		exitFileItem.setText(guiText("MainShell.menuFile.exit") + " \tCtrl+X");
		exitFileItem.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				log.debug("File->Exit clicked!");
				exit();
			}
		});
		exitFileItem.setAccelerator(SWT.MOD1 + 'X');

	}

	protected void closeFile() {
		synchronized (loadedFileNameLock) {
			// loadedFileName = null;
			setLoadedFileName(null);
		}
		cEditorComposite.setText("");
	}

	public boolean saveFile(boolean showDialog) {

		String fileName = null;

		synchronized (loadedFileNameLock) {
			if (!showDialog && !cEditorComposite.isDirty()) {
				log.info("No need to save, text not mofidied");
				return false;
			}
		}

		if (!showDialog && loadedFileName != null) {
			synchronized (loadedFileNameLock) {
				fileName = loadedFileName;
			}
		} else {
			FileDialog dialog = new FileDialog(shell, SWT.SAVE);
			dialog.setFilterExtensions(Config.DEFAULT_FILES_EXTENSIONS());
			synchronized (loadedFileNameLock) {
				if (loadedFileName != null && !loadedFileName.isEmpty())
					dialog.setFileName(loadedFileName);
			}
			fileName = dialog.open();
			if (fileName == null || fileName.isEmpty()) {
				log.info("No file selected");
				return false;
			}
		}
		saveFile(fileName);
		return true;
	}

	protected void saveFile(final String __fileName) {
		final String text = cEditorComposite.getText();
		WorkersHolder.THREAD_POOL.execute(new Runnable() {

			@Override
			public void run() {
				try {
					DocumentDao.saveDocument(__fileName, text);
					synchronized (loadedFileNameLock) {
						// loadedFileName = __fileName;
						setLoadedFileName(__fileName);
					}
					cEditorComposite.setDirty(false);
				} catch (IOException e) {
					log.error("faied with saving file", e);
					consoleComposite.setText(guiText("Console.error") + " "
							+ e.getMessage());
					return;
				}
			}
		});
	}

	protected void openFile() {

		FileDialog dialog = new FileDialog(shell, SWT.OPEN);
		dialog.setFilterExtensions(Config.DEFAULT_FILES_EXTENSIONS());

		final String fileName = dialog.open();
		if (fileName == null || fileName.isEmpty()) {
			log.info("No file selected");
			return;
		}
		openFile(fileName);
	}

	protected void buildProject() {
		saveFile(false);
		WorkersHolder.THREAD_POOL.execute(new Runnable() {
			@Override
			public void run() {
				synchronized (loadedFileNameLock) {
					if (loadedFileName == null || loadedFileName.isEmpty()) {
						log.warn("File not saved, breaking compilation");
						return;
					}
					String outFileName = loadedFileName.replace(".c",
							Config.GET_COMPILEDFILE_EXT());
					try {
						String out = CCompilerUtils.compile(loadedFileName,
								outFileName);
						consoleComposite
								.setText(guiText("Console.compilationSuccess")
										+ "\n" + out);
					} catch (Exception e) {
						log.error("Compilation error", e);
						consoleComposite
								.setText(guiText("Console.compilationError")
										+ "\n" + e.getMessage());
						return;
					}
				}
			}
		});

	}

	protected void parseProject() {
		cParserUtils.parseAndRefreshGuiInBackground();

	}

	private void setLoadedFileName(String loadedFileName) {
		this.loadedFileName = loadedFileName;

		updateTitle();
	}

	private void updateTitle() {
		Runnable action = new Runnable() {
			@Override
			public void run() {
				if (!shell.isDisposed()) {
					shell.setText(guiText("Shell.title")
							+ (loadedFileName == null ? "" : "   "
									+ loadedFileName));
					shell.redraw();
				}

			}
		};
		if (shell.getDisplay().getThread() == Thread.currentThread()) {
			action.run();
		} else {
			shell.getDisplay().asyncExec(action);
		}
	}

	public void exit() {

		log.debug("Exiting--------");

		// WorkersHolder.THREAD_POOL.shutdown();
		// try {
		// WorkersHolder.THREAD_POOL.awaitTermination(1, TimeUnit.SECONDS);
		// } catch (InterruptedException e) {
		// e.printStackTrace();
		// }
		if (!shell.isDisposed())
			shell.dispose();
	}
}
