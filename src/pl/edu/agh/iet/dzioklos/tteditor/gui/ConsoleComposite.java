package pl.edu.agh.iet.dzioklos.tteditor.gui;

import static pl.edu.agh.iet.dzioklos.tteditor.utils.GuiTextUtils.guiText;

import org.apache.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import pl.edu.agh.iet.dzioklos.tteditor.config.Config;

/**
 * Composite with log console implementation
 * 
 * 
 * @author Tomek Los
 * 
 */
public class ConsoleComposite extends TTComposite {
	private static final Logger log = Logger.getLogger(ConsoleComposite.class);
	private Text textComp;
	private Button cleanBtn;

	/**
	 * Thread safe
	 */
	public void clearConsole() {
		__setText("");
	}

	/**
	 * Thread safe
	 */
	public void setText(String text) {
		__setText(text);
	}

	/**
	 * Thread safe
	 */
	public void appendText(String text) {
		appendTextWithoutNewLine(text + "\n");
	}

	/**
	 * Thread safe
	 */
	public void appendTextWithoutNewLine(String text) {
		__appendText(text);
	}

	public ConsoleComposite(Composite arg0, int arg1) {
		super(arg0, arg1);
		init();
	}

	private void init() {
		setLayout(new GridLayout(2, false));
		
		Label label = new Label(this, SWT.NONE);
		label.setText(guiText("ConsoleComposite.label"));
		GridData gridData = new GridData(GridData.BEGINNING, GridData.FILL,
				true, true, 1, 1);
		label.setLayoutData(gridData);

		gridData = new GridData(GridData.END, GridData.FILL, true, true, 1, 1);
		cleanBtn = new Button(this, SWT.PUSH);

		cleanBtn.setText(guiText("ConsoleComposice.cleanBtn"));

		cleanBtn.setLayoutData(gridData);
		cleanBtn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				log.info("CleanConsole btn clicked");
				clearConsole();
			}
		});

		textComp = new Text(this, SWT.MULTI | SWT.READ_ONLY | SWT.BORDER
				| SWT.V_SCROLL | SWT.H_SCROLL);
		textComp.setEditable(false);
		gridData = new GridData(GridData.FILL, GridData.FILL, true, true, 2, 1);
		gridData.widthHint = Config.GET_CONSOLE_WIDTH();
		gridData.heightHint = Config.GET_CONSOLE_HEIGH();
		textComp.setLayoutData(gridData);
		textComp.setBackground(getDisplay().getSystemColor(Config.GET_CONSOLE_BACKGROUND()));
		textComp.setForeground(getDisplay().getSystemColor(Config.GET_CONSOLE_FOREGROUND()));
		textComp.setFont(new Font(getDisplay(), Config.GET_CONSOLE_FONT(),  Config.GET_EDITOR_FONT_SIZE(), 0));
	}

	public Text getTextComp() {
		return textComp;
	}

	
	
	
	private void __setText(String __text) {
		final String text = __text;
		Runnable action = new Runnable() {

			@Override
			public void run() {
				textComp.setText(text);
			}
		};
		performGuiAsyncActionOnCorrectThread(action);
	}

	private void __appendText(String __text) {
		final String text = __text;
		Runnable action = new Runnable() {
			@Override
			public void run() {
				textComp.append(text);
				//textComp.setTopIndex(textComp.getLineCount() - 1);
			}
		};
		performGuiAsyncActionOnCorrectThread(action);
	}

	
}
