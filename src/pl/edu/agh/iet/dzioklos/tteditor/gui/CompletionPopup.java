package pl.edu.agh.iet.dzioklos.tteditor.gui;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

import pl.edu.agh.iet.dzioklos.tteditor.beans.CElement;
import pl.edu.agh.iet.dzioklos.tteditor.beans.CElement.Type;
import pl.edu.agh.iet.dzioklos.tteditor.config.Config;

public class CompletionPopup {
	private static final Logger log = Logger.getLogger(CompletionPopup.class);
	private Shell popupShell;
	private Table table;
	private Display display;
	private StyledText text;
	private Shell shell;

	private String currentlyCompletedWord;
	private List<CElement> elements;

	public CompletionPopup(Display display, StyledText text, Shell shell) {
		this.display = display;
		this.text = text;
		this.shell = shell;
		init();
		addLogic();
	}

	public Shell getPopupShell() {
		return popupShell;
	}

	public synchronized void setCElements(List<CElement> elements) {
		this.elements = elements;
		// elements.add(new
		// CElement().setName("time_t").setType(Type.STRUCT).addParam("a",
		// "int").addParam("dudu", "float"));

	}

	private void addLogic() {

		text.addListener(SWT.KeyDown, new Listener() {
			public void handleEvent(Event event) {

				if (!popupShell.isVisible())
					return;

				switch (event.keyCode) {
				case SWT.ARROW_DOWN:
					int index = (table.getSelectionIndex() + 1)
							% table.getItemCount();
					table.setSelection(index);
					event.doit = false;
					break;
				case SWT.ARROW_UP:
					index = table.getSelectionIndex() - 1;
					if (index < 0)
						index = table.getItemCount() - 1;
					table.setSelection(index);
					event.doit = false;
					break;
				case SWT.CR:
					if (popupShell.isVisible()
							&& table.getSelectionIndex() != -1) {
						// event.doit = false;
						compete();
					}
					break;
				case SWT.ESC:
					popupShell.setVisible(false);
					break;
				}
			}
		});

		text.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent arg0) {

			}

			@Override
			public void keyPressed(KeyEvent e) {

				if (e.keyCode == 32 && ((e.stateMask & SWT.CTRL) != 0)) {
					log.info("CTRL-SPACE");

					// String string = text.getText();

					String string = getWordBeforeCursor();
					currentlyCompletedWord = string;
					// if (string.length() == 0) {
					// popupShell.setVisible(false);
					// } else {

					List<String> suggentionForText = getSuggestionsForText(string);
					if (suggentionForText.isEmpty())
						popupShell.setVisible(false);

					table.removeAll();
					for (String suggestion : suggentionForText) {
						new TableItem(table, SWT.NONE).setText(suggestion);
					}
					// Rectangle textBounds = display.map(text, null,
					// text.getBounds());
					Rectangle cursorCos = text.getCaret().getBounds();
					
					cursorCos = display.map(text, null, cursorCos);
					
					// popupShell.setBounds(textBounds.x, textBounds.y
					// + textBounds.height, textBounds.width - 50, 100);
					log.info("carretBounts: x=" + cursorCos.x + " y="
							+ cursorCos.y + " h=" + cursorCos.height + " w="
							+ cursorCos.width);
					
					popupShell.setBounds(cursorCos.x + 15, cursorCos.y + 10,
							200, 100);

					popupShell.setVisible(true);
				}

				// }
			}

		});

		// text.addListener(SWT.Modify, new Listener() {
		// public void handleEvent(Event event) {
		// String string = text.getText();
		// if (string.length() == 0) {
		// popupShell.setVisible(false);
		// } else {
		// TableItem[] items = table.getItems();
		// for (int i = 0; i < items.length; i++) {
		// items[i].setText(string + '-' + i);
		// }
		// Rectangle textBounds = display.map(text, null,
		// text.getBounds());
		// popupShell.setBounds(textBounds.x, textBounds.y
		// + textBounds.height, textBounds.width, 150);
		// popupShell.setVisible(true);
		// }
		// }
		// });

		table.addListener(SWT.DefaultSelection, new Listener() {
			public void handleEvent(Event event) {
				compete();
			}
		});
		table.addListener(SWT.KeyDown, new Listener() {
			public void handleEvent(Event event) {
				if (event.keyCode == SWT.ESC) {
					popupShell.setVisible(false);
				}
			}
		});

		// Listener focusOutListener = new Listener() {
		// public void handleEvent(Event event) {
		// /* async is needed to wait until focus reaches its new Control */
		// display.asyncExec(new Runnable() {
		// public void run() {
		// if (display.isDisposed()) return;
		// Control control = display.getFocusControl();
		// if (control == null || (control != text && control != table)) {
		// popupShell.setVisible(false);
		// }
		// }
		// });
		// }
		// };

		Listener focusOutListener = new Listener() {
			public void handleEvent(Event event) {
				// popupShell.setVisible(false);
				// async is needed to wait until focus reaches its new Control/
				display.asyncExec(new Runnable() {
					public void run() {
						if (display.isDisposed())
							return;
						Control control = display.getFocusControl();
						if (control == null
								|| (control != text && control != table && control != popupShell)) {
							popupShell.setVisible(false);
						}
					}
				});
			}
		};

		table.addListener(SWT.FocusOut, focusOutListener);
		text.addListener(SWT.FocusOut, focusOutListener);

		shell.addListener(SWT.Move, new Listener() {
			public void handleEvent(Event event) {
				popupShell.setVisible(false);
			}
		});

	}

	protected void compete() {
		// text.setText(table.getSelection()[0].getText());

		if (currentlyCompletedWord == null) {
			log.warn("currentlyCompletedWord  == null ->true");
			return;
		}
		String selected = table.getSelection()[0].getText();
		log.info("Selected=" + selected);
		int currLen = currentlyCompletedWord.length();
		// todo add .

		String toSet = "";
		if (!currentlyCompletedWord.isEmpty()
				&& (currentlyCompletedWord.charAt(currentlyCompletedWord
						.length() - 1) == '.')
				|| currentlyCompletedWord.endsWith("->")) {
			toSet = selected;
		} else {
			toSet = selected.substring(currLen);
		}
		log.info("Trying to compete = " + toSet);

		int offset = text.getCaretOffset();

		text.insert(toSet);
		text.setCaretOffset(offset + toSet.length());

		popupShell.setVisible(false);

	}

	private List<String> tmpSuggestions = new LinkedList<String>();

	protected synchronized List<String> getSuggestionsForText(String string) {
		tmpSuggestions.clear();
		if (this.elements == null)
			return tmpSuggestions;

		boolean isDot = string.endsWith(".");
		if (isDot || string.endsWith("->")) {
			// trying to get struct items
			String structName = string.substring(0, string.length()
					- (isDot ? 1 : 2));
			log.info("Trying to append struct=" + structName + "  parts");
			for (CElement e : elements) {
				if (e.getType() == Type.STRUCT
						&& e.getName().equals(structName)) {
					for (String param : e.getParamsAndTypes().keySet()) {
						tmpSuggestions.add(param);
					}
				}
			}
		} else {

			for (CElement e : elements) {
				if (e.getName().startsWith(string))
					tmpSuggestions.add(e.getName());
			}
		}
		return tmpSuggestions;

	}

	protected String getWordBeforeCursor() {
		int offset = text.getCaretOffset();
		int lineNo = text.getLineAtOffset(offset);
		String line = text.getLine(lineNo);
		log.info("Trying to resolve word before coursor to completion, lineNo="
				+ lineNo + " line=" + line);
		int lineStartOffset = text.getOffsetAtLine(lineNo);
		int offsetInLine = offset - lineStartOffset;
		log.info("Trying to resolve word before coursor to completion, lineNo="
				+ lineNo + " line=" + line + " offsetInChank=" + offsetInLine);
		char[] chanks = line.toCharArray();
		int i = offsetInLine - 1;
		StringBuffer sb = new StringBuffer();
		while (i >= 0 && chanks[i] != ' ' && chanks[i] != '\n'
				&& chanks[i] != ';' && chanks[i] != '}' && chanks[i] != ')') {
			sb.append(chanks[i]);
			--i;
		}
		String result = sb.reverse().toString().trim();
		log.info("Resolved, result=" + result);
		return result;
	}

	private void init() {
		popupShell = new Shell(display, SWT.ON_TOP);
		table = new Table(popupShell, SWT.SINGLE);
		popupShell.setLayout(new FillLayout());
		
		table.setBackground(display.getSystemColor(Config.GET_COMPLETITION_POPUP_BACKGROUND()));
	}

}
