package pl.edu.agh.iet.dzioklos.tteditor.gui;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.Bullet;
import org.eclipse.swt.custom.ExtendedModifyEvent;
import org.eclipse.swt.custom.ExtendedModifyListener;
import org.eclipse.swt.custom.LineStyleEvent;
import org.eclipse.swt.custom.LineStyleListener;
import org.eclipse.swt.custom.ST;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.custom.VerifyKeyListener;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GlyphMetrics;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import pl.edu.agh.iet.dzioklos.tteditor.antlr.grammar.CParserWrapper;
import pl.edu.agh.iet.dzioklos.tteditor.beans.CElement;
import pl.edu.agh.iet.dzioklos.tteditor.config.Config;
import pl.edu.agh.iet.dzioklos.tteditor.utils.CElementUtils;
import pl.edu.agh.iet.dzioklos.tteditor.utils.CParserUtils;
import pl.edu.agh.iet.dzioklos.tteditor.utils.UndoRedoImpl;

//import static pl.edu.agh.iet.dzioklos.tteditor.utils.GuiTextUtils.guiText;

public class CEditorComposite extends TTComposite {
	private static final Logger log = Logger.getLogger(CEditorComposite.class);
	private StyledText styledText;
	private Display display;
	private boolean isDirty = false;
	private boolean turnOffEnter = false;
	private List<Integer> badLinesNo = new ArrayList<Integer>(10);
	private List<CElement> commentsCElemets = null;
	protected UndoRedoImpl undoRedoImpl;
	private CompletionPopup completionPopup;

	// otimalization :D - O(1)
	private Set<String> cKeywords = new HashSet<String>(
			CParserWrapper.getKeywords());

	public CEditorComposite(Composite comp, int options) {
		super(comp, options);
		this.display = getDisplay();
		init();
		addLogic();
	}

	public StyledText getStyledText() {
		return styledText;
	}

	

	/**
	 * THREAD SAFE
	 * 
	 */
	public boolean isDirty() {
		return isDirty;
	}

	/**
	 * THREAD SAFE
	 * 
	 * @return this
	 */
	public CEditorComposite addBadLineNo(int lineNo) {
		synchronized (badLinesNo) {
			this.badLinesNo.add(lineNo);
		}

		return this;
	}

	/**
	 * THREAD SAFE
	 * 
	 * @return this
	 */
	public CEditorComposite addBadLineNoAndRefresh(int lineNo) {
		addBadLineNo(lineNo);
		refreshTextStyles();
		return this;
	}

	/**
	 * THREAD SAFE
	 * 
	 * @return this
	 */
	public CEditorComposite clearBadLinesAndRefresh() {
		synchronized (badLinesNo) {
			badLinesNo.clear();
		}
		refreshTextStyles();
		return this;
	}

	/**
	 * THREAD SAFE
	 * 
	 * @return this
	 */
	public CEditorComposite refreshTextStyles() {
		performGuiAsyncActionOnCorrectThread(new Runnable() {
			@Override
			public void run() {
				styledText.redraw();
			}
		});
		return this;
	}

	/**
	 * THREAD SAFE
	 * 
	 * @return this
	 */
	public CEditorComposite setDirty(final boolean isDirty) {
		Runnable action = new Runnable() {
			@Override
			public void run() {
				CEditorComposite.this.isDirty = isDirty;
			}
		};
		performGuiAsyncActionOnCorrectThread(action);
		return this;
	}

	private String __tmpText = null;

	/**
	 * ThreadSafe
	 * 
	 * @return text from styledText component
	 * 
	 */
	public String getText() {
		Runnable action = new Runnable() {
			@Override
			public void run() {
				__tmpText = styledText.getText();
			}
		};
		performGuiSyncActionOnCorrectThread(action);
		String toReturn = __tmpText;
		__tmpText = null;
		return toReturn;
	}
	/**
	 * ThreadSafe
	 * 
	 * @param __text
	 * 
	 */
	public void setText(String __text) {
		final String text = __text;
		Runnable action = new Runnable() {
			@Override
			public void run() {
				styledText.setText(text);
			}
		};
		performGuiAsyncActionOnCorrectThread(action);
		CParserUtils.getInstance().parseAndRefreshGuiInBackground();
		//if( undoRedoImpl != null ) undoRedoImpl.clear();
		refreshTextStyles();
	}
	/**
	 * Sets cursor at start of lineNo ThreadSafe
	 * 
	 * @param lineNo
	 *            - line number counts from zero !
	 */
	public void goToLine(final int lineNo) {
		Runnable action = new Runnable() {
			@Override
			public void run() {
				__goToLine(lineNo);
			}
		};
		performGuiAsyncActionOnCorrectThread(action);
	}

	/**
	 * Sets cursor at start of lineNo and selects line ThreadSafe
	 * 
	 * @param lineNo
	 *            - line number counts from zero !
	 */
	public void goToAndSelectLine(final int lineNo) {
		Runnable action = new Runnable() {
			@Override
			public void run() {
				__goToAndSelectLine(lineNo);
			}
		};
		performGuiAsyncActionOnCorrectThread(action);
	}

	public synchronized void setCElements(List<CElement> elements) {
		completionPopup.setCElements(elements);
		this.commentsCElemets = CElementUtils.getLineCommentsOnly(elements);
		refreshTextStyles();
	}

	/**************************
	 * Logic / private methods
	 ***************************/

	private void addLogic() {
		final Set<String> preprocessorKeywords = new HashSet<String>(10);
		preprocessorKeywords.add("#include");
		preprocessorKeywords.add("#define");
		preprocessorKeywords.add("#endif");
		preprocessorKeywords.add("#if");
		preprocessorKeywords.add("#ifndef");
		preprocessorKeywords.add("#ifdef");
		preprocessorKeywords.add("NULL");
		preprocessorKeywords.add("#pragma");

		styledText.addLineStyleListener(new LineStyleListener() {
			public void lineGetStyle(LineStyleEvent e) {
				// Set the line number
				if (Config.SHOW_LINE_NUMBERS()) {
					createLineNumbers(e);
				}

				List<StyleRange> styles = new LinkedList<StyleRange>();

				// searching for keyword only in visible area
				int lineNo = styledText.getLineAtOffset(e.lineOffset);
				String currentLine = styledText.getLine(lineNo);
				int currLineLen = currentLine.length();
			//	int currLineOffset = e.lineOffset;
				// log.info("lineGetStyle, lineNo=" + lineNo + "line="
				// + currentLine);

				int lastIdx = 0;

				for (String word : currentLine.split("[\\s;\n()\\*]")) {
					// log.info(" word = " + word );
					lastIdx = currentLine.indexOf(word, lastIdx);
					if (cKeywords.contains(word.trim())) { // word.trim().equals("int")
						// log.info("lastIdx="+lastIdx+ " len="+word.length());
						styles.add(new StyleRange(e.lineOffset + lastIdx, word
								.length(), display.getSystemColor(Config
								.GET_CKEYWORDS_COLOR()), null, SWT.BOLD));

					} else if (preprocessorKeywords.contains(word.trim())) {
						styles.add(new StyleRange(e.lineOffset + lastIdx, word
								.length(), display.getSystemColor(Config
								.GET_PREPROCESOR_KEYWORDS_COLOR()), null,
								SWT.BOLD));

					}
					lastIdx += word.length();
				}

				// get error lines to repaint
				synchronized (badLinesNo) {
					if (badLinesNo.contains(lineNo)) {
						StyleRange style1 = new StyleRange();
						style1.start = e.lineOffset;
						style1.length = currentLine.length();
						style1.underline = true;
						style1.background = getDisplay().getSystemColor(
								Config.GET_EDITOR_ERRORS_BACKGROUND_COLOR());
						styles.add(style1);
					}

				}

				// check if line doesnt have comment
				// TODO: deep refactor in free time :) 
				synchronized (CEditorComposite.this) {
					if (CEditorComposite.this.commentsCElemets != null) {
							for (CElement element : CEditorComposite.this.commentsCElemets) {

								if (element.getCommentStartGlobalOffset() > styledText
										.getText().length()
										|| element
												.getCommentStartGlobalOffset()
												+ element.getCommentLength() > styledText
												.getText().length()) {
									continue;
								}
								int commentStartLine = styledText
										.getLineAtOffset(element
												.getCommentStartGlobalOffset());
								int commentEndLine = styledText
										.getLineAtOffset(element
												.getCommentStartGlobalOffset()
												+ element.getCommentLength());

								if (lineNo <= commentEndLine
										&& lineNo >= commentStartLine) {
									log.info("Comment in line " + lineNo);

									int startOfCommentInLine = element
											.getCommentStartGlobalOffset() < e.lineOffset ? e.lineOffset
											: element
													.getCommentStartGlobalOffset();
									int endOfCommentInLine = ((element
											.getCommentStartGlobalOffset() + element
											.getCommentLength()) > e.lineOffset
											+ currLineLen) ? e.lineOffset
											+ currLineLen
											: (element
													.getCommentStartGlobalOffset() + element
													.getCommentLength());

									styles.add(new StyleRange(
											startOfCommentInLine,
											endOfCommentInLine
													- startOfCommentInLine,
											display.getSystemColor(Config
													.GET_COMMENT_COLOR()),
											null, SWT.BOLD));
								}

							}
						}
				
				}

				e.styles = styles.toArray(new StyleRange[0]);

			}
		});

		styledText.addExtendedModifyListener(new ExtendedModifyListener() {
			// numery linii sie psuj� jak tego nie ma
			@Override
			public void modifyText(ExtendedModifyEvent arg0) {
				styledText.redraw();
				isDirty = true;
			}
		});
		styledText.addVerifyKeyListener(new VerifyKeyListener() {

			@Override
			public void verifyKey(VerifyEvent arg0) {
				// if (arg0.character == SWT.CR) {
				if (completionPopup.getPopupShell().isVisible()) {
					arg0.doit = false;
					turnOffEnter = true;
				}
				// }
			}
		});
		styledText.addKeyListener(new KeyListener() {

			@Override
			public void keyReleased(KeyEvent arg0) {

			}

			@Override
			public void keyPressed(KeyEvent arg0) {
				if (turnOffEnter) {
					turnOffEnter = false;
					return;
				}
				int cursorPos = styledText.getCaretOffset();
				if (arg0.character == '{') {
					int tabsCount = countStratingTabsInCurrentLine();
					String tabs = getTabs(tabsCount);
					styledText.insert("\n" + tabs + Config.GET_TAB_SEQUENCE()
							+ "\n" + tabs + "}");
					styledText.setCaretOffset(cursorPos + tabsCount + 1
							+ Config.GET_TAB_SEQUENCE().length());
					log.info("Inserting " + tabs.length() + " tabs");
				} else if (arg0.character == '(') {
					styledText.insert(")");
				} else if (arg0.character == '"') {
					styledText.insert("\"");

				} else if (arg0.character == '\'') {
					styledText.insert("'");

				}else if (arg0.character == '[') {
					styledText.insert("]");

				}else if (arg0.character == SWT.CR) {

					log.info("Enter");

					int tabs = countStratingTabsInCursorOffset(cursorPos - 1);
					// log.info("Inserting tabs="+tabs +
					// " currPosition="+cursorPos);

					String tabsString = getTabs(tabs);
					styledText.insert(tabsString);

					styledText.setCaretOffset(cursorPos + tabs
							* Config.GET_TAB_SEQUENCE().length());

				}

			}
		});

	}

	protected String getTabs(int tabs) {
		StringBuilder sb = new StringBuilder(tabs);
		for (int i = 0; i < tabs; i++)
			sb.append(Config.GET_TAB_SEQUENCE());
		return sb.toString();
	}

	private int countStratingTabsInCurrentLine() {
		return countStartingTabInLine(styledText.getLineAtOffset(styledText
				.getCaretOffset()));
	}

	private int countStratingTabsInCursorOffset(int offset) {
		return countStartingTabInLine(styledText.getLineAtOffset(offset));
	}

	private int countStartingTabInLine(int lineNo) {
		if (lineNo < 0 || lineNo >= styledText.getLineCount())
			return 0;
		String line = styledText.getLine(lineNo);
		// log.info("line="+line);
		int count = 0;
		int offset = 0;
		String tabSeq = Config.GET_TAB_SEQUENCE();
		while (offset + tabSeq.length() - 1 < line.length()) {
			String subString = line.substring(offset, offset + tabSeq.length());
			// log.info("subString="+subString + " offset="+offset + " len="+
			// tabSeq.length());
			if (subString.equals(tabSeq)) {
				count++;
			} else
				break;
			offset += tabSeq.length();
		}
		// log.info("Tabs in curr position="+count);
		return count;
	}

	private void createLineNumbers(LineStyleEvent e) {
		e.bulletIndex = styledText.getLineAtOffset(e.lineOffset);
		StyleRange style = new StyleRange();
		style.metrics = new GlyphMetrics(0, 0,
				Config.GET_LINE_NUMBERS_NUBER_COUNT() * 12);
		// style.metrics = new GlyphMetrics(0, 0, Integer.toString(
		// styledText.getLineCount() + 1).length() * 12);

		style.background = display.getSystemColor(Config
				.GET_EDITOR_LINENO_BACKGROUND());
		style.strikeoutColor = display.getSystemColor(SWT.COLOR_GRAY);
		e.bullet = new Bullet(ST.BULLET_NUMBER, style);
	}

	private void init() {
		this.setLayout(new FillLayout());
		styledText = new StyledText(this, SWT.BORDER | SWT.V_SCROLL
				| SWT.H_SCROLL);
		undoRedoImpl = new UndoRedoImpl(styledText);
		styledText.setForeground(display.getSystemColor(Config
				.GET_EDITOR_FONT_COLOR()));
		styledText.setBackground(display.getSystemColor(Config
				.GET_EDITOR_BACKGROUND_COLOR()));

		styledText.setFont(new Font(display, Config.GET_EDITOR_FONT(), Config
				.GET_EDITOR_FONT_SIZE(), 0));
		completionPopup = new CompletionPopup(getDisplay(), styledText,
				getShell());
	}

	private void __goToLine(int lineNo) {
		if (lineNo >= styledText.getLineCount()) {
			log.error("Out of bound lineNo");
			return;
		}
		int offset = styledText.getOffsetAtLine(lineNo);
		styledText.setCaretOffset(offset);
		log.info("going to line " + lineNo + " offset=" + offset);
	}

	private void __goToAndSelectLine(int lineNo) {
		if (lineNo - 1 >= styledText.getLineCount() || lineNo < 1) {
			log.error("Out of bound selection");
			return;
		}
		lineNo--;
		int offset = styledText.getOffsetAtLine(lineNo);
		styledText.setCaretOffset(offset);
		styledText.setSelectionRange(offset, styledText.getLine(lineNo)
				.length());

		styledText.showSelection();
		log.info("going and selecting line " + lineNo + " offset=" + offset);
	}
}
