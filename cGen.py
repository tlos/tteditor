import sys
import random


HOW_MANY_FUNCTIONS = 100;
HOW_MANY_GLOBALS = 30;

CREATE_MAIN = True

FUNCTIONS_PREFIX = "func_"
GLOBALS_PREXIX = "globalVarible_"

OUT_FILE = "generated.c"

types = ["int", "float", "double", "char"]

sampleDefinitions= [ "int a = 10; ", "float b = 10.1;", "char a ='a';", "double h = 0.0;" ]



def main():
	with open(OUT_FILE, "w") as f:
		f.write("#include <stdio.h>\n")
		f.write("#define HELLO 1\n\n")
		for i in range(HOW_MANY_GLOBALS):
			f.write( random.choice(types) + " " + GLOBALS_PREXIX + str(i) + ";  /* GLOBAL VAR */\n")
		#f.write("\n\n// functions \n")
		for i in range(HOW_MANY_FUNCTIONS):
			f.write(  random.choice(types) + " " + FUNCTIONS_PREFIX + str(i) + "( ){\n\t" +  random.choice(sampleDefinitions) +  "\n}\n\n" )
		if CREATE_MAIN:
			f.write("int main(){\n\t//Main method\n}")


if __name__ == "__main__":
	main()